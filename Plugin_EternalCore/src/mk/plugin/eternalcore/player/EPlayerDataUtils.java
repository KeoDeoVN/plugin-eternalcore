package mk.plugin.eternalcore.player;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

public class EPlayerDataUtils {
	
	private static Map<String, EPlayer> playerDataMap = new HashMap<String, EPlayer> ();
	
	public static boolean hasData(String name) {
		return playerDataMap.containsKey(name);
	}
	
	public static boolean hasData(Player player) {
		return hasData(player.getName());
	}
	
	public static EPlayer getData(String name) {
		if (!hasData(name)) return null;
		return playerDataMap.get(name);
	}
	
	public static EPlayer getData(Player player) {
		return getData(player.getName());
	}
	
	public static Map<String, EPlayer> getOnlineData() {
		return playerDataMap;
	}
	
	public static void setData(String name, EPlayer ep) {
		playerDataMap.put(name, ep);
	}
	
	public static void setData(Player player, EPlayer ep) {
		setData(player.getName(), ep);
	}
	
	public static void clearData(String name) {
		playerDataMap.remove(name);
	}
	
	public static void clearData(Player player) {
		clearData(player.getName());
	}
	
	public static void load(Player player) {
		EPlayer ep = EPlayerStorageUtils.load(player);
		setData(player, ep);
	}
	
	public static void save(Player player) {
		EPlayerStorageUtils.save(player);
	}
	
	
	
	
	
	
}

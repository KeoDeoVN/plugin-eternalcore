package mk.plugin.eternalcore.player;

import org.bukkit.entity.Player;

import net.luckperms.api.LuckPermsProvider;
import net.luckperms.api.model.user.User;
import net.luckperms.api.query.QueryOptions;

public class EPlayerListNameUtils {
	
	public static String getListName(Player player) {
		String prefix = player.hasPermission("eternal.special") ? "§d*" : "";
		User user = LuckPermsProvider.get().getUserManager().getUser(player.getUniqueId());
		QueryOptions queryOptions = LuckPermsProvider.get().getContextManager().getQueryOptions(player);
		String rank = user.getCachedData().getMetaData(queryOptions).getPrefix();
	
		return (rank + prefix + player.getName() + "§7 " + player.getLevel()).replace("&", "§");
	}
	
}

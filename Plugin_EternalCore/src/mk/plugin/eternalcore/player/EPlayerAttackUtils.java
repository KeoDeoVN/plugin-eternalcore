package mk.plugin.eternalcore.player;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Monster;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.hologram.HoloUtils;
import mk.plugin.eternalcore.feature.weaponsoul.EWeaponSoulUtils;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.mob.level.EMobLevelUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class EPlayerAttackUtils {
	
	private static Map<String, Long> cooldownAttack = new HashMap<String, Long> ();	
	private static Map<Integer, Long> cooldownDamage = new HashMap<Integer, Long> ();
	
	public static boolean isCooldownAttack(Player player) {
		if (!cooldownAttack.containsKey(player.getName())) return false;
		if (cooldownAttack.get(player.getName()) < System.currentTimeMillis()) return false;
		return true;
	}
	
	public static void setCooldownAttack(Player player, long timeMilis, Material material) {
		player.setCooldown(material, new Long(timeMilis / 50).intValue());
		cooldownAttack.put(player.getName(), System.currentTimeMillis() + timeMilis);	
		player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(100);
	}
	
//	public static ItemStack setAttackSpeed(ItemStack item, double value) {
//        net.minecraft.server.v1_12_R1.ItemStack nmsStack = CraftItemStack.asNMSCopy(item);
//        NBTTagCompound compound = (nmsStack.hasTag()) ? nmsStack.getTag() : new NBTTagCompound();
//        NBTTagList modifiers = new NBTTagList();
//        NBTTagCompound damage = new NBTTagCompound();
//       
//        damage.set("AttributeName", new NBTTagString("generic.attackSpeed"));
//        damage.set("Name", new NBTTagString("generic.attackSpeed"));
//        damage.set("Amount", new NBTTagDouble(value));
//        damage.set("Operation", new NBTTagInt(0));
//        damage.set("UUIDLeast", new NBTTagInt(894654));
//        damage.set("UUIDMost", new NBTTagInt(2872));
//        damage.set("Slot", new NBTTagString("mainhand"));
//        modifiers.add(damage);
//        compound.set("AttributeModifiers", modifiers);
//        nmsStack.setTag(compound);
//        return CraftItemStack.asBukkitCopy(nmsStack);
//	}
	
	public static void removeCooldownAttack(Player player) {
		cooldownAttack.remove(player.getName());
	}
	
	public static boolean isCooldownDamage(int entityID) {
		if (cooldownDamage.containsKey(entityID)) {
			if (cooldownDamage.get(entityID) > System.currentTimeMillis()) return true;
			return false;
		}
		return false;
	}
	
	public static void setCooldownDamage(int entityID, long timeMilis) {
		cooldownDamage.put(entityID, System.currentTimeMillis() + timeMilis);
	}
	
	public static void removeCooldownDamage(int entityID) {
		cooldownDamage.remove(entityID);
	}
	
	public static boolean hasAttackArrow(Arrow arrow) {
		return arrow.hasMetadata("attackClick");
	}
	
	public static void addAttackArrow(Arrow arrow) {
		arrow.setMetadata("attackClick", new FixedMetadataValue(MainEC.plugin, ""));
	}
	
	public static void removeAttackArrow(Arrow arrow) {
		arrow.removeMetadata("attackClick", MainEC.plugin);
	}
	
	// Damage execute
	public static void damage(Player player, LivingEntity target, double damage, EDamageType dT) {
		if (target.isDead()) return;
		if (target == player) return;
		if (target.hasMetadata("NPC")) return;
		if (target.getType() == EntityType.ARMOR_STAND) {
			return;
		}
		
		// Leveled mob
		if (!EMobLevelUtils.isBoss(target)) {
			int mobLevel = EMobLevelUtils.getLevel(target);
			if (mobLevel - player.getLevel() > 15) {
				player.sendMessage("§cKhông thể đánh quái quá 15 cấp độ");
				return;
			}
		}
		
		EPlayer ep = EPlayerDataUtils.getData(player);
		List<String> holo = Lists.newArrayList();
		if (target instanceof Player) {
			Player pTarget = (Player) target;
			EPlayer tep = EPlayerDataUtils.getData(pTarget);
			
			// Dodge
			if (Utils.rate(tep.getStatValue(EStat.DODGE, pTarget))) return;
			
			// Defense
			damage *= (1 - (tep.getStatValue(EStat.DEFENSE, pTarget) / 100));
			
			// - 70% damage
			damage *= (1 - 0.7);
		}
		
		// Weapon soul
		for (MCItemType it : ep.getSouls().keySet()) {
			if (MCItemType.isHoldingType(it, player)) {
				int point = ep.getSoul(it);
				damage *= (double) (100 + EWeaponSoulUtils.getBuff(point)) / 100;
			}
		}
		
		// Enchantments
		ItemStack itemHand = player.getInventory().getItemInMainHand();
		int damageall = itemHand.getEnchantmentLevel(Enchantment.DAMAGE_ALL);
		int damageundead = itemHand.getEnchantmentLevel(Enchantment.DAMAGE_UNDEAD);
		damage += damageall;
		if (target instanceof Monster || target.getType() == EntityType.SPIDER) {
			damage += damageundead;
		}
		int fireaspect = itemHand.getEnchantmentLevel(Enchantment.FIRE_ASPECT);
		if (fireaspect > 0) {
			target.setFireTicks(fireaspect * 20);
		}	
		
		// Check damage
		Utils.addDamageCheck(target);
		target.damage(0.00000001, player);
		if (!Utils.canDamageCheck(target)) return;
		Utils.removeCanDamageCheck(target);
		
		// Damage
		Utils.setHealth(target, Math.max(0, target.getHealth() - damage));
		
		// Holograms
		holo.add(dT.getPrefix() + dT.getIcon() + new Double(Utils.round(damage)).intValue());
		
		// Vampire
		double regain = damage * ep.getStatValue(EStat.LIFESTEAL, player);
		Utils.addHealth(player, regain);
		if (regain > 0.) {
			holo.add("§a§o+" + Utils.round(regain) + " HP");
		}
		
		// Potion
		if (EItemUtils.isEItem(itemHand)) {
			EItem ei = EItemUtils.fromItem(itemHand);
			ei.getPotions().forEach((t, p) -> {
				if (Utils.rate(p.getChance())) {
					target.addPotionEffect(new PotionEffect(t, 40, p.getLevel()));
					holo.add("§d§o" + Utils.getVNPotionName(t));
				}
			});
		}
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			HoloUtils.hologram(MainEC.plugin, holo, 15, player, target, 1);
		});
	}
	
	public static void damage(Player player, LivingEntity target, double damage, EDamageType dT, long timeMilis) {
		if (target.isDead()) return;
		if (isCooldownDamage(target.getEntityId())) return;
		setCooldownDamage(target.getEntityId(), timeMilis);		
		damage(player, target, damage, dT);
	}
	
	public static void attack(Player player, LivingEntity target) {
		if (isCooldownAttack(player)) return;
		if (isCooldownDamage(target.getEntityId())) return;

		EPlayer ep = EPlayerDataUtils.getData(player);
		double damage = ep.getStatValue(EStat.DAMAGE, player);
		
		EDamageType dT = EDamageType.DEFAULT;
		
		// Accuracy
		if (Utils.rate(ep.getStatValue(EStat.ACCURACY, player))) {
			damage *= 2;
			dT = EDamageType.CRITICAL;
			target.getWorld().spawnParticle(Particle.CLOUD, target.getLocation().clone().add(0, 1.5, 0.0), 7);
			player.playSound(player.getLocation(), Sound.BLOCK_ANVIL_LAND, 0.3f, 1f);
		}

		damage(player, target, damage, dT);
		// Effect
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			// Cooldown item
			setCooldownAttack(player, new Double(ep.getStatValue(EStat.ATTACKSPEED, player) * 1000).longValue(), player.getInventory().getItemInMainHand().getType());
		});
	}
	
	public static void setDamage(Arrow arrow, double damage) {
		arrow.setMetadata("edamage", new FixedMetadataValue(MainEC.plugin, damage));
	}
	
	public static double getDamage(Arrow arrow) {
		return 	arrow.getMetadata("edamage").get(0).asDouble();
	}
	
	public static Arrow shootArrow(Player player, double damage) {
		Arrow arrow = player.launchProjectile(Arrow.class);
		setDamage(arrow, damage);
		EPlayerAttackUtils.addAttackArrow(arrow);
		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);

		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			arrow.remove();
		}, 10);
		
		return arrow;
	}
	
	public static Arrow shootArrow(Player player, double damage, Vector v) {
		Arrow arrow = player.launchProjectile(Arrow.class, v);
//		arrow.setVelocity(v);
		setDamage(arrow, damage);
		EPlayerAttackUtils.addAttackArrow(arrow);
		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);

		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			arrow.remove();
		}, 10);
		
		return arrow;
	}
	
	public static Arrow shootArrow(Player player, double damage, Vector v, Location location) {
		Arrow arrow = player.getWorld().spawnArrow(location, v, 0, 0);
		arrow.setVelocity(v);
		setDamage(arrow, damage);
		EPlayerAttackUtils.addAttackArrow(arrow);
		player.playSound(player.getLocation(), Sound.ENTITY_ARROW_SHOOT, 1, 1);

		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			arrow.remove();
		}, 10);
		
		return arrow;
	}
	
	public static void damageEntitiesNearby(Player player, double x, double y, double z, double damage, long cd) {
		player.getNearbyEntities(x, y, z).forEach(e -> {
			if (e != player && e instanceof LivingEntity) {
				LivingEntity le = (LivingEntity) e;
				damage(player, le, damage, EDamageType.DEFAULT, cd);
			}
		});
	}
	
	public static void damageEntitiesNearby(Player player, Location location, double x, double y, double z, double damage, long cd) {
		location.getWorld().getNearbyEntities(location, x, y, z).forEach(e -> {
			if (e != player && e instanceof LivingEntity) {
				LivingEntity le = (LivingEntity) e;
				damage(player, le, damage, EDamageType.DEFAULT, cd);
			}
		});
	}

}

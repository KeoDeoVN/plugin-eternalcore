package mk.plugin.eternalcore.player;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.metadata.FixedMetadataValue;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.feature.buff.EExpBuff;
import mk.plugin.eternalcore.feature.buff.EStatBuff;
import mk.plugin.eternalcore.feature.power.EPowerUtils;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class EPlayerStatUtils {

	/*
	 * 1 level = 2 point
	 */
	public static int getRemainPotentials(Player player) {
		int sum = player.getLevel() * 2;
		EPlayer rpgP = EPlayerDataUtils.getData(player.getName());
		int has = 0;
		for (int i : rpgP.getPotentials().values()) has += i;
		return sum - has;
	}
	
	/*
	 1 level = 1 point
	 */
	
	public static int getRemainSkills(Player player) {
		int lv = player.getLevel();
		EPlayer rpgP = EPlayerDataUtils.getData(player.getName());
		int has = 0;
		for (int i : rpgP.getSkills().values()) has += i;
		return lv - has;
	}

	public static void updateStats(Player player) {
		// Sync update
		Object mo = null;
		if (player.hasMetadata("updatePlayer_mo")) {
			mo = player.getMetadata("updatePlayer_mo").get(0).value();
		} else {
			mo = new Object();
			player.setMetadata("updatePlayer_mo", new FixedMetadataValue(MainEC.plugin, mo));
		}

		synchronized (mo) {
			EPlayer rpgP = EPlayerDataUtils.getData(player);
			if (rpgP == null)
				return;

			// Item
			Map<EStat, Integer> itemStats = Maps.newHashMap();
			List<ItemStack> items = Utils.getItemsEquiped(player);

			for (EStat stat : EStat.values()) {
				int itemStat = stat.getMinimum();
				for (ItemStack item : items) {
					if (!EItemUtils.isEItem(item)) continue;
					EItem rpgI = EItemUtils.fromItem(item);
					if (rpgI == null) continue;
					itemStat += rpgI.getCalculatedStat(player, stat);
				}
				itemStats.put(stat, itemStat);
				stat.set(player, itemStat);
			}
			rpgP.setItemStats(itemStats);

			// Check stat buff
			List<EStatBuff> statBuffs = new ArrayList<EStatBuff>(rpgP.getStatBuffs());
			for (EStatBuff buff : rpgP.getStatBuffs()) {
				if (!buff.isStillEffective())
					statBuffs.remove(buff);
			}
			rpgP.setStatBuffs(statBuffs);

			// Check exp buff
			List<EExpBuff> expBuffs = new ArrayList<EExpBuff>(rpgP.getExpBuffs());
			for (EExpBuff buff : rpgP.getExpBuffs()) {
				if (!buff.isStillEffective())
					expBuffs.remove(buff);
			}
			rpgP.setExpBuffs(expBuffs);
			
			// Power
			rpgP.setLastPower(EPowerUtils.calculatePower(player));
			
			// Potions
			Map<PotionEffectType, Integer> max = Maps.newHashMap();
			items.forEach(item -> {
				if (!EItemUtils.isEItem(item)) return;
				EItem ei = EItemUtils.fromItem(item);
				ei.getPassivePotions().forEach((t, l) -> {
					if (max.containsKey(t)) {
						if (max.get(t) < l) max.put(t, l);
					} else max.put(t, l);
				});;
			});
			
			Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
				Lists.newArrayList(player.getActivePotionEffects()).forEach(p -> {
					if (p.getDuration() > 20000) player.removePotionEffect(p.getType());
				});
				max.forEach((t, l) ->  {
					if (t == null) return;
					player.addPotionEffect(new PotionEffect(t, Integer.MAX_VALUE, l - 1));
				});
			});

		}
	}

}

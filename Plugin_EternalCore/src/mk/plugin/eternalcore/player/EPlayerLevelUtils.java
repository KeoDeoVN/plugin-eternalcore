package mk.plugin.eternalcore.player;

import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

public class EPlayerLevelUtils {
	
	public static int getExpToLevel(int level) {
		if (level < 10) return 50;
		if (level < 20) return 50000;
		if (level < 30) return 250000;
		if (level < 40) return 350000;
		if (level < 50) return 450000;
		if (level < 70) return 1000000;
		if (level < 80) return 20000000;
		if (level < 90) return 90000000;
		if (level < 100) return 1000000000; 
		return Integer.MAX_VALUE;
	}
	
	public static void addExp(Player player, int exp) {
		int level = player.getLevel() + 1;
		// Set exp bar
		while (exp > 0) {
			int toExp = getExpToLevel(level);
			float more = new Double((double) exp / toExp).floatValue();
			float afterExpRate = Math.max(0, more + player.getExp());
			if (afterExpRate > 1) {
				exp = exp - new Double((toExp * (1 - player.getExp()))).intValue();
				player.setLevel(level);
				player.setExp(0);
				level++;
			} else {
				exp = 0;
				player.setExp(afterExpRate);
			}
		}
	}
	
	public static void levelUpEffet(Player player, int newLevel) {
		player.sendTitle(" §a§lLÊN CẤP " + newLevel, "§7Tăng điểm tiềm năng và kỹ năng", 1 * 20, 2 * 20, 1 * 20);
		player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
		player.spawnParticle(Particle.FIREWORKS_SPARK, player.getLocation(), 50, 1, 1, 1, 1);
	}
	
	
	
}

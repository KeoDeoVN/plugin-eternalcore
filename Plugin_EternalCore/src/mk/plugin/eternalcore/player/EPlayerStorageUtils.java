package mk.plugin.eternalcore.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.feature.buff.EExpBuff;
import mk.plugin.eternalcore.feature.buff.EStatBuff;
import mk.plugin.eternalcore.feature.potential.EPotential;
import mk.plugin.eternalcore.feature.skill.ESkill;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.playerdata.storage.PlayerData;
import mk.plugin.playerdata.storage.PlayerDataAPI;

public class EPlayerStorageUtils {
	
	public static EPlayer load(Player player) {
		PlayerData data = PlayerDataAPI.getPlayerData(player);
		
		Map<EPotential, Integer> potentials = new HashMap<EPotential, Integer> ();
		Map<MCItemType, Integer> weaponSouls = Maps.newHashMap();
		Map<ESkill, Integer> skills = Maps.newHashMap();
		List<EStatBuff> statBuffs = new ArrayList<EStatBuff> ();
		List<EExpBuff> expBuffs = new ArrayList<EExpBuff> ();
		Map<Integer, ItemStack> gems = Maps.newHashMap();
		
		// Potential
		for (EPotential p : EPotential.values()) {
			int v = 0;
			if (data.hasData("eternal.potential." + p.name())) v = Integer.parseInt(data.getValue("eternal.potential." + p.name()));
			potentials.put(p, v);
		}
		
		// Stat buffs
		int i = 1;
		while (data.hasData("eternal.statbuff." + i + ".stat")) {
			EStat stat = EStat.valueOf(data.getValue("eternal.statbuff." + i + ".stat"));
			int value = Integer.parseInt(data.getValue("eternal.statbuff." + i + ".value"));
			long start = Long.parseLong(data.getValue("eternal.statbuff." + i + ".start"));
			long time = Long.parseLong(data.getValue("eternal.statbuff." + i + ".time"));
			
			EStatBuff sb = new EStatBuff(stat, value, start, time);
			statBuffs.add(sb);
			
			i++;
		}
		
		// Exp buffs
		i = 1;
		while (data.hasData("eternal.expbuff." + i + ".value")) {

			int value = Integer.parseInt(data.getValue("eternal.expbuff." + i + ".value"));
			long start = Long.parseLong(data.getValue("eternal.expbuff." + i + ".start"));
			long time = Long.parseLong(data.getValue("eternal.expbuff." + i + ".time"));
			
			EExpBuff eb = new EExpBuff(value, start, time);
			expBuffs.add(eb);
			
			i++;
		}
		
		// Power
		int power = 0;
		if (data.hasData("eternal.power")) power = Integer.parseInt(data.getValue("eternal.power"));
		
		// Weapon soul
		i = 1;
		while (data.hasData("eternal.weaponsoul." + i)) {
			String s = data.getValue("eternal.weaponsoul." + i);
			MCItemType it = MCItemType.valueOf(s.split(":")[0]);
			int point = Integer.parseInt(s.split(":")[1]);
			weaponSouls.put(it, point);
			
			i++;
		}
		
		// Skill
		i = 1;
		while (data.hasData("eternal.skill." + i)) {
			String s = data.getValue("eternal.skill." + i);
			ESkill it = ESkill.valueOf(s.split(":")[0]);
			int point = Integer.parseInt(s.split(":")[1]);
			skills.put(it, point);
			
			i++;
		}
		
		// Gems
		for (i = 0 ; i < 9 ; i++) {
			if (data.hasData("gem." + i)) {
				if (data.getValue("gem." + i).equalsIgnoreCase("")) continue;
				ItemStack item = ItemStackUtils.toItemStack(data.getValue("gem." + i));
				gems.put(i, item);
			}
		}
		
		// Skill slots
		Map<Integer, ESkill> skillSlots = Maps.newHashMap();
		if (data.hasData("eternal.skillslot")) {
			String s = data.getValue("eternal.skillslot");
			for (String ss : s.split(";")) {
				skillSlots.put(Integer.valueOf(ss.split(":")[0]), ESkill.valueOf(ss.split(":")[1]));
			}
		}
		
		EPlayer ep = new EPlayer(player.getLevel(), power, potentials, statBuffs, expBuffs, weaponSouls, skills, gems, skillSlots);
		
		return ep;
	}
	
	public static void save(Player player) {
		EPlayer ep = EPlayerDataUtils.getData(player.getName());
		PlayerData data = PlayerDataAPI.getPlayerData(player);
		
		// Potential
		for (EPotential p : EPotential.values()) {
			data.set("eternal.potential." + p.name(), ep.getPotential(p) + "");
		}
		
		// Stat buffs
		int c = 0;
		for (EStatBuff sb : ep.getStatBuffs()) {
			c++;
			data.set("eternal.statbuff." + c + ".stat", sb.getStat().name() + "");
			data.set("eternal.statbuff." + c + ".value", sb.getValue() + "");
			data.set("eternal.statbuff." + c + ".start", sb.getStart() + "");
			data.set("eternal.statbuff." + c + ".time", sb.getTime() + "");
		}
		
		// Exp buffs
		c = 0;
		for (EExpBuff eb : ep.getExpBuffs()) {
			data.set("eternal.expbuff." + c + ".value", eb.getValue() + "");
			data.set("eternal.expbuff." + c + ".start", eb.getValue() + "");
			data.set("eternal.expbuff." + c + ".time", eb.getValue() + "");
		}
		
		// Power
		data.set("eternal.power", ep.getLastPower() + "");
		
		// Weapon soul
		c = 0;
		for (Entry<MCItemType, Integer> e : ep.getSouls().entrySet()) {
			c++;
			data.set("eternal.weaponsoul." + c, e.getKey().name() + ":" + e.getValue());
		}
		
		// Skill
		c = 0;
		for (Entry<ESkill, Integer> e : ep.getSkills().entrySet()) {
			c++;
			data.set("eternal.skill." + c, e.getKey().name() + ":" + e.getValue());
		}
		
		// Gems
		for (int i = 0 ; i < 9 ; i++) {
			data.set("gem." + i, "");
		}
		ep.getGems().forEach((s, i) -> {
			data.set("gem." + s, ItemStackUtils.toBase64ItemStack(i));
		});
		
		// Skill Slots
		String ss = "";
		for (Entry<Integer, ESkill> slot : ep.getActiveSkills().entrySet()) {
			ss += slot.getKey() + ":" + slot.getValue() + ";";
		}
		if (ss.length() != 0) {
			ss = ss.substring(0, ss.length() - 1);
			data.set("eternal.skillslot", ss);
		}

		PlayerDataAPI.saveData(player);
	}
	
}

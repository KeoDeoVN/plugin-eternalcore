package mk.plugin.eternalcore.player;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.feature.buff.EExpBuff;
import mk.plugin.eternalcore.feature.buff.EStatBuff;
import mk.plugin.eternalcore.feature.permbuff.EPermBuff;
import mk.plugin.eternalcore.feature.potential.EPotential;
import mk.plugin.eternalcore.feature.skill.ESkill;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.stat.EStat;

public class EPlayer {
	
	private int level;
	private int power;
	private Map<EStat, Integer> itemStats = new HashMap<EStat, Integer> ();
	private Map<EPotential, Integer> potentials = new HashMap<EPotential, Integer> ();
	private List<EStatBuff> statBuffs = new ArrayList<EStatBuff> ();
	private List<EExpBuff> expBuffs = new ArrayList<EExpBuff> ();	
	private Map<ESkill, Integer> skills = Maps.newHashMap();
	private Map<Integer, ESkill> activeSkills = Maps.newHashMap();	
	private Map<MCItemType, Integer> weaponSouls = Maps.newHashMap();
	private Map<Integer, ItemStack> gems = Maps.newHashMap();
	
	public EPlayer(int level, int power, Map<EPotential, Integer> potentials, List<EStatBuff> statBuffs, List<EExpBuff> expBuffs, Map<MCItemType, Integer> weaponSouls, Map<ESkill, Integer> skills, Map<Integer, ItemStack> gems, Map<Integer, ESkill> activeSkills) {
		this.level = level;
		this.power = power;
		this.potentials = potentials;
		this.statBuffs = statBuffs;
		this.expBuffs = expBuffs;
		this.weaponSouls = weaponSouls;
		this.skills = skills;
		this.gems = gems;
		this.activeSkills = activeSkills;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getStat(EStat stat, Player player) {
		int value = stat.getMinimum();
		
		// Potentials
		for (EPotential p : potentials.keySet()) {
			if (!p.getStats().containsKey(stat)) continue;
			value += potentials.get(p) * p.getStats().get(stat);
		}
		
		// Item
		value = itemStats.containsKey(stat) ? value + itemStats.get(stat) : value;
		
		// Buff
		for (EStatBuff buff : this.statBuffs) {
			if (buff.getStat() == stat) {
				value *= (100 + buff.getValue()) / 100;
			}
		}
		
		// Permbuff
		for (EPermBuff buff : EPermBuff.permBuffs.values()) {
			if (player.hasPermission(buff.getPermission()) && buff.getBuffs().containsKey(stat)) {
				value = value * (100 + buff.getBuffs().get(stat)) / 100;
			}
		}
		
		return value;
	}
	
	public int getStatWithoutBuff(EStat stat) {
		int value = stat.getMinimum();
		
		// Potentials
		for (EPotential p : potentials.keySet()) {
			if (!p.getStats().containsKey(stat)) continue;
			value += potentials.get(p) * p.getStats().get(stat);
		}
		
		// Item
		value = itemStats.containsKey(stat) ? value + itemStats.get(stat) : value;
		
		return value;
	}
	
	public double getStatValue(EStat stat, Player player) {
		return stat.pointsToValue(getStat(stat, player));
	}
	
	public Map<EStat, Integer> getItemStats() {
		return this.itemStats;
	}
	
	public int getPotential(EPotential potential) {
		if (potentials.containsKey(potential)) return potentials.get(potential);
		return 0;
	}
	
	public Map<EPotential, Integer> getPotentials() {
		return this.potentials;
	}
	
	public List<EStatBuff> getStatBuffs() {
		return this.statBuffs;
	}
	
	public List<EExpBuff> getExpBuffs() {
		return this.expBuffs;
	}
	
	public int getLastPower() {
		return this.power;
	}
	
	public Map<ESkill, Integer> getSkills() {
		return this.skills;
	}
	
	public int getSkillPoint(ESkill skill) {
		if (this.skills.containsKey(skill)) return this.skills.get(skill);
		return 0;
	}
	
	public Map<Integer, ESkill> getActiveSkills() {
		return this.activeSkills;
	}
	
	public int getSoul(MCItemType it) {
		return this.weaponSouls.getOrDefault(it, 0);
	}
	
	public Map<MCItemType, Integer> getSouls() {
		return this.weaponSouls;
	}
	
	public Map<Integer, ItemStack> getGems() {
		return this.gems;
	}
	
	// Setters
	
	
	public void setPotentials(Map<EPotential, Integer> potentials) {
		this.potentials = potentials;
	}
	
	public void setPotential(EPotential potential, int value) {
		this.potentials.put(potential, value);
	}
	
	public void setStatBuffs(List<EStatBuff> statBuffs) {
		this.statBuffs = statBuffs;
	}
	
	public void setExpBuffs(List<EExpBuff> expBuffs) {
		this.expBuffs = expBuffs;
	}
	
	public void removeStatBuff(EStatBuff buff) {
		this.statBuffs.remove(buff);
	}
	
	public void removeExpBuff(EExpBuff buff) {
		this.expBuffs.remove(buff);
	}
	
	public void setItemStats(Map<EStat, Integer> stats) {
		this.itemStats = stats;
	}
	
	public void setLastPower(int power) {
		this.power = power;
	}
	
	public void setSkills(Map<ESkill, Integer> skills) {
		this.skills = skills;
	}
	
	public void setActiveSkills(List<ESkill> activeSkills) {
//		this.activeSkills = activeSkills;
		for (int i = 0 ; i < activeSkills.size() ; i++) {
			this.activeSkills.put(i, activeSkills.get(i));
		}
	}
	
	public void setActiveSkill(ESkill skill, int index) {
		this.activeSkills.put(index, skill);
	}
	
	public void setSkill(ESkill skill, int point) {
		this.skills.put(skill, point);
	}
	
	public void setGem(int slot, ItemStack item) {
		this.gems.put(slot, item);
	}
	
	public void addSkilPoint(ESkill skill, int amount) {
		setSkill(skill, getSkillPoint(skill) + amount);
	}
	
	public void setSoul(MCItemType it, int soul) {
		this.weaponSouls.put(it, soul);
	}
	
	public void addSoul(MCItemType it, int amount) {
		setSoul(it, getSoul(it) + amount);
	}
}

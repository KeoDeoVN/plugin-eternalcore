package mk.plugin.eternalcore.feature.cuonghoa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ECHGUI {
	
	public static final int ITEM_SLOT = 11;
	public static final int DCH_SLOT = 13;
	public static final int AMULET_SLOT = 15;
	public static final int RESULT_SLOT = 23;
	public static final int BUTTON_SLOT = 21;
	public static final int INFO_SLOT = 35;
	
	public static String TITLE = "§c§lCƯỜNG HÓA TRANG BỊ";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.LIME)), "§aÔ chứa §oVật phẩm"));
			inv.setItem(DCH_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.BLUE)), "§aÔ chứa §oĐá cường hóa"));
			inv.setItem(AMULET_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.WHITE)), "§aÔ chứa §oSách may mắn §r§7(Không bắt buộc)"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.PINK)), "§aÔ chứa §oSản phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(INFO_SLOT, getInfo());
		});
		
		player.openInventory(inv);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Get Item
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item == null) {
					player.sendMessage("§cThiếu Vật phẩm!");
					return;
				}
				// Get DCH
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				if (dchI == null) {
					player.sendMessage("§cThiếu Đá cường hóa!");
					return;
				}
				// Get Amulet
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				// Object
			    EItem rpgI = EItemUtils.fromItem(item);
				EDCH dch = EDCH.fromItem(dchI);
				ESMM amulet = ESMM.fromItem(amuletI);
				double chance = getChance(dch, amulet, rpgI.getLevel() + 1);
				// Do
				
				int uLevel = 0;
				if (Utils.rate(chance)) {
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§6§l+" + rpgI.getLevel() + " >> +" + (rpgI.getLevel() + 1), 0, 15, 0);
					player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
					if (rpgI.getLevel() + 1 >= 5) {
						Utils.broadcast("§f[§6Cường hóa§f] §aPlayer §f" + player.getName() + " §ađã ép thành công vật phẩm lên +" + (rpgI.getLevel() + 1) + ", lực chiến lên một tầm cao mới");
					}
					
					// Increase
					uLevel = 1;
				} else {
					if (amulet == null) uLevel = -1;
					String s = "§c§l+" + rpgI.getLevel() + " >> +" + (rpgI.getLevel() + uLevel);
					player.sendTitle("§7§lTHẤT BẠI T_T", s, 0, 15, 0);
					player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1, 1);
				}
				
				rpgI.setLevel(rpgI.getLevel() + uLevel);
				item = EItemUtils.update(player, item, rpgI);
				Utils.giveItem(player, item);
				
				// Close and reopen
				inv.setItem(DCH_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(AMULET_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
					openGUI(player);
				}, 10);
				
				// Call event
				Bukkit.getPluginManager().callEvent(new CuongHoaEvent(player, item, rpgI.getLevel() - uLevel, rpgI.getLevel()));
			}
		} 
		// Action on bottom inv
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
					// Update result
					Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
						ItemStack clone = clickedItem.clone();
						EItem rpgI = EItemUtils.fromItem(clone);
						rpgI.setLevel(rpgI.getLevel() + 1);
						rpgI.setName(rpgI.getName() + " (Sản phẩm)");
						EItemUtils.update(player, clone, rpgI);
						inv.setItem(RESULT_SLOT, clone);
					});
					return;
				}
				else if (EDCH.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, DCH_SLOT)) {
						player.sendMessage("§cĐã để Đá cường hoá rồi");
						return;
					}
				}
				else if (ESMM.fromItem(clickedItem) != null) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, AMULET_SLOT)) {
						player.sendMessage("§cĐã để Sách may mắn rồi");
						return;
					}
				}
				// Update chance
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				ItemStack dchI = GUIUtils.getItem(inv, DCH_SLOT);
				ItemStack amuletI = GUIUtils.getItem(inv, AMULET_SLOT);
				if (item != null && dchI != null) {
					EItem rpgI = EItemUtils.fromItem(item);
					EDCH dch = EDCH.fromItem(dchI);
					ESMM amulet = ESMM.fromItem(amuletI);
					double chance = Utils.round(getChance(dch, amulet, rpgI.getLevel() + 1));
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}
				
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(AMULET_SLOT));
		items.add(inv.getItem(DCH_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Cường hóa thất bại bị giảm cấp");
		ItemStackUtils.addLoreLine(item, "§f2. §oSách an toàn §r§fvà §oSách may mắn §r§fgiúp không bị giảm");
		ItemStackUtils.addLoreLine(item, "§f3. §oSách may mắn §r§fcòn giúp tăng tỉ lệ thành công");
		
		return item;
	}
	
	private static double getChance(EDCH dch, ESMM sach, int nextLv) {
		if (nextLv >= 15) return 0;
		if (nextLv > dch.getMaxLevel()) return 0;
		
		double chance = 100;
		for (int i = dch.getLevel() + 1 ; i <= nextLv ; i++) {
			chance = (double) chance / 3;
		}
		
		int bonus = 0;
		if (sach != null) bonus = sach.getBonus(); 
		return Utils.round(Math.min(chance * ((double) (100 + bonus) / 100), 100));
	}
	
//	private static double getBaseChance(int nextLv) {
//		if (nextLv <= 0) return 100;
//		switch (nextLv) {
//		case 1: return 100;
//		case 2: return 12.5;
//		case 3: return 8.5;
//		case 4: return 6.5;
//		case 5: return 5;
//		case 6: return 4;
//		case 7: return 3;
//		case 8: return 2;
//		case 9: return 1;
//		case 10: return 0.8;
//		case 11: return 0.5;
//		case 12: return 0.1;
//		case 13: return 0.05;
//		case 14: return 0.025;
//		case 15: return 0.01;
//		}
//		return 0;
//	}
	
}

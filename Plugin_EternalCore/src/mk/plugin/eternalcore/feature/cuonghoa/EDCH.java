package mk.plugin.eternalcore.feature.cuonghoa;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.util.ItemStackUtils;

public enum EDCH {
	
	CAP_1("§7§lĐá cường hóa I", 1, Material.COAL_ORE, 3),
	CAP_2("§9§lĐá cường hóa II", 2, Material.IRON_ORE, 4),
	CAP_3("§c§lĐá cường hóa III", 3, Material.GOLD_ORE, 6),
	CAP_4("§6§lĐá cường hóa IV", 4,  Material.DIAMOND_ORE, 8),
	CAP_5("§3§lĐá cường hóa V", 5, Material.EMERALD_ORE, 20);
	
	private String name;
	private int level;
	private Material material;
	private int maxlevel;
	
	private EDCH(String name, int level, Material material, int maxlevel) {
		this.name = name;
		this.level = level;
		this.material = material;
		this.maxlevel = maxlevel;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public int getMaxLevel() {
		return this.maxlevel;
	}
	
	public Material getMaterial() {
		return this.material;
	}
	
	public ItemStack getItem() {
		ItemStack item = new ItemStack(this.getMaterial());
		ItemStackUtils.setDisplayName(item, this.getName());
		ItemStackUtils.addEnchantEffect(item);
		
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		String name = ItemStackUtils.getName(item);
		for (EDCH dch : values()) {
			if (name.contains(dch.getName())) return true;
		}
		return false;
	}
	
	public static EDCH fromItem(ItemStack item) {
		String name = ItemStackUtils.getName(item);
		for (EDCH dch : values()) {
			if (name.contains(dch.getName())) return dch;
		}
		return null;
	}
	
}

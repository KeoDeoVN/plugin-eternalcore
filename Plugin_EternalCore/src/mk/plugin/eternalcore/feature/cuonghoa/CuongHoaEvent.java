package mk.plugin.eternalcore.feature.cuonghoa;

import org.bukkit.entity.Player;
import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;
import org.bukkit.inventory.ItemStack;

public class CuongHoaEvent extends Event {

	private Player player;
	private ItemStack item;
	private int oldLevel;
	private int newLevel;
	
	public CuongHoaEvent(Player player, ItemStack item, int oldLevel, int newLevel) {
		this.player = player;
		this.item = item;
		this.oldLevel = oldLevel;
		this.newLevel = newLevel;
	}
	
	public ItemStack getItemStack() {
		return this.item;
	}
	
	public int getOldLevel() {
		return this.oldLevel;
	}
	
	public int getNewLevel() {
		return this.newLevel;
	}
	
	public Player getPlayer() {
		return this.player;
	}
	
	private static final HandlerList handlers = new HandlerList();
	
	public static HandlerList getHandlerList() {
		return handlers;
	}
	
	@Override
	public HandlerList getHandlers() {
		return handlers;
	}
	
}

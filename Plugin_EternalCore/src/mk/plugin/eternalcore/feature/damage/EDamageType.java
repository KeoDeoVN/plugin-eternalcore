package mk.plugin.eternalcore.feature.damage;

public enum EDamageType {
	
	DEFAULT("⚔ ", "§c§l§o"),
	CRITICAL("⚔ ", "§6§l§o"),
	TRUE("⚔ ", "§f§l§o");
	
	private String icon;
	private String prefix;
	
	private EDamageType(String icon, String prefix) {
		this.icon = icon;
		this.prefix = prefix;
	}
	
	public String getIcon() {
		return this.icon;
	}
	
	public String getPrefix() {
		return this.prefix;
	}
	
}

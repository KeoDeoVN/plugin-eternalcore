package mk.plugin.eternalcore.feature.chuyenhoa;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.eco.PointAPI;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ECHHGUI {
	
	public static final String TITLE = "§c§lCHUYỂN CẤP CƯỜNG HÓA";
	
	public static final List<Integer> ITEM_SLOTS = Arrays.asList(new Integer[] {11, 15});
	public static final int BUTTON_SLOT = 22;
	
	public static final int POINT = 49;
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			for (int i = 12 ; i <= 14 ; i ++) {
				inv.setItem(i, GUIUtils.getGreenSlot());
			}
			ITEM_SLOTS.forEach(i -> {
				inv.setItem(i, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.BLUE)),  "§aÔ chứa §oItem"));
			});
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aBấm để thực hiện", "§6Phí: §e" + POINT + "P"})));
			inv.setItem(35, getInfo());
		});
		
		player.openInventory(inv);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack item1 = inv.getItem(ITEM_SLOTS.get(0));
				ItemStack item2 = inv.getItem(ITEM_SLOTS.get(1));
				
				EItem rpgI1 = EItemUtils.fromItem(item1);
				EItem rpgI2 = EItemUtils.fromItem(item2);
				if (!EItemUtils.isEItem(item1) || !EItemUtils.isEItem(item2)) {
					player.sendMessage("§cKhông thể thực thi, item không đúng!");
					return;
				}
				
				// Fee
				if (!PointAPI.pointCost(player, POINT)) {
					player.sendMessage("§cBạn cần §e" + POINT + "P §cđể thực hiện");
					return;
				}
				
				// Do
				int lv1 = rpgI1.getLevel();
				rpgI1.setLevel(rpgI2.getLevel());
				rpgI2.setLevel(lv1);
				
				item1 = EItemUtils.update(player, item1, rpgI1);
				item2 = EItemUtils.update(player, item2, rpgI2);
				
				// Give
				Utils.giveItem(player, item1);
				Utils.giveItem(player, item2);
				// Set
				ITEM_SLOTS.forEach(i -> {
					inv.setItem(i, null);
				});
				// Close
				player.closeInventory();
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§7Chuyển cấp 2 vũ khí", 0, 40, 0);
				player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);
			}
		}
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOTS)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		ITEM_SLOTS.forEach(i -> {
			items.add(inv.getItem(i));
		});
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Đặt 2 vật phẩm");
		ItemStackUtils.addLoreLine(item, "§f2. Cấp độ 2 vật phẩm sẽ hoán đổi cho nhau");
		
		return item;
	}
	
}

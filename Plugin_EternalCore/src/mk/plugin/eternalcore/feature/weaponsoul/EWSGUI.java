package mk.plugin.eternalcore.feature.weaponsoul;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;

public class EWSGUI {
	
	public static final String TITLE = "§c§lLINH HỒN VŨ KHÍ";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, GUIUtils.getBlackSlot());
			inv.setItem(3, getIcon(player, Material.DIAMOND_SWORD, MCItemType.SWORD));
			inv.setItem(4, getIcon(player, Material.BOW, MCItemType.BOW));
			inv.setItem(5, getIcon(player, Material.GOLD_AXE, MCItemType.AXE));
		});
	}
	
	public static ItemStack getIcon(Player player, Material material, MCItemType it) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		int soul = ep.getSoul(it);
		
		ItemStack item = new ItemStack(material);
		ItemStackUtils.setDisplayName(item, "§6§l" + it.getName() + " §c§l" + soul + " hồn");
		ItemStackUtils.addLoreLine(item, "§a+" + EWeaponSoulUtils.getBuff(soul) + "% sát thương gây bởi " + it.getName().toLowerCase());
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}
	
}

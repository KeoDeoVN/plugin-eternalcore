package mk.plugin.eternalcore.feature.weaponsoul;

public class EWeaponSoulUtils {
	
	public static int getBuff(int soul) {
		double a = (double) 100 * soul / (soul + 100000);
		if (a != 0) return new Double(a + 1).intValue();
		return 0;
	}
	
}

package mk.plugin.eternalcore.feature.tinhluyen;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EDTLUtils {
	
	public static final int BASIC_VALUE = 15;
	
	public static final int MIN_CHANCE = 10;
	public static final int MAX_CHANCE = 50;
	
	public static int rate(ETier tier) {
		int min = getMinRate(tier);
		int max = getMaxRate(tier);
		return Utils.randomInt(min, max);
	}
	
	public static int getMinRate(ETier tier) {
		return new Double((double) BASIC_VALUE * 0.75 * tier.getMulti()).intValue();
	}
	
	public static int getMaxRate(ETier tier) {
		return new Double((double) BASIC_VALUE * 1.25 * tier.getMulti()).intValue();
	}
	
	
	
	public static ItemStack getDTLItem(EDTL dtl) {
		String name = Utils.randomName(3);
		ETier tier = dtl.getTier();
		EStat stat = dtl.getStat();
		int success = dtl.getChance();
		int fail = 100 - success;
		
		ItemStack item = new ItemStack(Material.EMERALD);
		ItemStackUtils.setDisplayName(item, tier.getColor() + "§l" + name + getLockedLore(dtl));
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oĐá tinh luyện");
		lore.add("§aPhẩm chất: §f" + tier.getColor() + tier.getName());
		lore.add("§aChỉ số: §f" + stat.getName());
		lore.add("§aGiá trị: §f" + getMinRate(tier) + " >> " + getMaxRate(tier));
		lore.add("§aTỉ lệ: §2" + success + "% Thành §f| §c" + fail + "% Bại");
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addEnchantEffect(item);
		
		item = ItemStackUtils.setTag(item, "edtl", dtl.toString());
		
		return item;
	}
	
	public static boolean isEDTL(ItemStack item) {
		return ItemStackUtils.hasTag(item, "edtl");
	}
	
	public static EDTL fromItem(ItemStack item) {
		if (!isEDTL(item)) return null;
		return EDTL.fromString(ItemStackUtils.getTag(item, "edtl"));
	}
	
	public static String getLockedLore(EDTL dtl) {
		return dtl.isLocked() ? EConfig.LOCKED_LINE : "";
	}
	
}

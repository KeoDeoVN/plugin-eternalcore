package mk.plugin.eternalcore.feature.tinhluyen;

import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ERDTLUtils {
	
	public static ItemStack getItem(ERDTL rdtl) {
		ETier tier = rdtl.getMaxTier();
		ItemStack item = new ItemStack(Material.EMERALD);
		ItemStackUtils.setDisplayName(item, tier.getColor() + "§lĐá tinh luyện ngẫu nhiên" + getLockedLore(rdtl));
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick chuột phải để dùng");
		
		lore.add("§aXác suất:");
		for (Entry<ETier, Integer> e : rdtl.getTierChances().entrySet()) {
			lore.add("  " + e.getKey().getColor() + e.getKey().getName() + ": §f" + e.getValue() + "%");
		}
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addEnchantEffect(item);
		
		item = ItemStackUtils.setTag(item, "erdtl", rdtl.toString());
		
		return item;
	}
	
	public static boolean isERDTL(ItemStack item) {
		return ItemStackUtils.hasTag(item, "erdtl");
	}
	
	public static ERDTL fromItem(ItemStack item) {
		if (!isERDTL(item)) return null;
		return ERDTL.fromString(ItemStackUtils.getTag(item, "erdtl"));
	}
	
	public static String getLockedLore(ERDTL dtl) {
		return dtl.isLocked() ? EConfig.LOCKED_LINE : "";
	}
	
	public static void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		ItemStack item = player.getInventory().getItemInMainHand();	
		if (item == null || item.getType() == Material.AIR) return;
		if (isERDTL(item)) {
			e.setCancelled(true);
			if (item.getAmount() == 1) player.getInventory().setItemInMainHand(null);
			else item.setAmount(item.getAmount() - 1);
			
			ERDTL rdtl = fromItem(item);
			ETier tier = rdtl.rateTier();
			
			EDTL dtl = new EDTL(tier, EStat.values()[Utils.randomInt(0, EStat.values().length - 1)], Utils.randomInt(EDTLUtils.MIN_CHANCE, EDTLUtils.MAX_CHANCE), true);
			Utils.giveItem(player, EDTLUtils.getDTLItem(dtl));
			player.sendMessage("§aSử dụng thành công!");
			Utils.sendSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
		}
	}
	
	
	
	
	
	
	
	
	
}

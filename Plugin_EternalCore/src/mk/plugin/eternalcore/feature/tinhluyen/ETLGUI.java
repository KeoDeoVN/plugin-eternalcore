package mk.plugin.eternalcore.feature.tinhluyen;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.feature.cuonghoa.ESMM;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ETLGUI {
	
	public static int ITEM_SLOT = 19;
	public static List<Integer> DTL_SLOTS = Arrays.asList(new Integer[] {11, 12, 13});
	public static int SMM_SLOT = 14;
	public static int BUTTON_SLOT = 31;
	public static int RESULT_SLOT = 25;
	public static int INFO_SLOT = 44;
	
	public static String TITLE = "§c§lTINH LUYỆN";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 45, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			DTL_SLOTS.forEach(i -> {
				inv.setItem(i, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.BLUE)),  "§aÔ chứa §oĐá tinh luyện"));
			});
			inv.setItem(SMM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.WHITE)), "§aÔ chứa §oSách may mắn §r§7(Không bắt buộc)"));
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN)), "§aÔ chứa §oVật phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.PINK)), "§aÔ chứa §oSản phẩm"));
			inv.setItem(INFO_SLOT, getInfo());
		});
		
		player.openInventory(inv);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				// Check Item
				ItemStack rpgI = GUIUtils.getItem(inv, ITEM_SLOT);
				if (rpgI == null) {
					player.sendMessage("§cKhông có Vật phẩm");
					return;
				}
				EItem rpgItem = EItemUtils.fromItem(rpgI);
	
				
				// Check DTL
				List<EDTL> gems = Lists.newArrayList();
				DTL_SLOTS.forEach(i -> {
					ItemStack item = GUIUtils.getItem(inv, i);
					if (item != null) {
						gems.add(EDTLUtils.fromItem(item));
					}
				});
				if (gems.size() == 0) {
					player.sendMessage("§cKhông có Đá tinh luyện");
					return;
				}
				
				// Check Amulet
				ItemStack amuletI = GUIUtils.getItem(inv, SMM_SLOT);
				int bonus = amuletI == null ? 0 : ESMM.fromItem(amuletI).getBonus();
				double chance = 100;
				for (EDTL gem : gems) {
					if (gem.getChance() < chance) chance = gem.getChance();
				}
				chance *= (1 + ((double) bonus / 100));
				
				// Get min tier
				ETier t = gems.get(0).getTier();
				for (EDTL dtl : gems) {
					if (dtl.getTier().getMulti() < t.getMulti()) {
						t = dtl.getTier();
					}
				}

				// Do
				if (Utils.rate(chance)) {
					// Success
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "§fTinh luyện thành công!", 0, 20, 0);
					player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);	
					Map<EStat, Integer> stats = new HashMap<EStat, Integer> ();
					for (EDTL gem : gems) {
						EStat stat = gem.getStat();
						int value = EDTLUtils.rate(gem.getTier());
						// Stat bonus via material
						MCItemType type = MCItemType.getType(rpgI.getType());
						if (type != null) {
							if (type.getStatBonus().containsKey(stat)) value *= 1 + type.getStatBonus().get(stat);
						}
						stats.put(stat, stats.getOrDefault(stat, 0) + value);

					}
					rpgItem.setStats(stats);
					rpgItem.setTier(t);
				} else {
					// Fail
					player.sendTitle("§7§lTHẤT BẠI T_T", "§fChúc bạn may mắn lần sau", 0, 20, 0);
					player.playSound(player.getLocation(), Sound.ENTITY_GHAST_SCREAM, 1, 1);
				}
				rpgI = EItemUtils.update(player, rpgI, rpgItem);
				
				// Give
				Utils.giveItem(player, rpgI);
				
				// Remove
				inv.setItem(SMM_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				DTL_SLOTS.forEach(i -> {
					inv.setItem(i, null);
				});
				
				// Close and reopen
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
					openGUI(player);
				}, 10);
				
				// Call Event
				Bukkit.getPluginManager().callEvent(new TinhLuyenEvent(player));
			}
		}
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				// Item
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				// DTL
				else if (EDTLUtils.isEDTL(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, DTL_SLOTS)) {
						player.sendMessage("§cĐã để hết Đá tinh luyện rồi");
						return;
					}
				}
				// AMULET
				else if (ESMM.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SMM_SLOT)) {
						player.sendMessage("§cĐã để Bùa tinh luyện rồi");
						return;
					}
				}
				
				// CHANCE UPDATE
				
				ItemStack rpgI = GUIUtils.getItem(inv, ITEM_SLOT);
				if (rpgI == null) return;
				EItem rpgItem = EItemUtils.fromItem(rpgI);
				if (rpgItem == null) {
					return;
				}
				
				// Check DTL
				List<EDTL> gems = Lists.newArrayList();
				DTL_SLOTS.forEach(i -> {
					ItemStack item = GUIUtils.getItem(inv, i);
					if (item != null) {
						gems.add(EDTLUtils.fromItem(item));
					}
				});
				if (gems.size() == 0) {
					return;
				}
				
				// Check Amulet
				ItemStack amuletI = GUIUtils.getItem(inv, SMM_SLOT);
				int bonus = amuletI == null ? 0 : ESMM.fromItem(amuletI).getBonus();
				double chance = 100;
				for (EDTL gem : gems) {
					if (gem.getChance() < chance) chance = gem.getChance();
				}
				chance *= (1 + ((double) bonus / 100));
				inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				
				// Update Result
				Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
					// Stats
					Map<EStat, Integer> stats = new HashMap<EStat, Integer> ();
					for (EDTL gem : gems) {
						EStat stat = gem.getStat();
						int value = EDTLUtils.getMaxRate(gem.getTier());
						stats.put(stat, stats.getOrDefault(stat, 0) + value);

					}
					
					// ...
					rpgItem.setStats(stats);
					ItemStack clone = EItemUtils.update(player, rpgI.clone(), rpgItem);
					rpgItem.setName(rpgItem.getName() + " (May mắn nhận được)");
					inv.setItem(RESULT_SLOT, clone);
				});
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		DTL_SLOTS.forEach(i -> {
			items.add(inv.getItem(i));
		});
		items.add(inv.getItem(SMM_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Mỗi loại vật phẩm sẽ tăng thêm chỉ số ");
		ItemStackUtils.addLoreLine(item, "§f  (Kiếm, cung, rìu, giáp, khiên)");
		ItemStackUtils.addLoreLine(item, "§f2. Ví dụ: §oKiếm §rtăng §oTốc đánh");
		ItemStackUtils.addLoreLine(item, "§f3. §oSách may mắn §r§fcòn giúp tăng tỉ lệ thành công");
		ItemStackUtils.addLoreLine(item, "§f3. §fĐược dùng tối đa 3 Đá tinh luyện");
		
		return item;
	}
}

package mk.plugin.eternalcore.feature.tinhluyen;

import java.util.Map;
import java.util.Map.Entry;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.Utils;

public class ERDTL {
	
	private Map<ETier, Integer> tierChances = Maps.newHashMap();
	private boolean isLocked;
	
	public ERDTL(Map<ETier, Integer> tierChances, boolean isLocked) {
		this.tierChances = tierChances;
		this.isLocked = isLocked;
	}
	
	public Map<ETier, Integer> getTierChances() {
		Map<String, Integer> to = to();
		to = Utils.reserveSort(to);
		return from(to);
	}
	
	private Map<String, Integer> to() {
		Map<String, Integer> m = Maps.newHashMap();
		this.tierChances.forEach((tier, chance) -> {
			m.put(tier.name(), chance);
		});
		return m;
	}
	
	private Map<ETier, Integer> from(Map<String, Integer> map) {
		Map<ETier, Integer> m = Maps.newLinkedHashMap();
		map.forEach((tier, chance) -> {
			m.put(ETier.valueOf(tier), chance);
		});
		return m;
	}
	
	public boolean isLocked() {
		return this.isLocked;
	}
	
	public ETier getMaxTier() {
		int max = 0;
		ETier result = null;
		
		// Check max
		for (Entry<ETier, Integer> e : this.tierChances.entrySet()) {
			ETier tier = e.getKey();
			int value = e.getValue();
			if (value > max) {
				max = value;
				result = tier;
			}
		}
		
		return result;
	}
	
	public ETier rateTier() {
		ETier result = getMaxTier();
		
		// Rate each one
		for (Entry<ETier, Integer> e : this.tierChances.entrySet()) {
			ETier tier = e.getKey();
			int value = e.getValue();
			if (Utils.rate(value)) result = tier;
		}
		
		return result;
	}
	
	public String toString() {
		Map<ETier, Integer> map = this.tierChances;
		String s = "";
		if (map.size() == 0) return s;
		for (Entry<ETier, Integer> e : map.entrySet()) {
			s += e.getKey().name() + ":" + e.getValue() + ":" + this.isLocked + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	public static ERDTL fromString(String s) {
		Map<ETier, Integer> map = Maps.newHashMap();
		if (s.length() == 0) return new ERDTL(map, true);
		boolean isLocked = true;
		for (String ss : s.split(";")) {
			ETier stat = ETier.valueOf(ss.split(":")[0]);
			int value = Integer.valueOf(ss.split(":")[1]);
			isLocked = Boolean.valueOf(ss.split(":")[2]);
			map.put(stat, value);
		}
		return new ERDTL(map, isLocked);
	}
	
	
}

package mk.plugin.eternalcore.feature.tinhluyen;

import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;

public class EDTL {
	
	private ETier tier;
	private EStat stat;
	private int chance;
	private boolean isLocked;
	
	public EDTL(ETier tier, EStat stat, int chance, boolean isLocked) {
		this.tier = tier;
		this.stat = stat;
		this.chance = chance;
		this.isLocked = isLocked;
	}

	public ETier getTier() {
		return tier;
	}

	public EStat getStat() {
		return stat;
	}

	public int getChance() {
		return chance;
	}
	
	public boolean isLocked() {
		return this.isLocked;
	}
	
	public String toString() {
		return tier.name() + ":" + stat.name() + ":" + chance + ":" + isLocked;
	}
	
	public static EDTL fromString(String s) {
		ETier tier = ETier.valueOf(s.split(":")[0]);
		EStat stat = EStat.valueOf(s.split(":")[1]);
		int chance = Integer.valueOf(s.split(":")[2]);
		boolean isLocked = Boolean.valueOf(s.split(":")[3]);
		return new EDTL(tier, stat, chance, isLocked);
	}
	
}

package mk.plugin.eternalcore.feature.power;

import org.bukkit.entity.Player;

import me.clip.placeholderapi.expansion.PlaceholderExpansion;

public class EPowerPlaceholder extends PlaceholderExpansion {

	@Override
	public String getAuthor() {
		return "MankaiStep";
	}

	@Override
	public String getIdentifier() {
		return "eternal";
	}

	@Override
	public String getVersion() {
		return "1.0";
	}
	
	@Override
    public String onPlaceholderRequest(Player player, String s){
		if (s.equalsIgnoreCase("playerpower")) {
			return "" + EPowerUtils.calculatePower(player);
		}
		return "";
	}

}

package mk.plugin.eternalcore.feature.power;

import org.bukkit.entity.Player;

import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class EPowerUtils {
	
	public static int calculatePower(Player player) {
		EPlayer rpgP = EPlayerDataUtils.getData(player);
		int power = 0; 
		if (rpgP == null) return power;
		for (EStat stat : EStat.values()) {
			power += rpgP.getStat(stat, player) * getPower(stat);
		}
		
		return power;
	}
	
	private static int getPower(EStat rpgStat) {
		int power = 1;
		switch (rpgStat) {
			case DAMAGE: power = 6; break;
			case HEALTH: power = 2; break;
			case DEFENSE: power = 5; break;
			case DODGE: power = 4; break;
			case HEAL: power = 4; break;
			case LIFESTEAL: power = 5; break;
			case ACCURACY: power = 6; break;
//			case COUNTER: power = 6; break;
			case ATTACKSPEED: power = 4; break;
		}
		
		return power * 54;
	}
	
}

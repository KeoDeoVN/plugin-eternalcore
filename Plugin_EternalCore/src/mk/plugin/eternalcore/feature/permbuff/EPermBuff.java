package mk.plugin.eternalcore.feature.permbuff;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.stat.EStat;

public class EPermBuff {
	
	public static Map<String, EPermBuff> permBuffs = new HashMap<String, EPermBuff> ();	
	
	public static void load(FileConfiguration config) {
		permBuffs.clear();
		config.getConfigurationSection("permbuff").getKeys(false).forEach(id -> {
			String perm = config.getString("permbuff." + id + ".permission");
			Map<EStat, Integer> buffs = Maps.newHashMap();
			config.getStringList("permbuff." + id + ".buff").forEach(s -> {
				EStat stat = EStat.valueOf(s.split(":")[0].toUpperCase());
				int value = Integer.parseInt(s.split(":")[1]);
				buffs.put(stat, value);
			});
			EPermBuff buff = new EPermBuff(perm, buffs);
			permBuffs.put(id, buff);
		});
	}
	
	private String permission;
	private Map<EStat, Integer> buffs = new HashMap<EStat, Integer> ();
	
	public EPermBuff(String permission, Map<EStat, Integer> buffs) {
		this.permission = permission;
		this.buffs = buffs;
	}
	
	public String getPermission() {
		return this.permission;
	}
	
	public Map<EStat, Integer> getBuffs() {
		return this.buffs;
	}
	
}

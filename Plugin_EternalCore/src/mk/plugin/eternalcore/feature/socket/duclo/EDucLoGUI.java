package mk.plugin.eternalcore.feature.socket.duclo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.eco.PointAPI;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EDucLoGUI {
	
	public static String TITLE = "§c§lĐỤC LỖ";
	
	private static int ITEM_SLOT = 1;
	private static int BUTTON_SLOT = 7;
	private static int POINT = 45;
	
	public static final int MAX_HOLE = 5;
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);

		Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aBấm để thực hiện", "§6Giá: §e" + POINT + "P"})));
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN)), "§aÔ chứa §oVật phẩm"));
		});

		player.openInventory(inv);
	}

	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (!EItemUtils.isEItem(item)) {
					player.sendMessage("§cVật phẩm không đúng");
					return;
				}
				EItem rpgI = EItemUtils.fromItem(item);
				if (rpgI.getSockets().size() + rpgI.getEmptySocketHole() >= MAX_HOLE) {
					player.sendMessage("§cSố lỗ tối đa");
					return;
				}
				if (!PointAPI.pointCost(player, POINT)) {
					player.sendMessage("§cTài sản của bạn không đủ để chi trả");
					return;
				}
				rpgI.addEmptySocketHole();
				
				item = EItemUtils.setItem(item, rpgI);
				EItemUtils.updateLore(player, item);

				Utils.giveItem(player, item);
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Đục lỗ thành công", 0, 15, 1);
				Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 0.7f, 1f);
				
				// Done
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
					openGUI(player);
				}, 10);
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
}

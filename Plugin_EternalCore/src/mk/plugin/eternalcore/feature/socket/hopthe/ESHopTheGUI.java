package mk.plugin.eternalcore.feature.socket.hopthe;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;


import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.socket.ESocketUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESHopTheGUI {
	
	public static List<Integer> SOCKET_SLOTS = Arrays.asList(new Integer[] {10, 12, 28, 30});
	public static int BUTTON_SLOT = 24;
	public static int RESULT_SLOT = 20;
	public static int INFO_SLOT = 44;
	
	public static String TITLE = "§c§lHỢP THỂ ĐÁ QUÝ";
	
	public static Inventory openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 45, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			player.playSound(player.getLocation(), Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			SOCKET_SLOTS.forEach(i -> {
				inv.setItem(i, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN)),  "§aÔ chứa §oĐá quý"));
			});
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b101%", "§6Click để thực hiện"})));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.PINK)), "§aÔ chứa §oSản phẩm"));
			inv.setItem(INFO_SLOT, getInfo());
		});
		
		player.openInventory(inv);
		return inv;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				
				// Check DTL
				List<ESocket> gems = new ArrayList<ESocket> ();
				SOCKET_SLOTS.forEach(i -> {
					ItemStack item = GUIUtils.getItem(inv, i);
					if (item != null) {
						gems.add(ESocketUtils.fromItem(item));
					}
				});
				if (gems.size() < 4) {
					player.sendMessage("§cPhải đủ 4 Đá quý!");
					return;
				}
				
				// Get min tier
				double minMulti = 100;
				ETier tier = ETier.COMMON;
				for (ESocket rpgG : gems) {
					if (rpgG.getTier().getMulti() < minMulti) {
						minMulti = rpgG.getTier().getMulti();
						tier = rpgG.getTier();
					}
				}
				
				// Do
				player.playSound(player.getLocation(), Sound.ENTITY_FIREWORK_LAUNCH, 1, 1);	
				ESocket socket = new ESocket(tier, EStat.values()[Utils.randomInt(0, EStat.values().length - 1)], Utils.randomInt(ESocketUtils.getMinRate(tier), ESocketUtils.getMaxRate(tier)), Utils.randomInt(ESocketUtils.MIN_CHANCE, ESocketUtils.MAX_CHANCE), true);
				ItemStack result = ESocketUtils.getSocketItem(socket);
				
				// Give
				Utils.giveItem(player, result);
				player.sendMessage("§aNhận được §r" + ItemStackUtils.getName(result));
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "", 0, 20, 0);
				
				// Remove
				inv.setItem(RESULT_SLOT, result);
				SOCKET_SLOTS.forEach(i -> {
					inv.setItem(i, null);
				});
				
				// Close and reopen
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
					Inventory newInv = openGUI(player);
					newInv.setItem(RESULT_SLOT, result);
				}, 10);
			}
		}
		else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				// DTL
				if (ESocketUtils.isESocket(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SOCKET_SLOTS)) {
						player.sendMessage("§cĐã để hết Đá quý rồi");
						return;
					}
				}
				
				// Update Result
				Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
					inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.PINK)), "§aÔ chứa §oSản phẩm"));
				});
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) { 
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		SOCKET_SLOTS.forEach(i -> {
			items.add(inv.getItem(i));
		});
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
	private static ItemStack getInfo() {
		ItemStack item = new ItemStack(Material.PAPER);
		ItemStackUtils.setDisplayName(item, "§6§lHƯỚNG DẪN");
		ItemStackUtils.addLoreLine(item, "§f1. Hợp thể 4 Đá quý");
		ItemStackUtils.addLoreLine(item, "§f  sẽ ra ngẫu nhiên 1 đá quý khác");
		ItemStackUtils.addLoreLine(item, "§f  với loại chỉ số ngẫu nhiên");
		
		return item;
	}
	
}

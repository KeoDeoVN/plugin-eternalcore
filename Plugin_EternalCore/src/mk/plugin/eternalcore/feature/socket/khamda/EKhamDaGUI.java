package mk.plugin.eternalcore.feature.socket.khamda;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.feature.cuonghoa.ESMM;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.socket.ESocketUtils;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EKhamDaGUI {
	
	public static final int ITEM_SLOT = 10;
	public static final int SOCKET_SLOT = 11;
	public static final int BUTTON_SLOT = 23;
	public static final int RESULT_SLOT = 16;
	public static final int SMM_SLOT = 12;
	
	public static final String TITLE = "§c§lKHẢM ĐÁ";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 36, TITLE);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN)), "§aÔ chứa §oVật phẩm"));
			inv.setItem(SOCKET_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.PINK)), "§aÔ chứa §oĐá quý"));
			inv.setItem(RESULT_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.WHITE)), "§aÔ chứa §oSản phẩm"));
			inv.setItem(SMM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.RED)), "§aÔ chứa §oSách may mắn"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b0%", "§6Click để thực hiện"})));
		});
		player.openInventory(inv);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack gemItem = GUIUtils.getItem(inv, SOCKET_SLOT);
				ItemStack itemItem = GUIUtils.getItem(inv, ITEM_SLOT);
				
				// Check
				if (!EItemUtils.isEItem(itemItem)) {
					player.sendMessage("§cChưa đặt Vật phẩm");
					return;
				}
				if (!ESocketUtils.isESocket(gemItem)) {
					player.sendMessage("§cChưa đặt Đá");
					return;
				}
				
				// Do
				EItem rpgI = EItemUtils.fromItem(itemItem);
				ESocket socket = ESocketUtils.fromItem(gemItem);
				
				if (rpgI.getEmptySocketHole() <= 0) {
					player.sendMessage("§cĐã full slot đá!");
					return;
				}
				
				double chance = socket.getChance();
				
				ItemStack amuletI = GUIUtils.getItem(inv, SMM_SLOT);
				int bonus = amuletI == null ? 0 : ESMM.fromItem(amuletI).getBonus();
				chance *= (1 + ((double) bonus / 100));
				
				if (Utils.rate(chance)) {
					rpgI.setSocket(socket);
					player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Ép gem thành công", 0, 15, 1);
					Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 0.7f, 1f);
				} else {
					player.sendTitle("§7§lTHẤT BẠI T_T", "Chúc bạn may mắn lần sau", 0, 15, 1);
					Utils.sendSound(player, Sound.ENTITY_GHAST_SCREAM, 0.7f, 1f);
				}
				
				// Give
				itemItem = EItemUtils.setItem(itemItem, rpgI);
				Utils.giveItem(player, itemItem);
				
				// Clear
				inv.setItem(SOCKET_SLOT, null);
				inv.setItem(ITEM_SLOT, null);
				inv.setItem(SMM_SLOT, null);
				player.closeInventory();
				Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
					openGUI(player);
				}, 10);
				
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
				
				if (ESocketUtils.isESocket(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SOCKET_SLOT)) {
						player.sendMessage("§cĐã để Đá rồi");
						return;
					}
				}
				
				// AMULET
				else if (ESMM.isThatItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, SMM_SLOT)) {
						player.sendMessage("§cĐã để Bùa tinh luyện rồi");
						return;
					}
				}
				
				// Update button
				ESocket socket = ESocketUtils.fromItem(GUIUtils.getItem(inv, SOCKET_SLOT));
				if (socket != null) {
					double chance = socket.getChance();
					ItemStack amuletI = GUIUtils.getItem(inv, SMM_SLOT);
					int bonus = amuletI == null ? 0 : ESMM.fromItem(amuletI).getBonus();
					chance *= (1 + ((double) bonus / 100));
					inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aTỉ lệ thành công: §b" + chance + "%", "§6Click để thực hiện"})));
				}

				// Update result
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				if (item != null && socket != null) {
					ItemStack clone = item.clone();
					ItemStackUtils.setDisplayName(clone, ItemStackUtils.getName(clone) + " §e(Sản phẩm)");
					EItem rpgI = EItemUtils.fromItem(clone);
					if (rpgI.getEmptySocketHole() > 0) {
						rpgI.setSocket(socket);
					}
					inv.setItem(RESULT_SLOT, EItemUtils.setItem(clone, rpgI));
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(SOCKET_SLOT));
		items.add(inv.getItem(SMM_SLOT));
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
}

package mk.plugin.eternalcore.feature.socket.tach;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.eco.PointAPI;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.socket.ESocketUtils;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESocketSplitGUI {
	
	public static int ITEM_SLOT = 11;
	public static int BUTTON_SLOT = 15;                                                                
	
	public static String TITLE = "§c§lTÁCH ĐÁ QUÝ";
	
	public static final int POINT = 49;
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 27, TITLE);	
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			Utils.sendSound(player, Sound.BLOCK_ENDERCHEST_OPEN, 1, 1);
			for (int i = 0 ; i < inv.getSize() ;  i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(ITEM_SLOT, GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.GREEN)), "§aÔ chứa §oVật phẩm"));
			inv.setItem(BUTTON_SLOT, GUIUtils.getButton(Arrays.asList(new String[] {"§aClick để thực hiện","§6Phí: §e" + POINT + "P", "§6Nhớ để thừa ô trống trong kho đồ!"})));
		});
		player.openInventory(inv);
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getClickedInventory() == null) return;
		Inventory inv = e.getWhoClicked().getOpenInventory().getTopInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		e.setCancelled(true);
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		// Action on top inv
		if (e.getClickedInventory() == player.getOpenInventory().getTopInventory()) {
			if (slot == BUTTON_SLOT) {
				ItemStack item = GUIUtils.getItem(inv, ITEM_SLOT);
				EItem rpgItem = EItemUtils.fromItem(item);
				if (rpgItem == null) {
					player.sendMessage("§cVật phẩm không đúng");
					return;
				}
				
				// Check
				if (rpgItem.getSockets().size() == 0) {
					player.sendMessage("§cVật phẩm không có đá quý!");
					return;
				}
				
				if (!PointAPI.pointCost(player, POINT)) {
					player.sendMessage("§cBạn cần §e" + POINT + "P §cđể thực hiện");
					return;
				}
				
				for (ESocket s : Lists.newArrayList(rpgItem.getSockets())) {
					ItemStack si = ESocketUtils.getSocketItem(s);
					Utils.giveItem(player, si);
				}
				
				rpgItem.clearSockets();
				item = EItemUtils.update(player,item, rpgItem);
				Utils.giveItem(player, item);
				
				// Done
				inv.setItem(ITEM_SLOT, null);
				player.closeInventory();
				player.sendTitle("§a§lTHÀNH CÔNG ^_^", "Tách gem thành công", 0, 40, 1);
				Utils.sendSound(player, Sound.ENTITY_FIREWORK_LAUNCH, 0.7f, 1f);
			}
		} else {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				Utils.sendSound(player, Sound.BLOCK_LEVER_CLICK, 1f, 1f);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				
				// Place 
				if (EItemUtils.isEItem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, inv, bInv, ITEM_SLOT)) {
						player.sendMessage("§cĐã để Vật phẩm rồi");
						return;
					}
				}
			} else player.sendMessage("§cẤn §fShift + Chuột trái §cđể đặt item");
		}
	}
	
	public static void eventClose(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		
		
		Inventory inv = e.getInventory();
		if (!e.getView().getTitle().equals(TITLE)) return;
		
		if (!GUIUtils.checkCheck(player)) return;
		
		List<ItemStack> items = new ArrayList<ItemStack> ();
		items.add(inv.getItem(ITEM_SLOT));
		
		items.forEach(item -> {
			if (item == null || ItemStackUtils.hasTag(item, GUIUtils.SLOT_ITEM_TAG)) return;
			Utils.giveItem(player, item);
		});
	}
	
}

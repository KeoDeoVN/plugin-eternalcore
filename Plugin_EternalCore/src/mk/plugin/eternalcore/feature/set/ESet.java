package mk.plugin.eternalcore.feature.set;

import java.util.List;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.feature.buff.EStatBuff;

public class ESet {
	
	private String name;
	private List<String> items = Lists.newArrayList();
	private List<List<EStatBuff>> buffs = Lists.newArrayList();
	private List<String> descs = Lists.newArrayList();
	
	public ESet(String name, List<String> items, List<List<EStatBuff>> buffs, List<String> descs) {
		this.name = name;
		this.items = items;
		this.buffs = buffs;
		this.descs = descs;
	}
	
	public String getName() {
		return this.name;
	}
	
	public List<String> getItems() {
		return this.items;
	}
	
	public List<String> getDescs() {
		return this.descs;
	}
	
	public List<List<EStatBuff>> getBuffs() {
		return this.buffs;
	}
	
	public List<EStatBuff> getBuffs(int amount) {
		if (amount <= 1 || amount > 5) return Lists.newArrayList();
		return buffs.get(amount - 2);
	}
	
	public String getDesc(int amount) {
		return getDescs().get(amount - 2);
	}
	
	
}

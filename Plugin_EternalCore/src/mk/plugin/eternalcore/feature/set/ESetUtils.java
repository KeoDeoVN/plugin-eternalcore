package mk.plugin.eternalcore.feature.set;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESetUtils {
	
	public static List<String> getItems(Player player) {
		List<String> list = Lists.newArrayList();
		Utils.getItemsEquiped(player).forEach(item -> {
			if (EItemUtils.isEItem(item)) list.add(EItemUtils.fromItem(item).getName());
		});
		return list;
	}
	
	public static boolean isEquip(Player player, String item) {
		return getItems(player).contains(item);
	}
	
	public static Map<ESet, Integer> getSets(Player player) {
		List<String> items = getItems(player);
		Map<ESet, Integer> map = Maps.newHashMap();
		
		EConfig.SETS.forEach(set -> {
			int count = 0;
			for (String item : items) {
				if (set.getItems().contains(item)) count++;
			}
			if (count > 1) map.put(set, count);
		});
		
		return map;
	}
	
	public static int getAmount(Player player, ESet set) {
		return getSets(player).getOrDefault(set, 1);
	}
	
	public static boolean isInAnySet(String name) {
		for (ESet set : EConfig.SETS) {
			if (set.getItems().contains(name)) return true;
		}
		return false;
	}
	
	public static ESet getSetFromItem(String name) {
		for (ESet set : EConfig.SETS) {
			if (set.getItems().contains(name)) return set;
		}
		return null;
	}
	
}

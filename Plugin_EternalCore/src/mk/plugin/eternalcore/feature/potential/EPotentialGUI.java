package mk.plugin.eternalcore.feature.potential;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.feature.infogui.EInfoGUI;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerStatUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EPotentialGUI {
	
	public static final String TITLE = "§2§lTIỀM NĂNG";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			getSlots().forEach((i, p) -> {
				inv.setItem(i, getItem(p));
			});
			inv.setItem(8, EInfoGUI.getStatInfo(player));
		});
	}
	
	public static Map<Integer, EPotential> getSlots() {
		Map<Integer, EPotential> map = new HashMap<Integer, EPotential> ();
		map.put(3, EPotential.SUC_MANH);
		map.put(4, EPotential.THE_LUC);
		map.put(5, EPotential.NHANH_NHEN);
		
		return map;
	}
	
	public static Map<EPotential, Material> getIcons() {
		Map<EPotential, Material> map = new HashMap<EPotential, Material> ();
		map.put(EPotential.SUC_MANH, Material.IRON_SWORD);
		map.put(EPotential.THE_LUC, Material.IRON_CHESTPLATE);
		map.put(EPotential.NHANH_NHEN, Material.IRON_BOOTS);
		
		return map;
	}
	
	public static ItemStack getItem(EPotential p) {
		Material material = getIcons().get(p);
		ItemStack item = new ItemStack(material, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§l+1 " + p.getName());
		ItemStackUtils.addLoreLine(item, "§7§m              §r");
		p.getStats().forEach((stat, value) -> {
			ItemStackUtils.addLoreLine(item, "§a" + stat.getName() + " §f+" + value);
		});
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);

		Inventory inv = e.getInventory();
		Player player = (Player) e.getWhoClicked();
		player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
		
		EPlayer rpgP = EPlayerDataUtils.getData(player);
		Map<Integer, EPotential> slots = getSlots();
		int slot = e.getSlot();
		if (slots.containsKey(slot)) {
			int remain = EPlayerStatUtils.getRemainPotentials(player);
			if (remain <= 0) {
				player.sendMessage("§cKhông còn điểm dư");
				return;
			}
			EPotential po = slots.get(slot);
			int point = rpgP.getPotential(po);
			point++;
			rpgP.setPotential(po, point);
			
			// Update stat
			Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
				EPlayerStatUtils.updateStats(player);
				inv.setItem(8, EInfoGUI.getStatInfo(player));
			});
		}
	}
	
}

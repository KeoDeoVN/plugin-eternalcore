package mk.plugin.eternalcore.feature.potential;

import java.util.HashMap;
import java.util.Map;

import mk.plugin.eternalcore.stat.EStat;

public enum EPotential {
	
	SUC_MANH("Sức mạnh") {
		@Override
		public Map<EStat, Integer> getStats() {
			Map<EStat, Integer> map = new HashMap<EStat, Integer> ();
			map.put(EStat.DAMAGE, 2);
			
			return map;
		}
	},
	
	THE_LUC("Thể lực") {
		@Override
		public Map<EStat, Integer> getStats() {
			Map<EStat, Integer> map = new HashMap<EStat, Integer> ();
			map.put(EStat.HEALTH, 1);
			map.put(EStat.HEAL, 1);
			
			return map;
		}
	},
	
	KHEO_LEO("Khéo léo") {
		@Override
		public Map<EStat, Integer> getStats() {
			Map<EStat, Integer> map = new HashMap<EStat, Integer> ();
			map.put(EStat.DODGE, 1);
			map.put(EStat.ACCURACY, 1);
			
			return map;
		}
	},
	
	NHANH_NHEN("Nhanh nhẹn") {
		@Override
		public Map<EStat, Integer> getStats() {
			Map<EStat, Integer> map = new HashMap<EStat, Integer> ();
			map.put(EStat.ATTACKSPEED, 1);
			map.put(EStat.LIFESTEAL, 1);
			
			return map;
		}
	}
	;
	
	public abstract Map<EStat, Integer> getStats();
	
	private String name;
	
	private EPotential(String name) {
		this.name = name;
	}
	
	public String getName() {
		return this.name;
	}
	
}

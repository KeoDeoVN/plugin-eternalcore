package mk.plugin.eternalcore.feature.buff;

import mk.plugin.eternalcore.stat.EStat;

public class EStatBuff {
	
	private EStat stat;
	private int value;
	
	private long start;
	private long time;
	
	public EStatBuff(EStat stat, double value, long start, long time) {
		this.stat = stat;
		this.value = new Double(value).intValue();
		this.start = start;
		this.time = time;
	}
	
	public EStat getStat() {
		return this.stat;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public long getStart() {
		return this.start;
	}
	
	public void addTime(long time) {
		this.time += time;
	}
	
	public long getTime() {
		return this.time;
	}
	
	public boolean isStillEffective() {
		return this.start + this.time > System.currentTimeMillis();
	}
	
	public int getSecondsRemain() {
		return new Double((this.time - (System.currentTimeMillis() - this.start)) / 1000).intValue();
	}
	
}

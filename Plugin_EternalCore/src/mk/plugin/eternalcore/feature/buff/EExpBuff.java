package mk.plugin.eternalcore.feature.buff;

public class EExpBuff {
	
	// Percent
	private int value;
	
	private long start;
	private long time;
	
	public EExpBuff(int value, long start, long time) {
		this.value = value;
		this.start = start;
		this.time = time;
	}
	
	public int getValue() {
		return this.value;
	}
	
	public long getStart() {
		return this.start;
	}
	
	public long getTime() {
		return this.time;
	}
	
	public void addTime(long time) {
		this.time += time;
	}
	
	public boolean isStillEffective() {
		return this.start + this.time > System.currentTimeMillis();
	}
	
	public int getSecondsRemain() {
		return new Double((this.time - (System.currentTimeMillis() - this.start)) / 1000).intValue();
	}
	
}

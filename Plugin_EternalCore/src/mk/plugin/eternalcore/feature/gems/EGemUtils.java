package mk.plugin.eternalcore.feature.gems;

import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.util.ItemStackUtils;

public class EGemUtils {
	
	public static boolean isGem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "isGem");
	}
	
	public static ItemStack setGemTag(ItemStack item) { 
		return ItemStackUtils.setTag(item, "isGem", "");
	}
	
	public static ItemStack removeGemTag(ItemStack item) {
		return ItemStackUtils.removeTag(item, "isGem");
	}
	
}

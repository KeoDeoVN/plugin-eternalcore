package mk.plugin.eternalcore.feature.gems;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.DyeColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.ClickType;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EGemGUI {
	
	public static final List<Integer> GEM_LIST = Lists.newArrayList(1, 3, 5, 7);
	
	public static final String TITLE = "§2§lTÚI NGỌC";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) inv.setItem(i, GUIUtils.getBlackSlot());
			GEM_LIST.forEach(i -> inv.setItem(i, getSlot()));
			EPlayer ep = EPlayerDataUtils.getData(player);
			ep.getGems().forEach((s, i) -> inv.setItem(s, i));
		});
	}
	
	@SuppressWarnings("deprecation")
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().equals(TITLE)) return;	
		Player player = (Player) e.getWhoClicked();
		int slot = e.getSlot();
		
		if (e.getClickedInventory() == e.getWhoClicked().getOpenInventory().getTopInventory()) {
			e.setCancelled(true);
			if (GEM_LIST.contains(slot)) {
				ItemStack current = e.getCurrentItem();
				ItemStack cursor = e.getCursor();
				
				EPlayer ep = EPlayerDataUtils.getData(player);
				
				if (EGemUtils.isGem(cursor)) {
					if (GUIUtils.isItemSlot(current)) {
						e.setCursor(null);
					} else 	e.setCursor(current);
					e.setCurrentItem(cursor);
					ep.setGem(slot, cursor);
				}
				else {
					if (EGemUtils.isGem(current)) {
						e.setCursor(current);
						e.setCurrentItem(getSlot());
						ep.getGems().remove(slot);
					}
				}
			}
		}
		

		if (e.getClickedInventory() == e.getView().getBottomInventory()) {
			if (e.getClick() == ClickType.SHIFT_LEFT) {
				e.setCancelled(true);
				player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
				Inventory bInv = player.getOpenInventory().getBottomInventory();
				ItemStack clickedItem = bInv.getItem(slot);
				if (EGemUtils.isGem(clickedItem)) {
					if (!GUIUtils.placeItem(clickedItem, slot, e.getView().getTopInventory(), bInv, GEM_LIST)) {
						player.sendMessage("§cĐã để Ngọc rồi");
						return;
					}
				}
			}
		}
		

	}
	
	public static ItemStack getSlot() {
		ItemStack item = GUIUtils.getItemSlot(new ItemStack(Material.STAINED_GLASS_PANE, 1, Utils.getColor(DyeColor.BLUE)), "§a§lÔ chứa ngọc");
		ItemStackUtils.addLoreLine(item, "§7§oShift click để đặt nhanh");
		return item;
	}
	
	
	
	
}

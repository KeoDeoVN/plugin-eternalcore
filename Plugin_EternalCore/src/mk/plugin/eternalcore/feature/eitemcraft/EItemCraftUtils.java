package mk.plugin.eternalcore.feature.eitemcraft;

import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;

public class EItemCraftUtils {
	
	public static ItemStack getBasicAxe(Player player, Material m) {
		ItemStack item = new ItemStack(m);
		Map<EStat, Integer> stats = Maps.newHashMap();
		stats.put(EStat.DAMAGE, 7);
		stats.put(EStat.ATTACKSPEED, 3);
		EItem ei = new EItem("Rìu cơ bản", 0, true, ETier.COMMON, 2, stats, Maps.newHashMap(), Maps.newHashMap(), Lists.newArrayList());
		return EItemUtils.update(player, item, ei);
	}
	
	public static ItemStack getBasicSword(Player player, Material m) {
		ItemStack item = new ItemStack(m);
		Map<EStat, Integer> stats = Maps.newHashMap();
		stats.put(EStat.DAMAGE, 5);
		stats.put(EStat.ATTACKSPEED, 5);
		EItem ei = new EItem("Kiếm cơ bản", 0, true, ETier.COMMON, 2, stats, Maps.newHashMap(), Maps.newHashMap(), Lists.newArrayList());
		return EItemUtils.update(player, item, ei);
	}
	
	public static ItemStack getBasicBow(Player player, Material m) {
		ItemStack item = new ItemStack(m);
		Map<EStat, Integer> stats = Maps.newHashMap();
		stats.put(EStat.DAMAGE, 6);
		stats.put(EStat.ATTACKSPEED, 4);
		EItem ei = new EItem("Cung cơ bản", 0, true, ETier.COMMON, 2, stats, Maps.newHashMap(), Maps.newHashMap(), Lists.newArrayList());
		return EItemUtils.update(player, item, ei);
	}
	
	public static ItemStack getBasicShield(Player player, Material m) {
		ItemStack item = new ItemStack(m);
		Map<EStat, Integer> stats = Maps.newHashMap();
		stats.put(EStat.DEFENSE, 7);
		EItem ei = new EItem("Khiên cơ bản", 0, true, ETier.COMMON, 2, stats, Maps.newHashMap(), Maps.newHashMap(), Lists.newArrayList());
		return EItemUtils.update(player, item, ei);
	}
	
	public static ItemStack getBasicArmor(Player player, Material m) {
		ItemStack item = new ItemStack(m);
		Map<EStat, Integer> stats = Maps.newHashMap();
		stats.put(EStat.DEFENSE, 5);
		stats.put(EStat.HEALTH, 2);
		EItem ei = new EItem("Giáp cơ bản", 0, true, ETier.COMMON, 2, stats, Maps.newHashMap(), Maps.newHashMap(), Lists.newArrayList());
		return EItemUtils.update(player, item, ei);
	}
	
}

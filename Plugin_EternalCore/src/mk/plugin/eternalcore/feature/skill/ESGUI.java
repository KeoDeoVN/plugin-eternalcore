package mk.plugin.eternalcore.feature.skill;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerStatUtils;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESGUI {
	
	public static final String TITLE = "§c§lBẢNG KỸ NĂNG";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 54, TITLE + " (" + EPlayerStatUtils.getRemainSkills(player) + " ĐIỂM DƯ)");
		player.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			
			// Active Skills
			EPlayer ep = EPlayerDataUtils.getData(player);
			int c = 0;
			for (int i = 10 ; i <= 37  ; i+=9) {
				if (ep.getActiveSkills().containsKey(c)) {
					inv.setItem(i, getASkill(c, ep.getActiveSkills().get(c), player));
				}
				else inv.setItem(i, getNullSlot(c));
				c++;
			}
			
			// Unactive Skills
			getUSkillSlots().forEach((i, s) -> {
				inv.setItem(i, getUSkill(s, player));
			});
		});
	}
	
	private static Map<Integer, ESkill> getUSkillSlots() {
		Map<Integer, ESkill> map = Maps.newHashMap();
		map.put(14, ESkill.QUET_KIEM);
		map.put(23, ESkill.LIEN_KIEM);
		map.put(32, ESkill.BAO_KIEM);
		map.put(41, ESkill.CUONG_PHONG);
		
		map.put(15, ESkill.LIEN_TIEN);
		map.put(24, ESkill.TAN_XA_TIEN);
		map.put(33, ESkill.BOM_TIEN);
		map.put(42, ESkill.MUA_TIEN);
		
		map.put(16, ESkill.GAO_THET);
		map.put(25, ESkill.RIU_SET);
		map.put(34, ESkill.XONG_PHA);
		map.put(43, ESkill.TRUNG_PHAT);
		
		return map;
	}
	
	public static ItemStack getNullSlot(int index) {
		ItemStack item = new ItemStack(Material.BARRIER);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§c§lChưa trang bị");
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		item.setItemMeta(meta);
		item.setAmount(index + 1);
		return item;
	}
	
	public static ItemStack getASkill(int index, ESkill skill, Player player) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		ItemStack item = new ItemStack(Material.ENCHANTED_BOOK);
		ItemMeta meta = item.getItemMeta();
		
		meta.setDisplayName("§6§lKỹ năng §f§l" + skill.getName() + " Lv." + ep.getSkillPoint(skill));
		
		List<String> lore = getBasicLore(skill, player);
		lore.add("§cCombo: §f" + ESkillUtils.comboToString(ESkillUtils.getCombos().get(index).getCombo()));
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(meta);
		item.setAmount(index + 1);
		
		return item;
	}
	
	public static List<String> getBasicLore(ESkill skill, Player player) {
		List<String> lore = Lists.newArrayList();
		lore.add("§aYêu cầu");
		lore.add(" §7Level " + skill.getLevelRequirement());
		if (skill.getItemRequirement() != null) lore.add(" §7" + skill.getItemRequirement().getName());
		lore.add("§aHồi chiêu: §7" + skill.getCooldown() + "s");
		lore.add("§aMô tả");
		lore.addAll(Utils.toList(skill.getDesc(player), 30, " §7"));
		
		return lore;
	}
	
	public static ItemStack getUSkill(ESkill skill, Player player) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		Material m = Material.BOOK;
		
		if (ep.getActiveSkills().containsValue(skill)) m = Material.ENCHANTED_BOOK;
		if (skill.getLevelRequirement() > player.getLevel()) m = Material.KNOWLEDGE_BOOK;
		
		ItemStack item = new ItemStack(m);
		ItemMeta meta = item.getItemMeta();
		meta.setDisplayName("§6§lKỹ năng §f§l" + skill.getName() + " Lv." + ep.getSkillPoint(skill));
		
		List<String> lore = getBasicLore(skill, player);
		
		lore.add("");
		lore.add("§bClick Giữa để cộng điểm");
		lore.add("§bClick Trái để trang bị ô số 1");
		lore.add("§bClick Phải để trang bị ô số 2");
		lore.add("§bClick S.Trái để trang bị ô số 3");
		lore.add("§bClick S.Phải để trang bị ô số 4");
		
		meta.setLore(lore);
		meta.addItemFlags(ItemFlag.HIDE_ATTRIBUTES);
		meta.addItemFlags(ItemFlag.HIDE_ENCHANTS);
		item.setItemMeta(meta);
		return item;
	}
	
	public static void onClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);
		if (e.getClickedInventory() != e.getWhoClicked().getOpenInventory().getTopInventory()) return;
		
		Player player = (Player) e.getWhoClicked();
		player.playSound(player.getLocation(), Sound.BLOCK_LEVER_CLICK, 1, 1);
		
		Map<Integer, ESkill> m = getUSkillSlots();
		int slot = e.getSlot();
		if (m.containsKey(slot)) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			ESkill s = m.get(slot);
			int ss = -1;
			switch (e.getClick()) {
			case LEFT:
				ss = 0;
				break;
			case RIGHT:
				ss = 1;
				break;
			case SHIFT_LEFT:
				ss = 2;
				break;
			case SHIFT_RIGHT:
				ss = 3;
				break;
			case MIDDLE:
				if (EPlayerStatUtils.getRemainSkills(player) > 0) {
					ep.addSkilPoint(s, 1);
					player.sendMessage("§a+1 điểm kỹ năng " + s.getName());
				}
				break;
			default:
				break;
			}
			
			if (ss != -1) {
				ep.setActiveSkill(s, ss);
			}
			
			openGUI(player);
		}
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
}

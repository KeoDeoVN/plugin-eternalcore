package mk.plugin.eternalcore.feature.skill;

import org.bukkit.entity.Player;

import mk.plugin.eternalcore.feature.skill.list.SkillBaoKiem;
import mk.plugin.eternalcore.feature.skill.list.SkillChemLan;
import mk.plugin.eternalcore.feature.skill.list.SkillCuongPhong;
import mk.plugin.eternalcore.feature.skill.list.SkillGaoThet;
import mk.plugin.eternalcore.feature.skill.list.SkillLienHoanChem;
import mk.plugin.eternalcore.feature.skill.list.SkillLienTien;
import mk.plugin.eternalcore.feature.skill.list.SkillMuaTen;
import mk.plugin.eternalcore.feature.skill.list.SkillRiuSet;
import mk.plugin.eternalcore.feature.skill.list.SkillTanXaTien;
import mk.plugin.eternalcore.feature.skill.list.SkillTenNo;
import mk.plugin.eternalcore.feature.skill.list.SkillTrungPhat;
import mk.plugin.eternalcore.feature.skill.list.SkillXungPhong;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public enum ESkill {
	
	QUET_KIEM("Quét kiếm", 0, MCItemType.SWORD, 5, new SkillChemLan()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 100 + 10 * level;
			damage *= m / 100;
			
			return "Chém một vòng tròn, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	LIEN_KIEM("Liên kiếm", 5, MCItemType.SWORD, 8, new SkillLienHoanChem()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 70 + 5 * level;
			damage *= m / 100;
			
			return "Chém 5 lần liên tục, mỗi lần gây " + damage + " sát thương (" + m + "%)";
		}
	},
	BAO_KIEM("Bão kiếm", 10, MCItemType.SWORD, 10, new SkillBaoKiem()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 150 + 10 * level;
			damage *= m / 100;
			
			return "Tạo cơn lốc hất bay kẻ địch, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	CUONG_PHONG("Cuồng phong", 15, MCItemType.SWORD, 15, new SkillCuongPhong()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 200 + 10 * level;
			damage *= m / 100;
			
			return "Tạo 5 cơn lốc lớn về phía trước, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	
	
	LIEN_TIEN("Liên tiễn", 0, MCItemType.BOW, 5, new SkillLienTien()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 100 + 10 * level;
			damage *= m / 100;
			
			return "Bắn 5 tên liên tục, mỗi cái gây " + damage + " sát thương (" + m + "%)";
		}
	},
	TAN_XA_TIEN("Tán xạ tiễn", 5, MCItemType.BOW, 8, new SkillTanXaTien()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 120 + 10 * level;
			damage *= m / 100;
			
			return "Bắn ra một lúc 9 mũi tên, mỗi cái gây " + damage + " sát thương (" + m + "%)";
		}
	},
	BOM_TIEN("Bom tiễn", 10, MCItemType.BOW, 10, new SkillTenNo()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 150 + 10 * level;
			damage *= m / 100;
			
			return "Bắn ra tên làm nổ, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	MUA_TIEN("Mưa tiễn", 15, MCItemType.BOW, 15, new SkillMuaTen()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 200 + 10 * level;
			damage *= m / 100;
			
			return "Bắn mũi tên ra mọi phía, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	
	GAO_THET("Gào thét", 0, MCItemType.AXE, 5, new SkillGaoThet()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 100 + 10 * level;
			damage *= m / 100;
			
			return "Hét lên làm chậm và gây " + damage + " sát thương (" + m + "%)";
		}
	},
	RIU_SET("Rìu sét", 5, MCItemType.AXE, 8, new SkillRiuSet()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 120 + 10 * level;
			damage *= m / 100;
			
			return "Gọi sét xuống, gây " + damage + " sát thương (" + m + "%)";
		}
	},
	XONG_PHA("Địa chấn", 10, MCItemType.AXE, 10, new SkillXungPhong()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 70 + 10 * level;
			damage *= m / 100;
			
			return "Tạo địa chấn xung quanh, " + damage + " sát thương mỗi lần (" + m + "%)";
		}
	},
	TRUNG_PHAT("Trừng phạt", 15, MCItemType.AXE, 15, new SkillTrungPhat()) {
		@Override
		public String getDesc(Player player) {
			EPlayer ep = EPlayerDataUtils.getData(player);
			double damage = ep.getStatValue(EStat.DAMAGE, player);
			
			int level = ep.getSkillPoint(this);
			int m = 200 + 10 * level;
			damage *= m / 100;
			
			return "Gọi sấm về phía trước, gây " + damage + " sát thương (" + m + "%)";
		}
	};
	
	private String name;
	private int lvRq;
	private MCItemType itemRq;
	private Skill executor;
	private int cooldown;
	
	private ESkill(String name, ESComboType combo, int levelRequirement, MCItemType itemRequirement, int cooldown, Skill executor) {
		this.name = name;
		this.lvRq = levelRequirement;
		this.itemRq = itemRequirement;
		this.executor = executor;
		this.cooldown = cooldown;
	}
	
	private ESkill(String name, int levelRequirement, MCItemType itemRequirement, int cooldown, Skill executor) {
		this.name = name;
		this.lvRq = levelRequirement;
		this.itemRq = itemRequirement;
		this.executor = executor;
		this.cooldown = cooldown;
	}
	
	
	public abstract String getDesc(Player player);
	
	public String getName() {
		return this.name;
	}
	
	public int getLevelRequirement() {
		return this.lvRq;
	}
	
	public MCItemType getItemRequirement() {
		return this.itemRq;
	}
	
	public Skill getExecutor() {
		return this.executor;
	}
	
	public int getCooldown() {
		return this.cooldown;
	}
}

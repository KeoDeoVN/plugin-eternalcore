package mk.plugin.eternalcore.feature.skill;

import org.bukkit.entity.Player;

public interface Skill {
	
	public void run(Player player, int point);
	
}

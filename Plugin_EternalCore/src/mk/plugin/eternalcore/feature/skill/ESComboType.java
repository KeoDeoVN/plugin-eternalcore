package mk.plugin.eternalcore.feature.skill;

public enum ESComboType {
	
	A(new int[]{1}),
	B(new int[]{0}),
	
	C(new int[]{1, 0}),
	D(new int[]{0, 1}),
	E(new int[]{0, 0}),
	F(new int[]{1, 1}),
	
	G(new int[]{0, 0, 0}),
	H(new int[]{1, 1, 1}),
	I(new int[]{1, 0, 0}),
	K(new int[]{0, 1, 0}),
	L(new int[]{0, 0, 1}),
	M(new int[]{1, 1, 0}),
	N(new int[]{0, 1, 1}),
	O(new int[]{1, 0, 1}),
	
	P(new int[]{0, 0, 0, 0}),
	Q(new int[]{1, 1, 1, 1}),
	R(new int[]{1, 0, 0, 0}),
	S(new int[]{0, 1, 0, 0}),
	T(new int[]{0, 0, 1, 0}),
	U(new int[]{0, 0, 0, 1});
	
	private int[] combo;
	
	private ESComboType(int[] combo) {
		this.combo = combo;
	}
	
	public int[] getCombo() {
		return this.combo;
	}
	
}

package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillLienTien implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (100 + 10 * point) / 100;
		
		int amount = 5;
		BukkitRunnable br = new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i > amount) {
					this.cancel();
					return;
				}
				EPlayerAttackUtils.shootArrow(player, damage);
			}
		};
		
		br.runTaskTimer(MainEC.plugin, 0, 5);
	}

}

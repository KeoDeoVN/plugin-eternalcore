package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillLienHoanChem implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage1Hit = oD * (70 + 5 * point) / 100;

		LivingEntity target = getTarget(player);

		if (target == null) {
			player.sendMessage("§cKhông tìm thấy đối tượng");
			return;
		}

		LivingEntity e = target;
		if (e.hasMetadata("NPC")) return;
		BukkitRunnable br = new BukkitRunnable() {
			int i = 0;

			@Override
			public void run() {
				i++;
				if (i > 5) {
					this.cancel();
					return;
				}
				
				Vector v = player.getLocation().subtract(e.getLocation()).toVector().normalize();
				Location pl = e.getLocation().add(v);
				player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, pl, 1, 0, 0, 0);
				player.getWorld().playSound(pl, Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1);

				Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
					EPlayerAttackUtils.damage(player, e, damage1Hit, EDamageType.DEFAULT);
					e.setVelocity(v.multiply(-0.5).add(new Vector(0, 0.3, 0)));
				});

			}
		};
		br.runTaskTimer(MainEC.plugin, 0, 5);
	}

	private LivingEntity getTarget(Player player) {
		Location l = player.getLocation().add(player.getLocation().getDirection().multiply(4));
		for (Entity e : l.getWorld().getNearbyEntities(l, 3, 3, 3)) {
			if (e instanceof LivingEntity && e != player) {
				return (LivingEntity) e;
			}
		}
		return null;
	}

}

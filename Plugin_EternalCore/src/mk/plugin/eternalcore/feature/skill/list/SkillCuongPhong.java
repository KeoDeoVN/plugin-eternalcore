package mk.plugin.eternalcore.feature.skill.list;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillCuongPhong implements Skill {

	@Override
	public void run(Player player, int point) {
		int amount = 5;
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (200 + 10 * point) / 100;
		
		double angleBetweenArrows = (92 / (amount - 1)) * Math.PI / 180;
		double pitch = (player.getLocation().getPitch() + 90) * Math.PI / 180;
		double yaw = (player.getLocation().getYaw() + 90 - 92 / 2) * Math.PI / 180;
		double sZ = Math.cos(pitch);

		for (int i = 0; i < amount; i++) { 	
			double nX = Math.sin(pitch)	* Math.cos(yaw + angleBetweenArrows * i);
			double nY = Math.sin(pitch)* Math.sin(yaw + angleBetweenArrows * i);
			Vector newDir = new Vector(nX, sZ, nY);
			
			loc(player, damage, newDir);
		}
	}
	
	public void loc(Player player, double damage, Vector v) {
		Location location = player.getLocation();
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 8) {
					this.cancel();
					return;
				}
				Location newLocation = location.clone().add(v.normalize().multiply(i * 1.8));
				player.getWorld().playSound(newLocation, Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);

				int i = 0;
				List<Location> list = getListLocation(newLocation);
				for (i = 0 ; i < list.size() ; i ++) {
//					ParticleAPI.sendParticle(EnumParticle.SWEEP_ATTACK, list.get(i), i*0.05f, i*0.05f, i*0.05f, i*0.01f, new Double(i / 3).intValue());
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, list.get(i), new Double(i / 3).intValue(), i*0.07f, i*0.07f, i*0.07f, i*0.07f);
				}
				
				new BukkitRunnable() {
					@Override
					public void run() {
						for (Entity e : Lists.newArrayList(location.getWorld().getNearbyEntities(newLocation, 2, 5, 2))) {
							if (e.hasMetadata("NPC")) continue;
							if (e instanceof LivingEntity && e != player) {
								EPlayerAttackUtils.damage(player, (LivingEntity) e, damage, EDamageType.DEFAULT, 100);
								e.setVelocity(new Vector(0, 0.7, 0));
							}
						}
					}
				}.runTask(MainEC.plugin);
				
			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 2);
	}
	
	private List<Location> getListLocation(Location main) {
		List<Location> list = new ArrayList<Location> ();
		Location newLocation = main.clone().add(0, -2, 0);
		for (int i = 1 ; i < 15; i ++) {
			list.add(newLocation.clone().add(0, 0.5f * i, 0));
		}
		return list;
	}

}

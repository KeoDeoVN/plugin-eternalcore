package mk.plugin.eternalcore.feature.skill.list;

import java.util.ArrayList;

import org.bukkit.Location;
import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillMuaTen implements Skill {

//	@Override
//	public void run(Player player, int point) {
//		EPlayer ep = EPlayerDataUtils.getData(player);
//		double oD = ep.getStatValue(EStat.DAMAGE, player);
//		double damage = oD * (200 + 10 * point) / 100;
//		
//		Block block = player.getTargetBlock((Set<Material>) null, 100);
//		if (block == null || block.getType() == Material.AIR) {
//			player.sendMessage("§cKhoảng cách quá xa");
//			return;
//		}
//		Location loc = block.getLocation();
//		
//		int times = 10;
//		// Circle
//		new BukkitRunnable() {
//			int i = 0;
//			@Override
//			public void run() {
//				i++;
//				if (i >= times + 1) this.cancel();
//				Utils.circleParticles(Particle.FLAME, loc.clone().add(0, 1.5, 0), 5);
//			}
//		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 20);
//		
//		// Arrow
//		new BukkitRunnable() {
//			int i = 0;
//			@Override
//			public void run() {
//				i++;
//				if (i >= times * 2) this.cancel();
//				for (int i = 0 ; i < 8 ; i++) {
//					Location main = loc.clone().add(0, 40, 0);
//					double y = main.getY() + (double) (new Random().nextInt(10000) - 5000) / 1000;
//					double x = main.getX() + (double) (new Random().nextInt(10000) - 5000) / 1500;
//					double z = main.getZ() + (double) (new Random().nextInt(10000) - 5000) / 1500;
//					main.setX(x);
//					main.setZ(z);
//					main.setY(y);
//					
//					Arrow a = EPlayerAttackUtils.shootArrow(player, damage, new Vector(0, -10, 0), main);
//					a.setCritical(true);
//				}
//			}
//		}.runTaskTimer(MainEC.plugin, 0, 7);
//	}
	
	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (120 + 10 * point) / 100;
		
		Location location = player.getLocation().add(0, 1.1, 0);
		
		double radius = 5;
		int amount = 22;
		double increment = (2 * Math.PI) / amount;
        ArrayList<Location> locations = new ArrayList<Location>();
        
        for (int i = 0 ; i < amount ; i++) {
            double angle = i * increment;
            double x = location.getX() + (radius * Math.cos(angle));
            double z = location.getZ() + (radius * Math.sin(angle));
            locations.add(new Location(location.getWorld(), x, location.getY(), z));
        }
        
        for (Location l : locations) {
        	Vector v = l.subtract(location).toVector().normalize().multiply(2);
        	EPlayerAttackUtils.shootArrow(player, damage, v);
        }

	}
	
}

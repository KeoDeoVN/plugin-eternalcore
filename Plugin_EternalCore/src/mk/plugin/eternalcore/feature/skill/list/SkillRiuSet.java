package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillRiuSet implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (120 + 10 * point) / 100;
		
		Location location = player.getLocation();
		Location temp = location.clone().add(location.getDirection().multiply(5).setY(0));
		temp.getWorld().strikeLightningEffect(temp);
		for (Entity e : temp.getWorld().getNearbyEntities(temp, 2, 10, 2)) {
			if (e instanceof LivingEntity && e != player) {
				if (e.hasMetadata("NPC")) continue;
				EPlayerAttackUtils.damage(player, (LivingEntity) e, damage, EDamageType.DEFAULT);
			}
		}
	}

}

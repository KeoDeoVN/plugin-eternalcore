package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillChemLan implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (100 + 10 * point) / 100;
		
		double minR = 0.2;
		int amount = 20;
		player.getWorld().playSound(player.getLocation(), Sound.ENTITY_PLAYER_ATTACK_CRIT, 1, 1);
		BukkitRunnable br = new BukkitRunnable() {
			
			int i = 0;
			
			@Override
			public void run() {
				for (int k = 3 * i ; k < 3 * (i+1) ; k++) {
					for (int j = 0 ; j < 20 ; j ++) {
						Location l = player.getLocation().clone();
						double angle = Math.PI * 2 / (amount + j * 0.2) * k;
						
						double newX = l.getX() + (minR + j * 0.1) * Math.sin(angle + l.getYaw() * -1);
						double newZ = l.getZ() + (minR + j * 0.1) * Math.cos(angle + l.getYaw() * -1);
						
						l.setX(newX);
						l.setZ(newZ);
						l.setY(l.getY() + 1.0);
						
//						player.getWorld().spawnParticle(Particle.REDSTONE, l, 1, 0, 0, 0, 0, new Particle.DustOptions(Color.SILVER, 1));
						player.getWorld().spawnParticle(Particle.CRIT, l, 1, 0, 0, 0, 0);
						player.getWorld().spawnParticle(Particle.CRIT_MAGIC, l, 1, 0, 0, 0, 0);
					}
				}

				
				i++;
				if (i * 3 > amount) {
					this.cancel();
					Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
						EPlayerAttackUtils.damageEntitiesNearby(player, 2, 0.5, 2, damage, 100);
					});
					return;
				}
			}
		};
		br.runTaskTimerAsynchronously(MainEC.plugin, 0, 1);
	}

}

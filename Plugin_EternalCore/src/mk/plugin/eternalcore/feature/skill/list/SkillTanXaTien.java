package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.entity.Player;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillTanXaTien implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (120 + 10 * point) / 100;
		
		int amount = 9;
		
		double angleBetweenArrows = (45 / (amount - 1)) * Math.PI / 180;
		double pitch = (player.getLocation().getPitch() + 90) * Math.PI / 180;
		double yaw = (player.getLocation().getYaw() + 90 - 45 / 2) * Math.PI / 180;
		double sZ = Math.cos(pitch);
		
		for (int i = 0; i < amount; i++) { 	
			double nX = Math.sin(pitch)	* Math.cos(yaw + angleBetweenArrows * i);
			double nY = Math.sin(pitch)* Math.sin(yaw + angleBetweenArrows * i);
			Vector newDir = new Vector(nX, sZ, nY);
			
			EPlayerAttackUtils.shootArrow(player, damage, newDir.normalize().multiply(2f));
		}

	}

}

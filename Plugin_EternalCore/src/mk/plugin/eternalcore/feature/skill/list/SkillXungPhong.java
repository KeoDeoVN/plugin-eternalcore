package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class SkillXungPhong implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (70 + 10 * point) / 100;
		
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 5) {
					this.cancel();
					return;
				}
				
				// Get location
				Location temp = player.getLocation().clone();
				int k = 0;
				while (temp.getBlock().getType() == Material.AIR) {
					k++;
					if (k > 10) {
						this.cancel();
						break;
					}
					temp = temp.add(0,-1,0);
				}
				
				// Radius = 3	
				for (int j = 1 ; j <= 6 ; j++) {
//					Utils.createCircle(Particle.BLOCK_CRACK, temp.clone().add(0, 1.5, 0), j * 0.5, temp.clone().add(0, -1, 0).get);
					Utils.createCircle(Particle.CRIT, temp.clone().add(0, 1.5, 0), j * 0.5);
				}
				player.getWorld().playSound(temp, Sound.ENTITY_GENERIC_EXPLODE, 0.2f, 0.2f);

				
				for (Entity e : temp.getWorld().getNearbyEntities(temp, 3, 3, 3)) {
					if (e.hasMetadata("NPC")) continue;
					if (e instanceof LivingEntity && e != player) {
						new BukkitRunnable() {
							@Override
							public void run() {			
								EPlayerAttackUtils.damage(player, (LivingEntity) e, damage, EDamageType.DEFAULT);
								e.setVelocity(new Vector(0, 0.5, 0));
							}
						}.runTask(MainEC.plugin);
					}
				}
			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 20);
	}

}

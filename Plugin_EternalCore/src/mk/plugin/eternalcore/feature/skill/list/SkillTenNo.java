package mk.plugin.eternalcore.feature.skill.list;

import java.util.List;
import java.util.stream.Collectors;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillTenNo implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (150 + 10 * point) / 100;
		
		player.playSound(player.getLocation(), Sound.ENTITY_GENERIC_BURN, 1, 1);
		new BukkitRunnable () {
			Location lo = player.getLocation().add(0, 1, 0);
			int i = 0;
			@Override
			public void run() {
				if (i == 8) {
					this.cancel();
					return;
				}
				i++;
				Location temp = lo.add(lo.getDirection().multiply(i * 1)).clone();
				player.getWorld().spawnParticle(Particle.FLAME, temp, 3, 0.05f, 0.05f, 0.05f, 0.01f);
				player.getWorld().spawnParticle(Particle.SMOKE_NORMAL, temp, 5, 0.02f, 0.02f, 0.02f, 0.01f);
				
				Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
					List<Entity> l = temp.getWorld().getNearbyEntities(temp, 1, 1, 1).stream().filter(e -> e instanceof LivingEntity && e != player).collect(Collectors.toList());
					if (l.size() > 0) {
						this.cancel();
						
						player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, temp, 5, 0.05f, 0.05f, 0.05f, 0.01f);
						player.getWorld().playSound(temp, Sound.ENTITY_GENERIC_EXPLODE, 1, 1);
						
						Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
							temp.getWorld().getNearbyEntities(temp, 5, 5, 5).stream().filter(e -> e instanceof LivingEntity && e != player).collect(Collectors.toList()).forEach(e -> {
								if (e.hasMetadata("NPC")) return;
								EPlayerAttackUtils.damage(player, (LivingEntity) e, damage, EDamageType.DEFAULT, 100);
								e.setVelocity(e.getLocation().subtract(temp).toVector().normalize().multiply(1.5).add(new Vector(0, 1, 0)));
								e.setFireTicks(100);
							});
						});

					}
				});
				
			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 1);
		
	}

}

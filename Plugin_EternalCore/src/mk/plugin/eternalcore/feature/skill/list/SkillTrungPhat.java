package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillTrungPhat implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (200 + 10 * point) / 100;
		
		new BukkitRunnable () {
			Location lo = player.getLocation();
			int i = 0;
			@Override
			public void run() {
				if (i == 7) {
					this.cancel();
				}
				i++;
				int j = 0;
				Location temp = lo.add(lo.getDirection().multiply(i * 0.8)).clone();
				while (temp.getBlock().getType() == Material.AIR) {
					j++;
					if (j > 10) {
						this.cancel();
						break;
					}
					temp = temp.add(0,-1,0);
				}
				temp = temp.add(0, 1, 0);
				player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, temp.clone().add(0, 0.7, 0), 1, 0, 0, 0, 0);
				
				Location t = temp;
				Bukkit.getScheduler().runTask(MainEC.plugin, () -> {
					player.getWorld().strikeLightningEffect(t);
				});
				new BukkitRunnable() {
					@Override
					public void run() {
						EPlayerAttackUtils.damageEntitiesNearby(player, t, 2, 10, 2, damage, 1000);
					}
				}.runTask(MainEC.plugin);

			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 5);
	}

}

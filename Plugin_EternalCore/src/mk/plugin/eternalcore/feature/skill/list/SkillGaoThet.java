package mk.plugin.eternalcore.feature.skill.list;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffect;
import org.bukkit.potion.PotionEffectType;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class SkillGaoThet implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (100 + 10 * point) / 100;
		
		player.getWorld().spawnParticle(Particle.EXPLOSION_LARGE, player.getLocation().add(player.getLocation().getDirection().multiply(2)).add(0, 1, 0), 1, 0, 0, 0, 0);
		player.getWorld().playSound(player.getLocation(), Sound.ENTITY_ENDERDRAGON_GROWL, 0.5f, 0.5f);
		
		Location main = player.getLocation().add(player.getLocation().getDirection().multiply(3));
		Utils.getLivingEntities(player, main, 5, 5, 5).forEach(le -> {
			if (le.hasMetadata("NPC")) return;
			le.addPotionEffect(new PotionEffect(PotionEffectType.SLOW, 100, 2));
			le.addPotionEffect(new PotionEffect(PotionEffectType.WEAKNESS, 100, 2));
			EPlayerAttackUtils.damage(player, le, damage, EDamageType.DEFAULT);
			player.spawnParticle(Particle.CRIT_MAGIC, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
			player.spawnParticle(Particle.CRIT, le.getLocation(), 10, 0.2, 0.2, 0.2, 1);
		});
	}

}

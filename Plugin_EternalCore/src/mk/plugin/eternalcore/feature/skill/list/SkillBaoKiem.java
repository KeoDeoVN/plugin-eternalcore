package mk.plugin.eternalcore.feature.skill.list;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Location;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.util.Vector;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.feature.skill.Skill;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;

public class SkillBaoKiem implements Skill {

	@Override
	public void run(Player player, int point) {
		EPlayer ep = EPlayerDataUtils.getData(player);
		double oD = ep.getStatValue(EStat.DAMAGE, player);
		double damage = oD * (150 + 10 * point) / 100;
		
		Location location = player.getLocation();
		new BukkitRunnable() {
			int i = 0;
			@Override
			public void run() {
				i++;
				if (i >= 8) {
					this.cancel();
					return;
				}
				Location newLocation = location.clone().add(location.getDirection().multiply(i * 1.8));
				player.getWorld().playSound(newLocation, Sound.ENTITY_GHAST_SHOOT, 0.5f, 1.5f);

				int i = 0;
				List<Location> list = getListLocation(newLocation);
				for (i = 0 ; i < list.size() ; i ++) {
					player.getWorld().spawnParticle(Particle.SWEEP_ATTACK, list.get(i), new Double(i / 3).intValue(), i*0.07f, i*0.07f, i*0.07f, i*0.05f);
				}
				
				new BukkitRunnable() {
					@Override
					public void run() {
						for (Entity e : location.getWorld().getNearbyEntities(newLocation, 2, 5, 2)) {
							if (e.hasMetadata("NPC")) continue;
							if (e instanceof LivingEntity && e != player) {
								EPlayerAttackUtils.damage(player, (LivingEntity) e, damage, EDamageType.DEFAULT, 100);
								e.setVelocity(new Vector(0, 0.7, 0));
							}
						}
					}
				}.runTask(MainEC.plugin);
				
			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 0, 2);
	}
	
	private List<Location> getListLocation(Location main) {
		List<Location> list = new ArrayList<Location> ();
		Location newLocation = main.clone().add(0, 0, 0);
		for (int i = 1 ; i < 15; i ++) {
			list.add(newLocation.clone().add(0, 0.25f * i, 0));
		}
		return list;
	}

}

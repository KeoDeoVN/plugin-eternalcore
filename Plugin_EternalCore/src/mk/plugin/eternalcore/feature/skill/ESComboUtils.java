package mk.plugin.eternalcore.feature.skill;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.bukkit.World;
import org.bukkit.entity.Player;

import com.sk89q.worldguard.bukkit.WorldGuardPlugin;
import com.sk89q.worldguard.protection.ApplicableRegionSet;
import com.sk89q.worldguard.protection.flags.DefaultFlag;
import com.sk89q.worldguard.protection.managers.RegionManager;

import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class ESComboUtils {
	
	private static HashMap<Player, List<Integer>> combo = new HashMap<Player, List<Integer>> ();
	
	public static boolean canCombo(Player player) {
		World world = player.getWorld();
		RegionManager rm = WorldGuardPlugin.inst().getRegionContainer().get(world);
		ApplicableRegionSet ar = rm.getApplicableRegions(player.getLocation());
		boolean can = false;
		if (ar.testState(null, DefaultFlag.MOB_SPAWNING) || ar.testState(null, DefaultFlag.PVP))  {
			can = true;
		}
		
		return can;
	}
	
	public static void comboExcute(Player player) {
		EPlayer rpgP = EPlayerDataUtils.getData(player);

		Map<Integer, ESkill> skillList = rpgP.getActiveSkills();
		skillList.forEach((i, skill) -> {
			ESComboType combo = ESkillUtils.getCombos().get(i);
			if (combo == null) {
				return;
			}
			if (balancingCombo(player, combo.getCombo())) {
				if (!canCombo(player)) {
					player.sendMessage("§cKhông thể thực thi §a" + skill.getName());
					player.sendMessage("§cRa khỏi khu an toàn để combo!");
					return;
				}
				
				int point = rpgP.getSkillPoint(skill);
				if (point == 0) return;

				if (ESkillUtils.isDelayed(player, skill)) {
					player.sendMessage("§c" + skill.getName() + " §6chưa hổi xong, còn §f" + ESkillUtils.getTimeRemain(player, skill) + "s");
					return;
				}
				
				if (!ESkillUtils.checkRequirement(player, skill)) {
					player.sendMessage("§cKhông đủ điều kiện thực thi");
					return;
				}
				
				if (skill.getItemRequirement() != null) {
					MCItemType itemType = skill.getItemRequirement();
					if (!itemType.getMaterialList().contains(player.getInventory().getItemInMainHand().getType())) {
						player.sendMessage("§cYêu cầu cầm " + itemType.getName() + " khi dùng kỹ năng");
						return;
					}
				}

				skill.getExecutor().run(player, point);
				ESkillUtils.delay(player, skill);
				
				player.sendMessage("§e" + skill.getName() + " §7(" + skill.getCooldown() + "s hồi)");
				
				player.sendTitle("§c§l[ " + skill.getName() + " ]", "Thực thi kỹ năng", 0, 30, 0);
				return;
			}
		}); 
	}
	
	public static int getTimeOfClick(Player player) {
		if (!combo.containsKey(player)) {
			return 0;
		}
		return combo.get(player).size();
	}

	public static void addOne(Player player, int i) {
		if (combo.containsKey(player)) {
			List<Integer> list = combo.get(player);
			list.add(i);
			combo.put(player, list);
		}
		else {
			List<Integer> list = new ArrayList<Integer> ();
			list.add(i);
			combo.put(player, list);
		}
	}
	
	@SuppressWarnings("deprecation")
	public static void finishCombo(Player player) {
		combo.remove(player);
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(""));
	}
	
	public static boolean balancingCombo(Player player, int[] i) {
		if (!combo.containsKey(player)) {
			return false;
		}
		
		if (combo.get(player).size() != i.length) {
			return false;
		}
		boolean check = true;
		for (int i2 = 0 ; i2 < combo.get(player).size() ; i2 ++) {
			if (i[i2] != combo.get(player).get(i2)) {
				check = false;
				break;
			}
		}
		return check;
	}
	
	@SuppressWarnings("deprecation")
	public static void sendTitleCombo(Player player) {
		List<String> comboList = new ArrayList<String> ();
		for (int i : combo.get(player)) {
			if (i == 0) {
				comboList.add("§2§lTrái§f");
			}
			else comboList.add("§2§lPhải§f");
		}
		String mess = "§c§lShift §r";
		for (String s : comboList) {
			mess += s + "§a§l + ";
		}
		
		mess = "§f" + mess.substring(0, mess.length() - 3);
		player.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText(mess));
		
	}
	

	private static List<Player> isCombo = new ArrayList<Player> ();
	
	public static boolean isPlayerComboed(Player player) {
		if (isCombo.contains(player)) {
			return true;
		} else return false;
	}
	
	public static void addCombo(Player player) {
		if (!isPlayerComboed(player)) {
			isCombo.add(player);
		}
	}
	
	public static void huyCombo(Player player) {
		if (isPlayerComboed(player)) {
			isCombo.remove(player);
		}
		finishCombo(player);
	}

}

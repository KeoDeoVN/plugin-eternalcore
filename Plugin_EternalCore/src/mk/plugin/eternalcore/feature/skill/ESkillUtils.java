package mk.plugin.eternalcore.feature.skill;

import java.util.HashMap;
import java.util.Map;

import org.bukkit.entity.Player;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.mc.MCItemType;

public class ESkillUtils {
	
	private static Map<Player, Map<ESkill, Long>> delayed = new  HashMap<Player, Map<ESkill, Long>> ();
	
	public static Map<Integer, ESComboType> getCombos() {
		Map<Integer, ESComboType> m = Maps.newHashMap();
		m.put(0, ESComboType.C);
		m.put(1, ESComboType.D);
		m.put(2, ESComboType.E);
		m.put(3, ESComboType.F);
		
		return m;
	}
	
	public static boolean checkRequirement(Player player, ESkill skill) {
		boolean a = player.getLevel() >= skill.getLevelRequirement();
		boolean b = skill.getItemRequirement() == null || MCItemType.isHoldingType(skill.getItemRequirement(), player);
		return a && b;
	}
	
	public static String comboToString(int[] cb) {
		String combo = "";
		for (int i = 0 ; i < cb.length ; i++) {
			String s = cb[i] == 0 ? "T" : "P";
			combo += s;
		}

		String result = combo.replaceAll("T", "Trái ").replaceAll("P", "Phải ");
		if (combo.equalsIgnoreCase("")) return "Không cần";
		return "Shift " + result;
	}

	public static int getTimeRemain(Player player, ESkill skill) {
		if (isDelayed(player, skill)) {
			return skill.getCooldown() - new Double((System.currentTimeMillis() - delayed.get(player).get(skill)) / 1000  * 100).intValue() / 100;
		}
		return 0;
	}
	
	public static boolean isDelayed(Player player, ESkill skill) {
		if (delayed.containsKey(player)) {
			Map<ESkill, Long> map = delayed.get(player);
			if (map.containsKey(skill)) {
				long time = map.get(skill);
				if (System.currentTimeMillis() - time >= skill.getCooldown() * 1000) {
					map.remove(skill);
					delayed.put(player, map);
					return false;
				}
				else return true;
			}
		}
		
		return false;
	}
	
	public static void delay(Player player, ESkill skill) {
		Map<ESkill, Long> map = new HashMap<ESkill, Long>  ();
		if (delayed.containsKey(player)) {
			map = delayed.get(player);
		}
		map.put(skill, System.currentTimeMillis());
		delayed.put(player, map);
	}
	
}

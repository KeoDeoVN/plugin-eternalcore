package mk.plugin.eternalcore.feature.dabaoho;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.util.ItemStackUtils;

public class EKeepGem {
	
	private static final String NAME = "§a§lMảnh bảo hộ";
	
	public static ItemStack getItem() {
		ItemStack item = new ItemStack(Material.PRISMARINE_SHARD);
		ItemStackUtils.setDisplayName(item, NAME);
		ItemStackUtils.addLoreLine(item, "§7§oĐể ở trong kho đồ, có tác dụng");
		ItemStackUtils.addLoreLine(item, "§7§ogiữ đồ, kinh nghiệm khi chết và");
		ItemStackUtils.addLoreLine(item, "§7§otiêu hao một viên cho mỗi lần");
		ItemStackUtils.addEnchantEffect(item);
		return item;
	}
	
	public static boolean isThatItem(ItemStack item) {
		return ItemStackUtils.getName(item).contains(NAME);
	}
	
}

package mk.plugin.eternalcore.feature.infogui;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.feature.permbuff.EPermBuff;
import mk.plugin.eternalcore.feature.potential.EPotential;
import mk.plugin.eternalcore.feature.power.EPowerUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerStatUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EInfoGUI {
	
	public static final String TITLE = "§2§lTHÔNG TIN NGƯỜI CHƠI";
	
	public static void openGUI(Player player) {
		Inventory inv = Bukkit.createInventory(null, 9, TITLE);
		player.openInventory(inv);
		
		// Load item
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			for (int i = 0 ; i < inv.getSize() ; i++) {
				inv.setItem(i, Utils.getBlackSlot());
			}
			inv.setItem(2, getStatInfo(player));
			inv.setItem(4, getExpInfo(player));
			inv.setItem(6, getBuffInfo(player));
		});
		
		new BukkitRunnable() {
			@Override 
			public void run() {
				if (player.getOpenInventory() == null || !player.getOpenInventory().getTitle().equals(TITLE)) {
					this.cancel();
					return;
				}
				inv.setItem(6, getBuffInfo(player));
			}
		}.runTaskTimerAsynchronously(MainEC.plugin, 5, 5);
	}
	
	public static ItemStack getStatInfo(Player player) {
		ItemStack item = new ItemStack(Material.PAPER, 1);
		ItemStackUtils.setDisplayName(item, "§6§lCHỈ SỐ");
		
		EPlayer rpgP = EPlayerDataUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		lore.add("§3Điểm tiềm năng: §f" + EPlayerStatUtils.getRemainPotentials(player));
		lore.add("§3Lực chiến: §f" + EPowerUtils.calculatePower(player));
		lore.add("§7------------------");
		for (int i = 0 ; i < EPotential.values().length ; i++) {
			EPotential po = EPotential.values()[i];
			lore.add("§c" + po.getName() + ": §f" + rpgP.getPotential(po));
		}
		lore.add("§7------------------");
		for (EStat stat : EStat.values()) {
			lore.add("§a" + stat.getName() + ": §f" + rpgP.getStat(stat, player) + " §7(" + Utils.round(rpgP.getStatValue(stat, player)) + ")");
		}
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getExpInfo(Player player) {
		ItemStack item = new ItemStack(Material.EXP_BOTTLE, 1);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lKINH NGHIỆM");
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		lore.add("§aKinh nghiệm: §f" + Utils.round(player.getExp() * 100) + "%");
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static ItemStack getBuffInfo(Player player) {
		ItemStack item = new ItemStack(Material.DRAGONS_BREATH);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.setDisplayName(item, "§6§lBUFF");
		EPlayer rpgP = EPlayerDataUtils.getData(player);
		List<String> lore = new ArrayList<String> ();
		lore.add("§7------------------");
		rpgP.getStatBuffs().forEach(buff -> {
			String s = "%";
			lore.add("§a+" + buff.getValue() + s + " " + buff.getStat().getName() + " §7(" + buff.getSecondsRemain() + "s)");
		});
		rpgP.getExpBuffs().forEach(buff -> {
			lore.add("§a+" + buff.getValue() + "% exp" + " §7(" + buff.getSecondsRemain() + "s)");
		});
		
		
		// Permbuff
		EPermBuff.permBuffs.values().forEach(buff -> {
			if (!player.hasPermission(buff.getPermission())) return;
			buff.getBuffs().forEach((stat, value) -> {
				lore.add("§6+" + value + "% " + stat.getName());
			});
		});;
		
		
		ItemStackUtils.setLore(item, lore);
		
		return item;
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (!e.getView().getTitle().contains(TITLE)) return;
		e.setCancelled(true);
	}
	
}

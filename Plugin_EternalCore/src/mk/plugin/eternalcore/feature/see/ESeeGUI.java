package mk.plugin.eternalcore.feature.see;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryDragEvent;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import mk.plugin.eternalcore.feature.infogui.EInfoGUI;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.util.GUIUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESeeGUI {
	
	public static String TITLE = "§c§lXEM THÔNG TIN ";
	
	public static void openGUI(Player player, Player viewer) {
		Inventory inv = Bukkit.createInventory(null, 54, TITLE + player.getName().toUpperCase());
		viewer.openInventory(inv);
		
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			PlayerInventory pInv = player.getInventory();
			ItemStack[] storageContents = pInv.getStorageContents();
			ItemStack[] armorContents = pInv.getArmorContents();
			inv.setStorageContents(storageContents);
			inv.setItem(45, pInv.getItemInOffHand());
			for (int i = 0 ; i < armorContents.length ; i++) {
				inv.setItem(49 - i, armorContents[i]);
			}
			for (int i = 36 ; i < 45 ; i++) {
				inv.setItem(i, GUIUtils.getBlackSlot());
			}
			inv.setItem(50, GUIUtils.getBlackSlot());
			inv.setItem(51, EInfoGUI.getStatInfo(player));
			inv.setItem(52, EInfoGUI.getExpInfo(player));
			inv.setItem(53, EInfoGUI.getBuffInfo(player));
			Utils.sendSound(viewer, Sound.BLOCK_CHEST_OPEN, 1, 1);
		});
	}
	
	public static void eventClick(InventoryClickEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}
	
	public static void eventDrag(InventoryDragEvent e) {
		if (e.getView().getTitle().contains(TITLE)) e.setCancelled(true);
	}	
	
}

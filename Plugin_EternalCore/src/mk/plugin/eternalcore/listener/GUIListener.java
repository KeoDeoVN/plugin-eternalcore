package mk.plugin.eternalcore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryCloseEvent;

import mk.plugin.eternalcore.feature.chuyenhoa.ECHHGUI;
import mk.plugin.eternalcore.feature.cuonghoa.ECHGUI;
import mk.plugin.eternalcore.feature.gems.EGemGUI;
import mk.plugin.eternalcore.feature.infogui.EInfoGUI;
import mk.plugin.eternalcore.feature.potential.EPotentialGUI;
import mk.plugin.eternalcore.feature.see.ESeeGUI;
import mk.plugin.eternalcore.feature.skill.ESGUI;
import mk.plugin.eternalcore.feature.socket.duclo.EDucLoGUI;
import mk.plugin.eternalcore.feature.socket.hopthe.ESHopTheGUI;
import mk.plugin.eternalcore.feature.socket.khamda.EKhamDaGUI;
import mk.plugin.eternalcore.feature.socket.tach.ESocketSplitGUI;
import mk.plugin.eternalcore.feature.tinhluyen.EHopTheGUI;
import mk.plugin.eternalcore.feature.tinhluyen.ETLGUI;
import mk.plugin.eternalcore.feature.unlock.EUnlockGUI;
import mk.plugin.eternalcore.feature.weaponsoul.EWSGUI;

public class GUIListener implements Listener {
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		EInfoGUI.eventClick(e);
		EPotentialGUI.eventClick(e);
		ESGUI.onClick(e);
		EWSGUI.eventClick(e);
		ECHGUI.eventClick(e);
		ECHHGUI.eventClick(e);
		EGemGUI.eventClick(e);
		ETLGUI.eventClick(e);
		EHopTheGUI.eventClick(e);
		EDucLoGUI.eventClick(e);
		EKhamDaGUI.eventClick(e);
		EUnlockGUI.eventClick(e);
		ESeeGUI.eventClick(e);
		ESocketSplitGUI.eventClick(e);
		ESHopTheGUI.eventClick(e);
	}
	
	@EventHandler
	public void onClose(InventoryCloseEvent e) {
		ECHGUI.eventClose(e);
		ECHHGUI.eventClose(e);
		ETLGUI.eventClose(e);
		EHopTheGUI.eventClose(e);
		EDucLoGUI.eventClose(e);
		EKhamDaGUI.eventClose(e);
		EUnlockGUI.eventClose(e);
		ESocketSplitGUI.eventClose(e);
		ESHopTheGUI.eventClose(e);
	}
	
}

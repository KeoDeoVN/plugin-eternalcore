package mk.plugin.eternalcore.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.attribute.Attribute;
import org.bukkit.entity.LivingEntity;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntitySpawnEvent;
import org.bukkit.inventory.ItemStack;

import io.lumine.xikage.mythicmobs.api.bukkit.events.MythicMobDeathEvent;
import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mob.level.EMobLevelUtils;
import mk.plugin.eternalcore.mythicmob.drop.MythicDrops;

public class EntityListener implements Listener {
	
	@EventHandler
	public void onEntitySpawn(EntitySpawnEvent e) {
		if (!EConfig.AUTO_STAT_MODIFIED_WORLDS.contains(e.getEntity().getWorld().getName())) return;

		// Set
		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			if (e.getEntity() instanceof LivingEntity == false) return;
			LivingEntity entity = (LivingEntity) e.getEntity();

			if (EMobLevelUtils.isBoss(entity)) return;
			int level = EMobLevelUtils.getLevel(entity);
			if (level == 0) return;
			
			// Calculate values
			double health = level * level * 0.75;
			double damage = health / 10;
			
			entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(health);
			entity.setHealth(entity.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
			entity.getAttribute(Attribute.GENERIC_ATTACK_DAMAGE).setBaseValue(damage);
		}, 5);

	}
	
	// MythicDrop
	@EventHandler
	public void onEntityDead(MythicMobDeathEvent e) {
		String id = e.getMobType().getInternalName();
		List<ItemStack> list = MythicDrops.getDrop(id);
		list.forEach(is -> e.getEntity().getWorld().dropItemNaturally(e.getEntity().getLocation(), is));
	}
	
}

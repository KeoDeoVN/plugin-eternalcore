package mk.plugin.eternalcore.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.inventory.InventoryType;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.PlayerInventory;

import kdvn.sky2.lockeditem.utils.LIUtils;
import mk.plugin.eternalcore.item.EItemUtils;

public class ItemListener implements Listener {
	
	// Update item
	@EventHandler
	public static void onOpenInventory(InventoryCloseEvent e) {
		Inventory inv = e.getInventory();
		if (inv.getType() == InventoryType.CRAFTING) {
			Player player = (Player) e.getPlayer();
			PlayerInventory pi = player.getInventory();
			
			for (ItemStack item : pi.getArmorContents()) {
				if (item != null) {
					if (EItemUtils.isEItem(item)) {
						if (LIUtils.isOwner(item, player.getName())) {
							EItemUtils.updateLore(player, item);
						}
					}
				}
			}
			
			ItemStack itemHand = pi.getItemInMainHand();
			if (EItemUtils.isEItem(itemHand)) {
				if (LIUtils.isOwner(itemHand, player.getName())) {
					EItemUtils.updateLore(player, itemHand);
				}
			}
		}
	}
	
}

package mk.plugin.eternalcore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLevelChangeEvent;

import mk.plugin.eternalcore.player.EPlayerLevelUtils;

public class LevelListener implements Listener {

	@EventHandler
	public void onLevelUp(PlayerLevelChangeEvent e) {
		EPlayerLevelUtils.levelUpEffet(e.getPlayer(), e.getNewLevel());
//		Player player = e.getPlayer();
//		player.setPlayerListName(player.getDisplayName() + "§7 " + player.getLevel());
	}
	
}

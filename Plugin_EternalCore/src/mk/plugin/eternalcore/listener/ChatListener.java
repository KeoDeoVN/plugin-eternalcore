package mk.plugin.eternalcore.listener;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import mk.plugin.eternalcore.util.ChatPlaceholder;

public class ChatListener implements Listener {

	@EventHandler
	public void onChatPlaceholder(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		if (ChatPlaceholder.cps.containsKey(player)) {
			player.sendMessage("§7Yêu cầu chat kết thúc!");
			ChatPlaceholder cp = ChatPlaceholder.cps.get(player);
			String s = e.getMessage();
			cp.runCmd(s.replace("&", "§"));
			ChatPlaceholder.cps.remove(player);
			e.setCancelled(true);
		}
	}
	
	// Level chat
	@EventHandler
	public void onChat(AsyncPlayerChatEvent e) {
		Player player = e.getPlayer();
		String message = player.hasPermission("eternal.special") ? "§d" + e.getMessage() : e.getMessage();
		e.setMessage(message);
		e.setFormat("§f[§cLv." + player.getLevel() + "§f] §r" + e.getFormat());
	}
	
}

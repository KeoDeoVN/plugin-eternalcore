package mk.plugin.eternalcore.listener;

import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.EntityDeathEvent;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.feature.dabaoho.EKeepGem;
import mk.plugin.eternalcore.feature.hologram.HoloUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.mob.level.EMobLevelUtils;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerLevelUtils;

public class DeathListener implements Listener {
	

	@EventHandler
	public void onEntityKilledByPlayer(EntityDeathEvent e) {
		if (e.getEntity().getKiller() instanceof Player) {
			Player player = (Player) e.getEntity().getKiller();
			EPlayer ep = EPlayerDataUtils.getData(player);
			ItemStack i = player.getInventory().getItemInMainHand();
			
			List<String> lines = Lists.newArrayList();
			
			// Name
			lines.add("§2" + e.getEntity().getName());
			
			// Exp
			int moblevel = EMobLevelUtils.getLevel(e.getEntity());
			if (moblevel > 0) {
				int exp = EMobLevelUtils.getMobExp(moblevel);
				lines.add("§a§o+" + exp + " Exp");
				EPlayerLevelUtils.addExp(player, exp);
			}
			
			// Weapon soul
			if (EConfig.WORLDS_WEAPONSOUL.contains(player.getWorld().getName())) {
				if (i == null) return;
				MCItemType it = MCItemType.getType(i.getType());
				if (it != null) {
					ep.addSoul(MCItemType.getType(i.getType()), 1);;
					lines.add("§c§o+1 Linh hồn " + it.getName());			
				}
			}

			
			Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
				HoloUtils.hologram(MainEC.plugin, lines, 40, player, e.getEntity(), 1);
			});
			
		}
	}
	
	@EventHandler
	public void onDeath(PlayerDeathEvent e) {
		Player player = e.getEntity();
		if (player.hasPermission("dabaoho.bypass")) {
			e.setKeepInventory(true);
			e.setKeepLevel(true);
			e.getDrops().clear();
			e.setDroppedExp(0);
			player.sendMessage("§aBạn được giữ lại đồ và cấp độ");
			return;
		}
		ItemStack[] contents = player.getInventory().getContents();
		for (int i = 0 ; i < contents.length ; i++) {
			ItemStack item = contents[i];
			if (item != null) {
				if (EKeepGem.isThatItem(item)) {
					player.sendMessage("§aBạn được giữ lại cấp độ và đồ nhờ Đá bảo hộ");
					e.setKeepInventory(true);
					e.setKeepLevel(true);
					e.getDrops().clear();
					e.setDroppedExp(0);
					if (item.getAmount() == 1) contents[i] = null;
					else item.setAmount(item.getAmount() - 1);
					player.getInventory().setContents(contents);
					break;
				}
			}
		}

//		List<ItemStack> is = e.getDrops();
//		for (int i = 0 ; i < is.size() ; i++) {
//			ItemStack item = is.get(i);
//			System.out.println(ItemStackUtils.getName(item));
//			if (LIUtils.isLocked(item)) {
//				System.out.println("Locked");
//				System.out.println("");
//				e.getDrops().remove(item);
//				Bukkit.getScheduler().runTask(MainEC.getPP(), () -> {
//					player.getInventory().addItem(item);
//				});
//			}
//		}
	}
	
}

package mk.plugin.eternalcore.listener;


import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.PrepareItemCraftEvent;
import org.bukkit.inventory.CraftingInventory;
import org.bukkit.inventory.ItemStack;

import mk.plugin.eternalcore.feature.eitemcraft.EItemCraftUtils;
import mk.plugin.eternalcore.mc.MCItemType;

public class CraftListener implements Listener {
	
	@EventHandler
	public void onCraft(PrepareItemCraftEvent e) {
		if (e.getRecipe() != null) {
			ItemStack result = e.getRecipe().getResult();
			CraftingInventory inv = e.getInventory();
			if (MCItemType.getType(result.getType()) == null) return;
			switch (MCItemType.getType(result.getType())) {
			case BOW:
				inv.setResult(EItemCraftUtils.getBasicBow(null, result.getType()));
				break;
			case SWORD:
				inv.setResult(EItemCraftUtils.getBasicSword(null, result.getType()));
				break;
			case AXE:
				inv.setResult(EItemCraftUtils.getBasicAxe(null, result.getType()));
				break;
			case ARMOR:
				inv.setResult(EItemCraftUtils.getBasicArmor(null, result.getType()));
				break;
			case SHIELD:
				inv.setResult(EItemCraftUtils.getBasicShield(null, result.getType()));
				break;
			default:
				break;
			}
		}
	}

}

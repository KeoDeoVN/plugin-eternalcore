package mk.plugin.eternalcore.listener;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.Particle;
import org.bukkit.Sound;
import org.bukkit.attribute.Attribute;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Arrow;
import org.bukkit.entity.Entity;
import org.bukkit.entity.LivingEntity;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.inventory.InventoryCloseEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerItemHeldEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerSwapHandItemsEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffect;

import mk.plugin.eternalcore.feature.damage.EDamageType;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerAttackUtils;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerStatUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class StatListener implements Listener {

	/*
	 * Damage check
	 */
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onEntityDamage(EntityDamageByEntityEvent e) {
		Entity target = e.getEntity();
		if (Utils.hasDamageCheck(target)) {
			if (!e.isCancelled()) {
				Utils.setCanDamageCheck(target);
			} else Utils.removeDamageCheck(target);
		}
	}
	
	
	/*
	 Show default
	 */
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		player.getAttribute(Attribute.GENERIC_ATTACK_SPEED).setBaseValue(16);
	}
	
	@EventHandler
	public void onShootBow(PlayerInteractEvent e) {
		if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			Player player = e.getPlayer();
			if (MCItemType.isHoldingType(MCItemType.BOW, player)) e.setCancelled(true);
		}
	}
	
	
	/*
	 Player attack
	 */
	
	// Remove default attack
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onPlayerDamage(EntityDamageByEntityEvent e) {
		if (!(e.getDamager() instanceof Player)) return;
		if (e.getDamager() == e.getEntity()) return;
		Player player = (Player) e.getDamager();
		
		if (e.isCancelled()) return;		
		if (e.getCause() != DamageCause.ENTITY_ATTACK) return;

		// Shift
		if (player.isSneaking()) {
			e.setCancelled(true);
			return;
		}
		
		// Cancel default damage, use EternalDamage instead
		if (e.getEntity() instanceof LivingEntity && e.getDamage() > 0.1) {
			e.setCancelled(true);
			
			// Another method for Bow
			ItemStack itemHand = player.getInventory().getItemInMainHand();
			if (itemHand != null && itemHand.getType() == Material.BOW) return;
			
			// Attack
			EPlayerAttackUtils.attack(player, (LivingEntity) e.getEntity());
			player.spawnParticle(Particle.SWEEP_ATTACK, player.getLocation().add(0, 1.2, 0).add(player.getLocation().getDirection().multiply(1.2)), 1, 0, 0, 0, 0);
			player.playSound(player.getLocation(), Sound.ENTITY_PLAYER_ATTACK_SWEEP, 1, 1);
		}
	}
	
	@EventHandler(priority = EventPriority.HIGHEST)
	public void onArrowDamage(EntityDamageByEntityEvent e) {
		if (!(e.getDamager() instanceof Arrow)) return;
		if (!(e.getEntity() instanceof LivingEntity)) return;
		
		Arrow arrow = (Arrow) e.getDamager();
		if (!(arrow.getShooter() instanceof Player)) return;
		
		if (e.isCancelled()) return;
		
		Player player = (Player) arrow.getShooter();
		if (EPlayerAttackUtils.hasAttackArrow(arrow)) {
			e.setCancelled(true);
			arrow.remove();
			
			// Check damage
			double damage = EPlayerAttackUtils.getDamage(arrow);
			EPlayer ep = EPlayerDataUtils.getData(player);
			
			EDamageType dT = EDamageType.DEFAULT;
			
			// Accuracy
			if (Utils.rate(ep.getStatValue(EStat.ACCURACY, player))) {
				damage *= 2;
				dT = EDamageType.CRITICAL;
			}
			
			EPlayerAttackUtils.damage(player, (LivingEntity) e.getEntity(), damage, dT);
		}
	}
	
	
	// Shoot arrows
	@EventHandler
	public void onClick(PlayerInteractEvent e) {
		if (e.getAction() == Action.LEFT_CLICK_AIR && e.getItem() != null && e.getItem().getType() == Material.BOW) {
			Player player = e.getPlayer();
			
			// Shift
			if (player.isSneaking()) {
				e.setCancelled(true);
				return;
			}
			
			if (!EPlayerAttackUtils.isCooldownAttack(player)) {
				EPlayer ep = EPlayerDataUtils.getData(player);
				double damage = ep.getStatValue(EStat.DAMAGE, player);
				
				ItemStack itemHand = player.getInventory().getItemInMainHand();
				damage += itemHand.getEnchantmentLevel(Enchantment.ARROW_DAMAGE);
				
				EPlayerAttackUtils.shootArrow(player, damage);
				EPlayerAttackUtils.setCooldownAttack(player, new Double(ep.getStatValue(EStat.ATTACKSPEED, player) * 1000).longValue(), player.getInventory().getItemInMainHand().getType());
			}
		}
	}
	
	
	// Player get damaged not from player
	@EventHandler(priority = EventPriority.HIGH)
	public void onPlayerDamaged(EntityDamageByEntityEvent e) {
		if (e.isCancelled()) return;
		if (e.getEntity() instanceof Player  && !(e.getDamager() instanceof Player)) {
			Player player = (Player) e.getEntity();
			EPlayer ep = EPlayerDataUtils.getData(player);
			
			double damage = e.getDamage();
			// Defense
			damage *= (1 - (ep.getStatValue(EStat.DEFENSE, player) / 100));
			
			e.setDamage(damage);
		}
	}
	
	/*
	 Armor change
	 */
//	@EventHandler
//	public void onArmorChange(PlayerArmorChangeEvent e) {
//		Player player = e.getPlayer();
//		ItemStack oI = e.getOldItem();
//		ItemStack nI = e.getNewItem();
//		
//		if (EItemUtils.isEItem(oI)) {
//			EItem o = EItemUtils.fromItemStack(oI);
//			o.getPassivePotions().keySet().forEach(t -> {
//				player.removePotionEffect(t);
//			});
//		}
//		
//		if (EItemUtils.isEItem(nI)) {
//			EItem n = EItemUtils.fromItemStack(nI);
//			n.getPassivePotions().forEach((t, v) -> {
//				player.addPotionEffect(new PotionEffect(t, Integer.MAX_VALUE, v));
//			});
//		}
//	}
	
	/*
	 Update stat
	 */
	
	@EventHandler
	public void onCloseInv(InventoryCloseEvent e) {
		Player player = (Player) e.getPlayer();
		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			EPlayerStatUtils.updateStats(player);
		}, 20);
	}
	
	@EventHandler
	public void onSwitchItem(PlayerItemHeldEvent e) {
		Player player = e.getPlayer();
//		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
//			EPlayerStatUtils.updateStats(player);
//		}, 20);
		
		// Potion
		ItemStack oI = player.getInventory().getItem(e.getPreviousSlot());
		ItemStack nI = player.getInventory().getItem(e.getNewSlot());
		if (EItemUtils.isEItem(oI)) {
			EItem o = EItemUtils.fromItem(oI);
			o.getPassivePotions().keySet().forEach(t -> {
				player.removePotionEffect(t);
			});
		}
		if (EItemUtils.isEItem(nI)) {
			EItem n = EItemUtils.fromItem(nI);
			n.getPassivePotions().forEach((t, v) -> {
				player.addPotionEffect(new PotionEffect(t, Integer.MAX_VALUE, v - 1));
			});
		}
	}
	
	@EventHandler
	public void onSwitchHand(PlayerSwapHandItemsEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskLater(MainEC.plugin, () -> {
			EPlayerStatUtils.updateStats(player);
		}, 20);
	}
	
	
	
	
	
	
	
	
}

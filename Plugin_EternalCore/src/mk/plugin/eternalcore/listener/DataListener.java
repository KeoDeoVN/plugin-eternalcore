package mk.plugin.eternalcore.listener;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerQuitEvent;

import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerStorageUtils;

public class DataListener implements Listener{
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			EPlayerDataUtils.load(player);
		});
	}
	
	@EventHandler
	public void onQuit(PlayerQuitEvent e) {
		Player player = e.getPlayer();
		Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, () -> {
			EPlayerStorageUtils.save(player);
		});
	}
	
}

package mk.plugin.eternalcore.listener;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEvent;

import mk.plugin.eternalcore.feature.tinhluyen.ERDTLUtils;
import mk.plugin.eternalcore.socket.ERSocketUtils;

public class UseListener implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		ERDTLUtils.onInteract(e);
		ERSocketUtils.onInteract(e);
	}
	
}

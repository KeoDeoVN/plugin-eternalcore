package mk.plugin.eternalcore.listener;

import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.event.player.PlayerToggleSneakEvent;
import org.bukkit.inventory.EquipmentSlot;

import mk.plugin.eternalcore.feature.skill.ESComboUtils;

public class SkillListener implements Listener {
	
	@EventHandler
	public void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		
		if (e.getAction() != Action.RIGHT_CLICK_AIR && e.getAction() != Action.LEFT_CLICK_AIR) return;
		if (!ESComboUtils.isPlayerComboed(player)) return;
		if (e.getHand() != EquipmentSlot.HAND) return;

		player.playSound(player.getLocation(), Sound.BLOCK_WOOD_BUTTON_CLICK_ON, 1, 1);
		
		if (e.getAction() == Action.LEFT_CLICK_AIR || e.getAction() == Action.LEFT_CLICK_BLOCK) {
			ESComboUtils.addOne(player, 0);
			ESComboUtils.sendTitleCombo(player);
		}
		else if (e.getAction() == Action.RIGHT_CLICK_AIR || e.getAction() == Action.RIGHT_CLICK_BLOCK) {
			ESComboUtils.addOne(player, 1);
			ESComboUtils.sendTitleCombo(player);
		}
	}
	
	@EventHandler
	public void onSneak(PlayerToggleSneakEvent e) {
		Player player = e.getPlayer();
		if (e.isSneaking()) {
			ESComboUtils.addCombo(player);
		} else {
			ESComboUtils.comboExcute(player);
			ESComboUtils.huyCombo(player);
		}
		
	}
}

package mk.plugin.eternalcore.mythicmob.drop;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import kdvn.facrpg.shopitemgui.itemdatabase.ItemManager;
import mk.plugin.eternalcore.util.Utils;

public class MythicDrops {
	
	private static Map<String, List<MythicDrop>> drops = Maps.newHashMap();
	private static Map<String, String> mobDrops = Maps.newHashMap();
	
	public static void reload(FileConfiguration config) {
		drops.clear();
		config.getConfigurationSection("mythicmob-drop.drop").getKeys(false).forEach(dropID -> {
			List<MythicDrop> md = Lists.newArrayList();
			config.getStringList("mythicmob-drop.drop." + dropID).forEach(s -> {
				String itemID = s.split(";")[0];
				int amount = Integer.valueOf(s.split(";")[1]);
				double chance = Double.valueOf(s.split(";")[2]);
				md.add(new MythicDrop(itemID, amount, chance));
			});
			drops.put(dropID, md);
		});
		mobDrops.clear();
		config.getStringList("mythicmob-drop.mob").forEach(s -> {
			mobDrops.put(s.split(";")[0], s.split(";")[1]);
		});
	}
	
	public static List<ItemStack> getDrop(String mythicMobID) {
		if (!mobDrops.containsKey(mythicMobID)) return Lists.newArrayList();
		//
		List<ItemStack> list = Lists.newArrayList();
		List<MythicDrop> md = drops.get(mobDrops.get(mythicMobID));
		//
		md.forEach(drop -> {
			if (Utils.rate(drop.getChance())) {
				if (Bukkit.getPluginManager().isPluginEnabled("Shops")) {
					if (!ItemManager.getItems().contains(drop.getItemID())) return;
					ItemStack i = ItemManager.getItem(drop.getItemID()).clone();
					i.setAmount(drop.getAmount());
					list.add(i);
				}
			}
		});
		
		return list;
		
	}
	
}

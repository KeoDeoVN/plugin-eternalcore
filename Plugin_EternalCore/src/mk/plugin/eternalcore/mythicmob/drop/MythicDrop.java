package mk.plugin.eternalcore.mythicmob.drop;

public class MythicDrop {
	
	private String itemID;
	private int amount;
	private double chance;
	
	public MythicDrop(String itemID, int amount, double chance) {
		this.itemID = itemID;
		this.amount = amount;
		this.chance = chance;
	}
	
	public String getItemID() {
		return this.itemID;
	}
	
	public int getAmount() {
		return this.amount;
	}
	
	public double getChance() {
		return this.chance;
	}
	
}

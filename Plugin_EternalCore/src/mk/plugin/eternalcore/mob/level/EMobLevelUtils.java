package mk.plugin.eternalcore.mob.level;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bukkit.entity.LivingEntity;

import io.lumine.xikage.mythicmobs.MythicMobs;

public class EMobLevelUtils {
	
	public static int getLevel(LivingEntity e) {
		if (MythicMobs.inst().getAPIHelper().isMythicMob(e)) {
			String name = e.getName();
			String regex = "Lv\\.(?<level>\\d+)";
			Pattern p = Pattern.compile(regex);
			Matcher m = p.matcher(name);
			while (m.find()) {
				return Integer.valueOf(m.group("level"));
			}
		}
		return 0;
	}

	public static int getMobExp(int moblevel) {
		return moblevel * 2;
	}
	
	public static boolean isBoss(LivingEntity e) {
		if (MythicMobs.inst().getAPIHelper().isMythicMob(e)) {
			String name = e.getName();
			return name.contains("§4");
		}
		return false;
	}
	
	
	
}

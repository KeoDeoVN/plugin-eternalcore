package mk.plugin.eternalcore.item;

import java.util.List;
import java.util.Map;

import org.bukkit.entity.Player;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.feature.buff.EStatBuff;
import mk.plugin.eternalcore.feature.set.ESet;
import mk.plugin.eternalcore.feature.set.ESetUtils;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;

public class EItem {
	
	private String name = "";
	private String desc = "";
	private int level = 0;
	private ETier tier = ETier.COMMON;
	private int socketHole = 0;
	private List<ESocket> sockets = Lists.newArrayList();	
	private Map<EStat, Integer> stats = Maps.newHashMap();
	private Map<PotionEffectType, EItemPotion> potions = Maps.newHashMap();
	private Map<PotionEffectType, Integer> passivePotions = Maps.newHashMap();
	private boolean isLocked = false;
	
	
	public EItem(String name) {
		this.name = name.replace("&", "§").replace("_", " ");
	}
	
	public EItem(String name, int level) {
		this.name = name.replace("&", "§").replace("_", " ");
		this.level = level;
	}
	
	public EItem(String name, int level, boolean isLocked, ETier tier, int socketHole, Map<EStat, Integer> stats, Map<PotionEffectType, EItemPotion> potions, Map<PotionEffectType, Integer> passivePotions, List<ESocket> sockets) {
		this.name = name != null ? name.replace("&", "§").replace("_", " ") : name;
		this.level = level;
		this.stats = stats;
		this.potions = potions;
		this.passivePotions = passivePotions;
		this.tier = tier;
		this.socketHole = socketHole;
		this.sockets = sockets;
		this.isLocked = isLocked;
	}
	
	public EItem(String name, String desc, int level, boolean isLocked, ETier tier, int socketHole, Map<EStat, Integer> stats, Map<PotionEffectType, EItemPotion> potions, Map<PotionEffectType, Integer> passivePotions, List<ESocket> sockets) {
		this.name = name != null ? name.replace("&", "§").replace("_", " ") : name;
		this.desc = desc;
		this.level = level;
		this.stats = stats;
		this.potions = potions;
		this.passivePotions = passivePotions;
		this.tier = tier;
		this.socketHole = socketHole;
		this.sockets = sockets;
		this.isLocked = isLocked;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public Map<EStat, Integer> getStats() {
		return this.stats;
	}
	
	public String getDesc() {
		return this.desc;
	}
	
	public int getCalculatedStat(Player player, EStat stat) {
		int value = this.getStats().getOrDefault(stat, 0);
		if (value == 0) return 0;
		
		// Level
		value += 3 * this.getLevel();
		
		// Socket
		for (ESocket sk : this.getSockets()) {
			if (sk.getStat() == stat) {
				value += Math.max(1, value * sk.getBuff() / 100);
			}
		}
		
		// ESet
		if (ESetUtils.isInAnySet(this.name)) {
			ESet set = ESetUtils.getSetFromItem(name);
			int amount = ESetUtils.getAmount(player, set);
			if (amount > 1) {
				for (EStatBuff buff : set.getBuffs(amount)) {
					if (buff.getStat() == stat) {
						value += Math.max(1, value * buff.getValue() / 100);
					}

				};
			}
		}
		
		return value;
	}
	
	public void setStats(Map<EStat, Integer> stats) {
		this.stats = stats;
	}
	
	public void setStat(EStat stat, int value) {
		if (value == 0) this.stats.remove(stat);
		else this.stats.put(stat, value);
	}
	
	public void setLevel(int level) {
		this.level = level;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public void setDesc(String desc) {
		this.desc = desc;
	}
	
	public Map<PotionEffectType, EItemPotion> getPotions() {
		return this.potions;
	}
	
	public void setPotion(PotionEffectType type, EItemPotion p) {
		this.potions.put(type, p);
	}
	
	public Map<PotionEffectType, Integer> getPassivePotions() {
		return this.passivePotions;
	}
	
	public void setPassivePotion(PotionEffectType type, int level) {
		this.passivePotions.put(type, level);
	}

	public boolean isLocked() {
		return isLocked;
	}

	public void setLocked(boolean isLocked) {
		this.isLocked = isLocked;
	}

	public ETier getTier() {
		return tier;
	}

	public int getSocketHole() {
		return socketHole;
	}

	public void setSocketHole(int socketHole) {
		this.socketHole = socketHole;
	}

	public List<ESocket> getSockets() {
		return sockets;
	}

	public void setSockets(List<ESocket> sockets) {
		this.sockets = sockets;
	}
	
	public void setSocket(ESocket socket) {
		this.sockets.add(socket);
	}
	
	public void setTier(ETier tier) {
		this.tier = tier;
	}
	
	public int getEmptySocketHole() {
		return this.socketHole - this.sockets.size();
	}
	
	public void addEmptySocketHole() {
		this.socketHole++;
	}
	
	public void removeSocket(ESocket socket) {
		this.sockets.remove(socket);
	}
	
	public void clearSockets() {
		this.sockets.clear();
	}
	
}

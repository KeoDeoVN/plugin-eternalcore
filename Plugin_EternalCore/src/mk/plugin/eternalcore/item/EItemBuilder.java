package mk.plugin.eternalcore.item;

import java.util.List;
import java.util.Map;

import org.bukkit.ChatColor;
import org.bukkit.enchantments.Enchantment;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemFlag;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.feature.set.ESet;
import mk.plugin.eternalcore.feature.set.ESetUtils;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class EItemBuilder {
	
	public static void buildItem(Player player, ItemStack item, EItem ei) {
		String name = ei.getName();
		String desc = ei.getDesc();
		int level = ei.getLevel();
		ETier tier = ei.getTier();
		int socketHole = ei.getSocketHole();
		int emptySocketHole = ei.getEmptySocketHole();
		List<ESocket> sockets = ei.getSockets();	
		Map<EStat, Integer> stats = ei.getStats();
		Map<PotionEffectType, EItemPotion> potions = ei.getPotions();
		Map<PotionEffectType, Integer> passivePotions = ei.getPassivePotions();
		
		// Name
		ItemStackUtils.setDisplayName(item, getLevelString(level) + tier.getColor() + "§l" + ChatColor.stripColor(name) + getLockedLore(ei));
		
		List<String> lore = Lists.newArrayList();
		
		// Desc
		if (desc != null && desc.length() != 0) {
			lore.add(EConfig.DESC_LINE);
			lore.addAll(Utils.toList(desc, 21, "§7§o"));
		}
		
		// Stats
		lore.add(EConfig.STAT_LINE);
		stats.forEach((stat, value) -> {
			lore.add(getStatLine(ei, stat, value));
		});
		
		// Active Potions
		if (potions.size() > 0) {
			lore.add(EConfig.ACTIVE_POTION_LINE);
			potions.forEach((type, potion) -> {
				lore.add(getPotionLine(type, potion));
			});
		}
		
		// Passive Potions
		if (passivePotions.size() > 0) {
			lore.add(EConfig.PASSIVE_POTION_LINE);
			passivePotions.forEach((type, i) -> {
				lore.add(getPassivePotionLine(type, i));
			});
		}
		
		// Enchants
		if (item.getEnchantments().size() > 0) {
			lore.add(EConfig.ENCHANT_LINE);
			item.getEnchantments().forEach((e, i) -> {
				lore.add(getEnchantLine(e, i));
			});
		}
		
		// Socket
		if (socketHole > 0) {
			lore.add(EConfig.SOCKET_LINE);
			sockets.forEach(socket -> {
				lore.add(getSocketLine(socket));
			});
			for (int i = 0 ; i < emptySocketHole ; i++) {
				lore.add(getEmptySocketLine());
			}
		}
		
		if (player != null) {
			// Set
			if (ESetUtils.isInAnySet(name)) {
				// Check if amount >= 2
				ESet set = ESetUtils.getSetFromItem(name);
				int amount = ESetUtils.getAmount(player, set);
				lore.add(EConfig.SET_LINE);
				lore.add(getSetNameLine(set, amount));
				
				// Add desc line
				for (int i = 2 ; i <= set.getBuffs().size() + 1; i++) {
					boolean active = i == amount;
					lore.add(getSetLine(set,i , active));
				}
			}
		}
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.setUnbreakable(item, true);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_UNBREAKABLE);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ATTRIBUTES);
		ItemStackUtils.addFlag(item, ItemFlag.HIDE_ENCHANTS);
		
	}
	
	public static String getLevelString(int level) {
		return level == 0 ? "" : "§c§l[§f§l+" + level + "§c§l] ";
	}
	
	public static String getStatLine(EStat stat, int value, int bonus) {
		if (bonus != 0) return "§a" + stat.getName() + ": §f" + value + " §7(+" + bonus + ")";
		else return "§a" + stat.getName() + ": §f" + value;
	}
	
	public static String getStatLine(EItem ei, EStat stat, int value) {
		int bonus = 3 * ei.getLevel();
		if (bonus != 0) return "§a" + stat.getName() + ": §f" + value + " §7(+" + bonus + ")";
		else return "§a" + stat.getName() + ": §f" + value;
	}
	
	public static String getPotionLine(PotionEffectType type, EItemPotion potion) {
		return "§d" + Utils.getVNPotionName(type) + " " + Utils.getRomanNumber(potion.getLevel()) + ": §f" + potion.getChance() + "%";
	}
	
	public static String getPassivePotionLine(PotionEffectType type, int level) {
		return "§6" + "Nội tại: §e" + Utils.getVNPotionName(type) + " " + Utils.getRomanNumber(level);
	}
	
	public static String getSocketLine(ESocket sk) {
		return "§e■ +" + sk.getBuff() + "% " + sk.getStat().getName(); 
	}
	
	public static String getEmptySocketLine() {
		return "§e□ <Trống>";
	}
	
	public static String getEnchantLine(Enchantment e, int i) {
		return "§b◆ " + Utils.getVNEnchantName(e, i);
	}
	
	public static String getLockedLore(EItem ei) {
		return ei.isLocked() ? EConfig.LOCKED_LINE : "";
	}
	
	public static String getSetLine(ESet set, int amount, boolean isActive) {
		String prefix = isActive ? "§a" : "§7";
		return prefix + set.getDesc(amount);
	}
	
	public static String getSetNameLine(ESet set, int amount) {
		return "§3Set đồ: §a" + set.getName() + " [" + amount + "/" + set.getItems().size() + "]";
	}
	
}

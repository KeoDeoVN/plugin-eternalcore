package mk.plugin.eternalcore.item;

public class EItemPotion {
	
	private int level;
	private double chance;
	
	public EItemPotion(int level, double chance) {
		this.level = level;
		this.chance = chance;
	}
	
	public int getLevel() {
		return this.level;
	}
	
	public double getChance() {
		return this.chance;
	}
	
	public String toString() {
		return level + "-" + chance;
	}
	
	public static EItemPotion fromString(String s) {
		int level = Integer.valueOf(s.split("-")[0]);
		double chance = Double.valueOf(s.split("-")[1]);
		return new EItemPotion(level, chance);
	}
	
}

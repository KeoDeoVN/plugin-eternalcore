package mk.plugin.eternalcore.item;

import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.bukkit.craftbukkit.v1_12_R1.inventory.CraftItemStack;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Maps;

import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import net.minecraft.server.v1_12_R1.NBTTagCompound;

public class EItemUtils {
	
	public static boolean isEItem(ItemStack item) {
		return ItemStackUtils.hasTag(item, "eitem");
	}
	
	public static EItem fromItem(ItemStack item) {
    	net.minecraft.server.v1_12_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item);
    	if (!nmsItemStack.hasTag()) return null;
    	NBTTagCompound tags = nmsItemStack.getTag();
		
		String name = EItemSerializer.getName(ItemStackUtils.getName(item), tags);
		String desc = EItemSerializer.getDesc("", tags);
		int level = EItemSerializer.getLevel(tags);
		ETier tier = EItemSerializer.getTier(tags);
		int socketHole = EItemSerializer.getSocketHole(tags);
		boolean isLocked = EItemSerializer.getIsLocked(tags);
		List<ESocket> sockets = EItemSerializer.getSockets(tags);	
		Map<EStat, Integer> stats = EItemSerializer.getStats(tags);
		Map<PotionEffectType, EItemPotion> potions = EItemSerializer.getPotions(tags);
		Map<PotionEffectType, Integer> passivePotions = EItemSerializer.getPassivePotions(tags);
		
		return new EItem(name, desc, level, isLocked, tier, socketHole, stats, potions, passivePotions, sockets);
	}
	
	public static ItemStack setItem(ItemStack item, EItem ei) {
    	net.minecraft.server.v1_12_R1.ItemStack nmsItemStack = CraftItemStack.asNMSCopy(item.clone());
    	NBTTagCompound tags = nmsItemStack.getTag();
		
		EItemSerializer.setIdentifier(tags);
		EItemSerializer.saveName(tags, ei.getName());
		EItemSerializer.saveDesc(tags, ei.getDesc());
		EItemSerializer.saveLevel(tags, ei.getLevel());
		EItemSerializer.saveTier(tags, ei.getTier());
		EItemSerializer.saveSocketHole(tags, ei.getSocketHole());
		EItemSerializer.saveIsLocked(tags, ei.isLocked());
		EItemSerializer.saveSockets(tags, ei.getSockets());
		EItemSerializer.saveStats(tags, ei.getStats());
		EItemSerializer.savePotions(tags, ei.getPotions());
		EItemSerializer.savePassivePotions(tags, ei.getPassivePotions());
		
    	nmsItemStack.setTag(tags);
		
		return nmsItemStack.asBukkitCopy();
	}
	
	public static ItemStack update(Player player, ItemStack item, EItem ei) {
		EItemBuilder.buildItem(player, item, ei);
		return setItem(item, ei);
	}
	
	public static void updateLore(Player player, ItemStack item) {
		EItem ei = fromItem(item);
		EItemBuilder.buildItem(player, item, ei);
	}
	
	public static String statsToString(Map<EStat, Integer> stats) {
		String s = "";
		if (stats.size() == 0) return s;
		for (Entry<EStat, Integer> e : stats.entrySet()) {
			s += e.getKey().name() + ":" + e.getValue() + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	public static Map<EStat, Integer> stringToStats(String s) {
		Map<EStat, Integer> map = Maps.newHashMap();
		if (s.length() == 0) return map;
		for (String ss : s.split(";")) {
			EStat stat = EStat.valueOf(ss.split(":")[0]);
			int value = Integer.valueOf(ss.split(":")[1]);
			map.put(stat, value);
		}
		return map;
	}
	
	public static String potionsToString(Map<PotionEffectType, EItemPotion> potions) {
		String s = "";
		if (potions.size() == 0) return s;
		for (Entry<PotionEffectType, EItemPotion> e : potions.entrySet()) {
			s += e.getKey().getName() + ":" + e.getValue().toString() + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	public static Map<PotionEffectType, EItemPotion> stringToPotions(String s) {
		Map<PotionEffectType, EItemPotion> map = Maps.newHashMap();
		if (s.length() == 0) return map;
		for (String ss : s.split(";")) {
			PotionEffectType pt = PotionEffectType.getByName(ss.split(":")[0]);
			EItemPotion p = EItemPotion.fromString(ss.split(":")[1]);
			map.put(pt, p);
		}
		return map;	
	}
	
	public static String passivePotionsToString(Map<PotionEffectType, Integer> passivePotions) {
		String s = "";
		if (passivePotions.size() == 0) return s;
		for (Entry<PotionEffectType, Integer> e : passivePotions.entrySet()) {
			s += e.getKey().getName() + ":" + e.getValue() + ";";
		}
		return s.substring(0, s.length() - 1);
	}
	
	public static Map<PotionEffectType, Integer> stringToPassivePotions(String s) {
		Map<PotionEffectType, Integer> map = Maps.newHashMap();
		if (s.length() == 0) return map;
		for (String ss : s.split(";")) {
			PotionEffectType stat = PotionEffectType.getByName(ss.split(":")[0]);
			int value = Integer.valueOf(ss.split(":")[1]);
			map.put(stat, value);
		}
		return map;
	}
	
}

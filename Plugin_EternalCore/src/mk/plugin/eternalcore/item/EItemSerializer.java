package mk.plugin.eternalcore.item;

import java.util.List;
import java.util.Map;

import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import net.minecraft.server.v1_12_R1.NBTTagCompound;

public class EItemSerializer {
	
	public static void setIdentifier(NBTTagCompound tags) {
		tags.setString("eitem", "ditmemay");
	}
	
	public static void saveName(NBTTagCompound tags, String name) {
		tags.setString("eitem.name", name);
	}
	
	public static String getName(String defaultName, NBTTagCompound tags) {
		if (!tags.hasKey("eitem.name")) return defaultName;
		return tags.getString("eitem.name");
	}
	
	public static void saveDesc(NBTTagCompound tags, String desc) {
		tags.setString("eitem.desc", desc);
	}
	
	public static String getDesc(String defaultDesc, NBTTagCompound tags) {
		if (!tags.hasKey("eitem.desc")) return defaultDesc;
		return tags.getString("eitem.desc");
	}
	
	public static void saveLevel(NBTTagCompound tags, int level) {
		tags.setString("eitem.level", "" + level);
	}
	
	public static int getLevel(NBTTagCompound tags) {
		if (!tags.hasKey("eitem.level")) return 0;
		try {
			return Integer.valueOf(tags.getString("eitem.level"));
		}
		catch (Exception e) {
			return 0;
		}
	}
	
	public static void saveTier(NBTTagCompound tags, ETier tier) {
		tags.setString("eitem.tier", "" + tier.name());
	}
	
	public static ETier getTier(NBTTagCompound tags) {
		if (!tags.hasKey("eitem.tier")) return ETier.COMMON;
		try {
			return ETier.valueOf(tags.getString("eitem.tier"));
		}
		catch (Exception e) {
			return ETier.COMMON;
		}
	}
	
	public static void saveSocketHole(NBTTagCompound tags, int hole) {
		tags.setString("eitem.sockethole", "" + hole);
	}
	
	public static int getSocketHole(NBTTagCompound tags) {
		if (!tags.hasKey("eitem.sockethole")) return 0;
		return Integer.valueOf(tags.getString("eitem.sockethole"));
	}
	
	public static void saveSockets(NBTTagCompound tags, List<ESocket> list) {
		String s = "";
		for (ESocket sk : list) s = s.concat(sk.toString() + ";");
		if (s.length() > 0) s = s.substring(0, s.length() - 1);
		tags.setString("eitem.sockets", s);
	}
	
	public static List<ESocket> getSockets(NBTTagCompound tags) {
		List<ESocket> list = Lists.newArrayList();
		if (!tags.hasKey("eitem.sockets")) return list;
		String s = tags.getString("eitem.sockets");
		if (s.length() == 0) return list;
		for (String ss : s.split(";")) {
			list.add(ESocket.fromString(ss));
		}
		return list;
	}
	
	public static void saveStats(NBTTagCompound tags, Map<EStat, Integer> stats) {
		tags.setString("eitem.stats", EItemUtils.statsToString(stats));
	}
	
	public static Map<EStat, Integer> getStats(NBTTagCompound tags) {
		Map<EStat, Integer> map = Maps.newHashMap();
		if (!tags.hasKey("eitem.stats")) return map;
		return EItemUtils.stringToStats(tags.getString("eitem.stats"));
	}
	
	public static void savePotions(NBTTagCompound tags, Map<PotionEffectType, EItemPotion> potions) {
		tags.setString("eitem.potions", EItemUtils.potionsToString(potions));
	}
	
	public static Map<PotionEffectType, EItemPotion> getPotions(NBTTagCompound tags) {
		Map<PotionEffectType, EItemPotion> map = Maps.newHashMap();
		if (!tags.hasKey("eitem.potions")) return map;
		return EItemUtils.stringToPotions(tags.getString("eitem.potions"));
	}
	
	public static void savePassivePotions(NBTTagCompound tags, Map<PotionEffectType, Integer> potions) {
		tags.setString("eitem.passivepotions", EItemUtils.passivePotionsToString(potions));
	}
	
	public static Map<PotionEffectType, Integer> getPassivePotions(NBTTagCompound tags) {
		Map<PotionEffectType, Integer> map = Maps.newHashMap();
		if (!tags.hasKey("eitem.passivepotions")) return map;
		return EItemUtils.stringToPassivePotions(tags.getString("eitem.passivepotions"));
	}
	
	public static void saveIsLocked(NBTTagCompound tags, boolean isLocked) {
		tags.setString("eitem.isLocked", "" + isLocked);
	}
	
	public static boolean getIsLocked(NBTTagCompound tags) {
		if (!tags.hasKey("eitem.isLocked")) return false;
		return Boolean.valueOf(tags.getString("eitem.isLocked"));
	}
	
}

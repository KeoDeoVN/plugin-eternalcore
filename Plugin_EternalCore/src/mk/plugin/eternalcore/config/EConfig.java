package mk.plugin.eternalcore.config;

import java.util.List;

import org.bukkit.configuration.file.FileConfiguration;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.feature.buff.EStatBuff;
import mk.plugin.eternalcore.feature.set.ESet;
import mk.plugin.eternalcore.stat.EStat;

public class EConfig {
	
	public static String DESC_LINE;
	public static String LOCKED_LINE;
	public static String STAT_LINE;
	public static String ACTIVE_POTION_LINE;
	public static String PASSIVE_POTION_LINE;
	public static String SOCKET_LINE;
	public static String ENCHANT_LINE;
	public static String SET_LINE;
	
	public static List<ESet> SETS = Lists.newArrayList();
	
	public static List<String> WORLDS_WEAPONSOUL = Lists.newArrayList();
	public static List<String> AUTO_STAT_MODIFIED_WORLDS = Lists.newArrayList();
	
	public static void init(FileConfiguration config) {
		DESC_LINE = config.getString("desc-lore").replace("&", "§");
		LOCKED_LINE = config.getString("locked-lore").replace("&", "§");
		STAT_LINE = config.getString("stat-lore").replace("&", "§");
		ACTIVE_POTION_LINE = config.getString("active-potion-lore").replace("&", "§");
		PASSIVE_POTION_LINE = config.getString("passive-potion-lore").replace("&", "§");
		SOCKET_LINE = config.getString("socket-lore").replace("&", "§");
		ENCHANT_LINE = config.getString("enchant-lore").replace("&", "§");
		SET_LINE = config.getString("set-lore").replace("&", "§");
		
		SETS.clear();
		config.getConfigurationSection("set").getKeys(false).forEach(id -> {
			String name = config.getString("set." + id + ".name");
			List<String> items = config.getStringList("set." + id + ".items");
			List<String> descs = config.getStringList("set." + id + ".descs");
			List<List<EStatBuff>> buffs = Lists.newArrayList();
			config.getStringList("set." + id + ".buffs").forEach(s -> {
				List<EStatBuff> list = Lists.newArrayList();
				for (String c : s.split(";")) {
					EStat stat = EStat.valueOf(c.split(":")[0].toUpperCase());
					double value = Double.valueOf(c.split(":")[1]);
					list.add(new EStatBuff(stat, value, 0, 0));
				}
				buffs.add(list);
			});
			SETS.add(new ESet(name, items, buffs, descs));
		});
		
		WORLDS_WEAPONSOUL = config.getStringList("world-weaponsoul");
		AUTO_STAT_MODIFIED_WORLDS = config.getStringList("auto-stat-modify-worlds");
	}
	
}

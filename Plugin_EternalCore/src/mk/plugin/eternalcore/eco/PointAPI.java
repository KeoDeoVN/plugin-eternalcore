package mk.plugin.eternalcore.eco;

import org.bukkit.entity.Player;

import mk.plugin.eternalcore.main.MainEC;

public class PointAPI {
	
	@SuppressWarnings("deprecation")
	public static int getPoint(Player player) {
		int points = MainEC.getPP().getAPI().look(player.getName());
		return points;
	}
	
	public static boolean pointCost(Player player, int points) {
		if (points > getPoint(player)) {
			return false;
		} else {
			MainEC.getPP().getAPI().take(player.getUniqueId(), points);
			return true;
		}
	}
	
	public static void givePoint(Player player, int point) {
		MainEC.getPP().getAPI().give(player.getUniqueId(), point);
	}
	
	
}

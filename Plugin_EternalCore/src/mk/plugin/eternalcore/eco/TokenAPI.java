package mk.plugin.eternalcore.eco;

import java.util.OptionalLong;

import org.bukkit.entity.Player;

import mk.plugin.eternalcore.main.MainEC;

public class TokenAPI {
	
	public static boolean tokenCost(Player player, int value) {
		OptionalLong l = MainEC.getTokenManager().getTokens(player);
		if (l.getAsLong() < value) return false;
		MainEC.tokenM.setTokens(player, MainEC.getTokenManager().getTokens(player).getAsLong() - value);
		return true;
	}
	
	public static void give(Player player, int value) {
		MainEC.getTokenManager().setTokens(player, MainEC.getTokenManager().getTokens(player).getAsLong() + value);
	}
	
}

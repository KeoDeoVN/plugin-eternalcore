package mk.plugin.eternalcore.eco;

import org.bukkit.entity.Player;

import mk.plugin.eternalcore.main.MainEC;
import net.milkbowl.vault.economy.Economy;

public class MoneyAPI {
	
	public static double getMoney(Player player) {
		Economy eco = MainEC.getEcononomy();
		
		return eco.getBalance(player);
	}

	public static boolean moneyCost(Player player, double money) {
		Economy eco = MainEC.getEcononomy();
		double moneyOfPlayer = eco.getBalance(player);
		if (moneyOfPlayer < money) {
			return false;
		}
		eco.withdrawPlayer(player, money);
		return true;
		
	}
	
	public static void giveMoney(Player player, double money) {
		Economy eco = MainEC.getEcononomy();
		eco.depositPlayer(player, money);
	}
	
}

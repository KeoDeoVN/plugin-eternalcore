package mk.plugin.eternalcore.stat;

import org.bukkit.attribute.Attribute;
import org.bukkit.entity.Player;

import mk.plugin.eternalcore.util.Utils;

public enum EStat {
	
	
	DAMAGE("Sát thương", 1) {
		@Override
		public double pointsToValue(int point) {
			return point * 0.9;
		}

		@Override
		public void set(Player player, int point) {}

		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	HEALTH("Sinh lực", 10) {
		@Override
		public double pointsToValue(int point) {
			return Math.max(getMinimum(), point * 2.5);
		}
		
		@Override
		public void set(Player player, int point) {
			player.getAttribute(Attribute.GENERIC_MAX_HEALTH).setBaseValue(pointsToValue(point));
			if (player.getHealth() > player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue()) {
				player.setHealth(player.getAttribute(Attribute.GENERIC_MAX_HEALTH).getValue());
			}
		}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	DEFENSE("Sức thủ", 0) {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 150))) * 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	ATTACKSPEED("Tốc đánh", 0) {
		@Override
		public double pointsToValue(int point) {
			return 1 - ((double) point / (point + 50)) * 1;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	DODGE("Né đòn", 0) {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 100))) * 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	},
	
	HEAL("Hồi phục", 1) {
		@Override
		public double pointsToValue(int point) {
			return 1 + point * 0.07;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	LIFESTEAL("Hút máu", 1) {
		@Override
		public double pointsToValue(int point) {
			return (5 + Utils.round(((double) point / (point + 200))) * 20) / 100;
		}
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "";
		}
	},
	
	ACCURACY("Chí mạng", 1) {
		@Override
		public double pointsToValue(int point) {
			return Utils.round(((double) point / (point + 200))) * 100;
		} 
		
		@Override
		public void set(Player player, int point) {}
		
		@Override
		public String getSubStat() {
			return "%";
		}
	};
//	,
//	
//	
//	COUNTER("Phản đòn", 0) {
//		@Override
//		public double pointsToValue(int point) {
//			return Utils.round(((double) point / (point + 150))) * 100;
//		}
//		
//		@Override
//		public void set(Player player, int point) {}
//		
//		@Override
//		public String getSubStat() {
//			return "%";
//		}
//	};
	
	public abstract double pointsToValue(int point);
	public abstract void set(Player player, int point);
	public abstract String getSubStat();
	
	private String name;
	private int minValue;
	
	private EStat(String name, int minValue) {
		this.name = name;
		this.minValue = minValue;
	}
	
	public String getName() {
		return this.name;
	}
	
	public int getMinimum() {
		return this.minValue;
	}
	
}

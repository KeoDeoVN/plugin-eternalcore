package mk.plugin.eternalcore.task;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.player.EPlayerListNameUtils;

public class EListNameTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			player.setPlayerListName(EPlayerListNameUtils.getListName(player));
		});
	}

}

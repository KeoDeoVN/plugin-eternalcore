package mk.plugin.eternalcore.mc;

import java.util.List;
import java.util.Map;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.stat.EStat;

public enum MCItemType {
	
	BOW("Cung", Lists.newArrayList("BOW")) {
		@Override
		public Map<EStat, Double> getStatBonus() {
			Map<EStat, Double> map = Maps.newHashMap();
			map.put(EStat.DAMAGE, 0.25);
			map.put(EStat.ATTACKSPEED, 0.25);
			return map;
		}
	},
	SWORD("Kiếm, thương, lưỡi hái", Lists.newArrayList("_SWORD")) {
		@Override
		public Map<EStat, Double> getStatBonus() {
			Map<EStat, Double> map = Maps.newHashMap();
			map.put(EStat.ATTACKSPEED, 0.5);
			return map;
		}
	},
	AXE("Rìu, chùy", Lists.newArrayList("_AXE")) {
		@Override
		public Map<EStat, Double> getStatBonus() {
			Map<EStat, Double> map = Maps.newHashMap();
			map.put(EStat.DAMAGE, 0.5);
			return map;
		}
	},
	SHIELD("Khiên", Lists.newArrayList("SHIELD")) {
		@Override
		public Map<EStat, Double> getStatBonus() {
			Map<EStat, Double> map = Maps.newHashMap();
			map.put(EStat.DEFENSE, 0.5);
			return map;
		}
	},
	ARMOR("Giáp", Lists.newArrayList("HEAD", "HELMET", "CHESTPLATE", "LEGGINGS", "BOOTS")) {
		@Override
		public Map<EStat, Double> getStatBonus() {
			Map<EStat, Double> map = Maps.newHashMap();
			map.put(EStat.HEALTH, 0.25);
			map.put(EStat.DEFENSE, 0.25);
			return map;
		}
	};
	
	private String name;
	private List<String> suffixs;
	
	private MCItemType(String name, List<String> suffixs) {
		this.suffixs = suffixs;
		this.name = name;
	}
	
	public abstract Map<EStat, Double> getStatBonus();
	
	public String getName() {
		return this.name;
	}
	
	public List<Material> getMaterialList() {
		List<Material> list = Lists.newArrayList();
		for (Material m : Material.values()) {
			boolean is = false;
			for (String suffix : suffixs) {
				if (m.name().endsWith(suffix)) {
					is = true;
					break;
				}
			}
			if (is) list.add(m);
		}
		return list;
	}
	
	public static MCItemType getType(Material material) {
		for (MCItemType type : values()) {
			if (type.getMaterialList().contains(material)) return type;
		}
		return null;
	}
	
	public static boolean isHoldingType(MCItemType type, Player player) {
		ItemStack item = player.getInventory().getItemInMainHand();
		if (item == null || item.getType () == Material.AIR) return false;
		return getType(item.getType()) == type;
	}
	
}

package mk.plugin.eternalcore.heal;

import org.bukkit.Bukkit;
import org.bukkit.scheduler.BukkitRunnable;

import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.util.Utils;

public class EHealTask extends BukkitRunnable {

	@Override
	public void run() {
		Bukkit.getOnlinePlayers().forEach(player -> {
			if (player.isDead()) return;
			EPlayer ep = EPlayerDataUtils.getData(player);
			if (ep == null) return;
			double value = ep.getStatValue(EStat.HEAL, player);
			Utils.addHealth(player, value);
		});
	}

}

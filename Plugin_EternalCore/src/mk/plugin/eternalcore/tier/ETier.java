package mk.plugin.eternalcore.tier;

public enum ETier {
	
	COMMON("Thường", "§7", 1),
	UNCOMMON("Hiếm", "§c", 1.5),
	EPIC("Cực phẩm", "§6", 2),
	LEGENDARY("Huyền thoại", "§e", 3);
	
	private String name;
	private String color;
	private double multi;
	
	private ETier(String name, String color, double multi) {
		this.name = name;
		this.color = color;
		this.multi = multi;
	}
	
	public String getName() {
		return this.name;
	}
	
	public String getColor() {
		return this.color;
	}
	
	public double getMulti() {
		return this.multi;
	}
	
}

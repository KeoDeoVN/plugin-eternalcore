package mk.plugin.eternalcore.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import mk.plugin.eternalcore.feature.potential.EPotentialGUI;
import mk.plugin.eternalcore.feature.see.ESeeGUI;
import mk.plugin.eternalcore.feature.skill.ESGUI;

public class EPlayerCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command cmd, String arg2, String[] args) {
		
		if (!(sender instanceof Player )) return false;
		Player player = (Player) sender;
		
		if (args.length == 1 && cmd.getName().equalsIgnoreCase("see")) {
			Player target = Bukkit.getPlayer(args[0]);
			if (target == null) {
				player.sendMessage("§aKhông có member nào tên " + args[0]);
				return false;
			}
			ESeeGUI.openGUI(target, player);
		}
		
		else if (cmd.getName().equalsIgnoreCase("potential")) {
			EPotentialGUI.openGUI(player);
		}
		
		else if (cmd.getName().equalsIgnoreCase("skill")) {
			ESGUI.openGUI(player);
		}
		
		return false;
	}

}

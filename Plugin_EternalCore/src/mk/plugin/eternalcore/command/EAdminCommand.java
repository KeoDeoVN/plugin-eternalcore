package mk.plugin.eternalcore.command;

import java.util.List;
import java.util.Map;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.potion.PotionEffectType;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;

import mk.plugin.eternalcore.feature.chuyenhoa.ECHHGUI;
import mk.plugin.eternalcore.feature.cuonghoa.ECHGUI;
import mk.plugin.eternalcore.feature.cuonghoa.EDCH;
import mk.plugin.eternalcore.feature.cuonghoa.ESMM;
import mk.plugin.eternalcore.feature.dabaoho.EKeepGem;
import mk.plugin.eternalcore.feature.gems.EGemGUI;
import mk.plugin.eternalcore.feature.gems.EGemUtils;
import mk.plugin.eternalcore.feature.infogui.EInfoGUI;
import mk.plugin.eternalcore.feature.potential.EPotential;
import mk.plugin.eternalcore.feature.potential.EPotentialGUI;
import mk.plugin.eternalcore.feature.skill.ESGUI;
import mk.plugin.eternalcore.feature.skill.ESkill;
import mk.plugin.eternalcore.feature.socket.duclo.EDucLoGUI;
import mk.plugin.eternalcore.feature.socket.hopthe.ESHopTheGUI;
import mk.plugin.eternalcore.feature.socket.khamda.EKhamDaGUI;
import mk.plugin.eternalcore.feature.socket.tach.ESocketSplitGUI;
import mk.plugin.eternalcore.feature.tinhluyen.EDTL;
import mk.plugin.eternalcore.feature.tinhluyen.EDTLUtils;
import mk.plugin.eternalcore.feature.tinhluyen.EHopTheGUI;
import mk.plugin.eternalcore.feature.tinhluyen.ERDTL;
import mk.plugin.eternalcore.feature.tinhluyen.ERDTLUtils;
import mk.plugin.eternalcore.feature.tinhluyen.ETLGUI;
import mk.plugin.eternalcore.feature.unlock.EUnlockGUI;
import mk.plugin.eternalcore.feature.weaponsoul.EWSGUI;
import mk.plugin.eternalcore.item.EItem;
import mk.plugin.eternalcore.item.EItemBuilder;
import mk.plugin.eternalcore.item.EItemPotion;
import mk.plugin.eternalcore.item.EItemUtils;
import mk.plugin.eternalcore.main.MainEC;
import mk.plugin.eternalcore.mc.MCItemType;
import mk.plugin.eternalcore.player.EPlayer;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.player.EPlayerLevelUtils;
import mk.plugin.eternalcore.socket.ERSocket;
import mk.plugin.eternalcore.socket.ERSocketUtils;
import mk.plugin.eternalcore.socket.ESocket;
import mk.plugin.eternalcore.socket.ESocketUtils;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ChatPlaceholder;
import mk.plugin.eternalcore.util.Utils;
import net.md_5.bungee.api.ChatMessageType;
import net.md_5.bungee.api.chat.TextComponent;

public class EAdminCommand implements CommandExecutor {

	@SuppressWarnings("deprecation")
	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {

		if (!sender.hasPermission("eternal.*"))
			return false;

		// Common

		if (args.length == 0) {
			sendTut(sender);
			return false;
		}
		
		if (args[0].equalsIgnoreCase("reload")) {
			MainEC.plugin.reloadConfig();
			sender.sendMessage("Configuration reloaded!");
		}
		
		else if (args[0].equalsIgnoreCase("m")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				String s = "";
				for (int i = 2 ; i < args.length ; i++) {
					s += args[i] + " ";
				}
				s = s.substring(0, s.length() - 1);
				target.sendMessage(s.replace("&", "§"));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/e m <player> <content>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("resetpotential")) {
			Player target = Bukkit.getPlayer(args[1]);
			EPlayer ep = EPlayerDataUtils.getData(target);
			for (EPotential p : EPotential.values()) ep.setPotential(p, 0);
			target.sendMessage("§aTiềm năng đã được reset");
			sender.sendMessage("Ok");
		}
		
		else if (args[0].equalsIgnoreCase("resetskill")) {
			Player target = Bukkit.getPlayer(args[1]);
			EPlayer ep = EPlayerDataUtils.getData(target);
			ep.getSkills().keySet().forEach(skill -> ep.setSkill(skill, 0));
			target.sendMessage("§aKỹ năng đã được reset");
			sender.sendMessage("Ok");
		}
		
		else if (args[0].equalsIgnoreCase("renameitem")) {
			Player target = Bukkit.getPlayer(args[1]);
			if (args.length == 3) {
				String name = args[2].replace("_", " ");
				ItemStack item = target.getInventory().getItemInMainHand();
				Bukkit.getScheduler().runTaskAsynchronously(MainEC.plugin, ()  -> {
					EItem rpgI = EItemUtils.fromItem(item);
					if (EItemUtils.isEItem(item)) {
						rpgI.setName("*" + name.replace("&", "§"));
						ItemStack i = EItemUtils.setItem(item, rpgI);
						target.getInventory().setItemInMainHand(i);
					} else {
						target.sendMessage("§cVật phẩm này không thể đổi tên!");
					}
				});
			}
			else {
				new ChatPlaceholder(target, "§aNhập tên bạn muốn đổi (dấu cách là \"_\"). §cĐặt tên vi phạm sẽ bị phạt!", "eternal renameitem "  + target.getName() + " <s>");
			}
		} 
		
		else if (args[0].equalsIgnoreCase("addexp")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				int exp = Integer.parseInt(args[2]);
				EPlayerLevelUtils.addExp(target, exp);
				target.spigot().sendMessage(ChatMessageType.ACTION_BAR, TextComponent.fromLegacyText("§a§l+" + exp + " exp"));
				target.playSound(target.getLocation(), Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e addexp <player> <exp>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("levelup")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				int level = Integer.valueOf(args[2]);
				if (target.getLevel() > level) return false;
				target.setLevel(level);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e levelup <player> <level>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("setplayerlevel")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				int level = Integer.valueOf(args[2]);
				target.setLevel(level);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setplayerlevel <player> <level>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("infogui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EInfoGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e infogui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("potentialgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EPotentialGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e potentialgui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("skillgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ESGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e skillgui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("weaponsoulgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EWSGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e weaponsoul <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("cuonghoagui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ECHGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e cuonghoagui <player>");
			}
		}

		else if (args[0].equalsIgnoreCase("chuyenhoagui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ECHHGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e chuyenhoagui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("gemgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EGemGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e gemgui <player>");
			}
		} 
		
		else if (args[0].equalsIgnoreCase("tinhluyengui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ETLGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e tinhluyengui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("hopthegui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EHopTheGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e tinhluyengui <player>");
			}
		} 
		
		else if (args[0].equalsIgnoreCase("duclogui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EDucLoGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e duclogui <player>");
			}
		}
		 
		else if (args[0].equalsIgnoreCase("khamdagui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EKhamDaGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e khamdagui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("unlockgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				EUnlockGUI.openGUI(target);
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e khamdagui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("socketsplitgui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ESocketSplitGUI.openGUI(target);                                                                                                                                                                                 
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/eternal socketsplitgui <player>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("sockethopthegui")) {
			try {
				Player target = Bukkit.getPlayer(args[1]);
				ESHopTheGUI.openGUI(target);                                                                                                                                                                                 
				sender.sendMessage("§aOpened GUI for " + target.getName());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/eternal socketsplitgui <player>");
			}
		}

		// Ingame

		if (!(sender instanceof Player))
			return false;
		Player player = (Player) sender;

		ItemStack itemHand = player.getInventory().getItemInMainHand();
		EItem ei = null;
		if (EItemUtils.isEItem(itemHand)) ei = EItemUtils.fromItem(itemHand);

//		if (args[0].equalsIgnoreCase("shownbt")) {
//			try {
//				ItemStackUtils.getTags(itemHand).forEach((k, v) -> {
//					player.sendMessage(k + " " + v);
//				});
//			}
//			catch (ArrayIndexOutOfBoundsException e) {
//				sender.sendMessage("/e shownbt");
//			}
//		}

		
		if (args[0].equalsIgnoreCase("runskill")) {
			try {
				ESkill es = ESkill.valueOf(args[1]);
				es.getExecutor().run(player, Integer.parseInt(args[2]));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e runskill <skill> <player>");
			}
		}
		

		else if (args[0].equalsIgnoreCase("getch")) {
			try {
				for (EDCH dch : EDCH.values()) {
					Utils.giveItem(player, dch.getItem());
				}
				for (ESMM smm : ESMM.values()) {
					Utils.giveItem(player, smm.getItem());
				}
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e getch <player>");
			}
			
		}
		
		else if (args[0].equalsIgnoreCase("settaggem")) {
			try {
				player.getInventory().setItemInMainHand(EGemUtils.setGemTag(itemHand));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e settaggem <player>");
			}
			
		}
		
		else if (args[0].equalsIgnoreCase("testbuilditem")) {
			Map<EStat, Integer> stats = Maps.newHashMap();
			stats.put(EStat.DAMAGE, 15);
			stats.put(EStat.ATTACKSPEED, 10);
			stats.put(EStat.LIFESTEAL, 5);
			
			Map<PotionEffectType, EItemPotion> potions = Maps.newHashMap();
			potions.put(PotionEffectType.POISON, new EItemPotion(2, 60));
			potions.put(PotionEffectType.WITHER, new EItemPotion(1, 30));
			
			Map<PotionEffectType, Integer> passivePotions = Maps.newHashMap();
			passivePotions.put(PotionEffectType.SPEED, 2);
			passivePotions.put(PotionEffectType.HEAL, 1);
			
			List<ESocket> sockets = Lists.newArrayList();
			sockets.add(new ESocket(ETier.COMMON, EStat.DODGE, 10, 80, false));
			sockets.add(new ESocket(ETier.COMMON, EStat.DAMAGE, 8, 80, false));
			
			ei = new EItem("Mythic Pro Item", 5, true, ETier.EPIC, 5, stats, potions, passivePotions, sockets);
			player.getInventory().setItemInMainHand(EItemUtils.setItem(itemHand, ei));
			EItemBuilder.buildItem(player, player.getInventory().getItemInMainHand(), ei);
		}
		
		else if (args[0].equalsIgnoreCase("init")) {
			try {
				String name = "";
				for (int i = 1; i < args.length; i++) {
					name += args[i] + " ";
				}
				name = name.substring(0, name.length() - 1).replace("&", "§");
				EItem eitem = new EItem(name);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, eitem));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e init <name>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("setname")) {
			try {
				String name = "";
				for (int i = 1; i < args.length; i++) {
					name += args[i] + " ";
				}
				name = name.substring(0, name.length() - 1).replace("&", "§");
				ei.setName(name);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setname <name>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("setdesc")) {
			try {
				String name = "";
				for (int i = 1; i < args.length; i++) {
					name += args[i] + " ";
				}
				name = name.substring(0, name.length() - 1).replace("&", "§");
				ei.setDesc(name);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setdesc <value>");
			}
		}

		else if (args[0].equalsIgnoreCase("setlevel")) {
			try {
				int level = Integer.parseInt(args[1]);
				ei.setLevel(level);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setlevel <level>");
			}
		}
		


		else if (args[0].equalsIgnoreCase("setstat")) {
			try {
				EStat stat = EStat.valueOf(args[1].toUpperCase());
				int value = Integer.parseInt(args[2]);
				boolean mctypecheck = Boolean.valueOf(args[3]);
				if (mctypecheck) {
					MCItemType type = MCItemType.getType(itemHand.getType());
					if (type != null) {
						if (type.getStatBonus().containsKey(stat)) value *= 1 + type.getStatBonus().get(stat);
					}
				}
				ei.setStat(stat, value);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setstat <type> <value> <materialcheck>");
			}
		}


		
		else if (args[0].equalsIgnoreCase("setpotion1")) {
			try {
				PotionEffectType t = PotionEffectType.getByName(args[1]);
				int level = Integer.parseInt(args[2]);
				double chance = Double.valueOf(args[3]);
				EItemPotion ip = new EItemPotion(level, chance);
				ei.setPotion(t, ip);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setpotion1 <type> <level> <chance>");
			}
		}
		


		else if (args[0].equalsIgnoreCase("setpotion2")) {
			try {
				PotionEffectType t = PotionEffectType.getByName(args[1]);
				int level = Integer.parseInt(args[2]);
				ei.setPassivePotion(t, level);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setpotion2 <type> <level>");
			}
		}

		else if (args[0].equalsIgnoreCase("setsocket")) {
			try {
				ESocket sk = new ESocket(ETier.COMMON, EStat.valueOf(args[1].toUpperCase()), Integer.valueOf(args[2]), 0, false);
				ei.setSocket(sk);
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setsocket <stat> <buff>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("setlocked")) {
			try {
				ei.setLocked(Boolean.valueOf(args[1]));
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setlocked");
			}
		}
		
		else if (args[0].equalsIgnoreCase("setsockethole")) {
			try {
				ei.setSocketHole(Integer.valueOf(args[1]));
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e setsockethole <value>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("settier")) {
			try {
				ei.setTier(ETier.valueOf(args[1].toUpperCase()));;
				player.getInventory().setItemInMainHand(EItemUtils.update(player, itemHand, ei));
				player.updateInventory();
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e settier <type>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("runskill")) {
			try {
				ESkill es = ESkill.valueOf(args[1]);
				es.getExecutor().run(player, Integer.parseInt(args[2]));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e runskill <skill> <player>");
			}
		}
		

		else if (args[0].equalsIgnoreCase("getch")) {
			try {
				for (EDCH dch : EDCH.values()) {
					Utils.giveItem(player, dch.getItem());
				}
				for (ESMM smm : ESMM.values()) {
					Utils.giveItem(player, smm.getItem());
				}
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e getch <player>");
			}
			
		}
		
		else if (args[0].equalsIgnoreCase("settaggem")) {
			try {
				player.getInventory().setItemInMainHand(EGemUtils.setGemTag(itemHand));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("/e settaggem <player>");
			}
			
		}
		
		else if (args[0].equalsIgnoreCase("getdtl")) {
			try {
				ETier tier = ETier.valueOf(args[2].toUpperCase());
				EStat stat = EStat.valueOf(args[1].toUpperCase());
				int chance = Integer.valueOf(args[3]);
				boolean isLocked = Boolean.valueOf(args[4]);
				EDTL dtl = new EDTL(tier, stat, chance, isLocked);
				Utils.giveItem(player, EDTLUtils.getDTLItem(dtl));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/eternal getdtl <stat> <tier> <chance> <locked>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("getsocket")) {
			try {
				ETier tier = ETier.valueOf(args[2].toUpperCase());
				EStat stat = EStat.valueOf(args[1].toUpperCase());
				int chance = Integer.valueOf(args[3]);
				boolean isLocked = Boolean.valueOf(args[4]);
				ItemStack socket = ESocketUtils.getSocketItem(ESocketUtils.getSocket(tier, stat, chance, isLocked));
				Utils.giveItem(player, socket);
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/eternal getsocket <stat> <tier> <chance> <locked>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("getrdtl")) {
			try {
				ERDTL rdtl = ERDTL.fromString(args[1]);
				Utils.giveItem(player, ERDTLUtils.getItem(rdtl));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/eternal getrdtl <tier:chance:locked;>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("getrsocket")) {
			try {
				ERSocket rdtl = ERSocket.fromString(args[1]);
				Utils.giveItem(player, ERSocketUtils.getItem(rdtl));
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/eternal getrsocket <tier:chance:locked;>");
			}
		}
		
		else if (args[0].equalsIgnoreCase("getdabaoho")) {
			try {
				Utils.giveItem(player, EKeepGem.getItem());
			}
			catch (ArrayIndexOutOfBoundsException e) {
				sender.sendMessage("§a/eternal getdabaoho");
			}
		}
		
		

		return false;
	}

	public void sendTut(CommandSender sender) {
		sender.sendMessage("§cPotionTypes: §e" + getPotions());
		sender.sendMessage("§cStats: " + getStats());
		sender.sendMessage("");
		sender.sendMessage("§c§lCONSOLE");
		sender.sendMessage("§a/eternal reload");
		sender.sendMessage("§a/eternal m <player> <content>");
		sender.sendMessage("§a/eternal resetpotential <player>");
		sender.sendMessage("§a/eternal resetskill <player>");
		sender.sendMessage("§a/eternal levelup <player> <level>");
		sender.sendMessage("§a/eternal setplayerlevel <player> <level>");
		sender.sendMessage("§a/eternal renameitem <player>");
		sender.sendMessage("§a/eternal addexp <player> <exp>");
		sender.sendMessage("§a/eternal infogui <player>");
		sender.sendMessage("§a/eternal potentialgui <player>");
		sender.sendMessage("§a/eternal skillgui <player>");
		sender.sendMessage("§a/eternal weaponsoulgui <player>");
		sender.sendMessage("§a/eternal cuonghoagui <player>");
		sender.sendMessage("§a/eternal chuyenhoagui <player>");
		sender.sendMessage("§a/eternal gemgui <player>");
		sender.sendMessage("§a/eternal tinhluyengui <player>");
		sender.sendMessage("§a/eternal hopthegui <player>");
		sender.sendMessage("§a/eternal duclogui <player>");
		sender.sendMessage("§a/eternal khamdagui <player>");
		sender.sendMessage("§a/eternal unlockgui <player>");
		sender.sendMessage("§a/eternal socketsplitgui <player>");
		sender.sendMessage("§a/eternal sockethopthegui <player>");
		sender.sendMessage("");
		sender.sendMessage("§c§lPLAYER");
		sender.sendMessage("§a/eternal init <name>");
		sender.sendMessage("§a/eternal setname <name>");
		sender.sendMessage("§a/eternal setdesc <value>");
		sender.sendMessage("§a/eternal setlevel <level>");
		sender.sendMessage("§a/eternal setstat <stat> <value> <materialcheck>");
		sender.sendMessage("§a/eternal setpotion1 <type> <level> <chance>");
		sender.sendMessage("§a/eternal setpotion2 <type> <level>");
		sender.sendMessage("§a/eternal setsocket <stat> <buff%>");
		sender.sendMessage("§a/eternal setlocked <true|false>");
		sender.sendMessage("§a/eternal settier <type>");
		sender.sendMessage("§a/eternal setsockethole <value>");
		sender.sendMessage("§a/eternal settaggem");
		sender.sendMessage("§a/eternal shownbt");
		sender.sendMessage("§a/eternal runskill <skill> <point>");
		sender.sendMessage("§a/eternal testbuilditem");
		sender.sendMessage("§a/eternal getch"); 
		sender.sendMessage("§a/eternal getdtl <stat> <tier> <chance> <locked>");
		sender.sendMessage("§a/eternal getsocket <stat> <tier> <chance> <locked>");
		sender.sendMessage("§a/eternal getrdtl <tier:chance:locked;>");
		sender.sendMessage("§a/eternal getrsocket <tier:chance:locked;>");
		sender.sendMessage("§a/eternal getdabaoho");
		sender.sendMessage("");
	}

	public String getPotions() {
		String s = "";
		for (PotionEffectType t : PotionEffectType.values()) {
			if (t == null) continue;
			s += t.getName() + ", ";
		}
		s = s.substring(0, s.length() - 2);
		return s;
	}

	public String getStats() {
		String s = "";
		for (EStat t : EStat.values()) {
			s += t.name() + ", ";
		}
		s = s.substring(0, s.length() - 2);
		return s;
	}

}

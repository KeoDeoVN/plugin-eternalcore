package mk.plugin.eternalcore.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import com.massivecraft.factions.FPlayers;
import com.massivecraft.factions.Faction;

public class EFactionCommand implements CommandExecutor {

	@Override
	public boolean onCommand(CommandSender sender, Command arg1, String arg2, String[] args) {
		
		if (!sender.hasPermission("eternal.*")) return false;
		if (!Bukkit.getPluginManager().isPluginEnabled("Factions") && !Bukkit.getPluginManager().isPluginEnabled("SavageFactions")) return false;
		
		if (args[0].equalsIgnoreCase("setchestsize")) {
			Faction f = FPlayers.getInstance().getByPlayer(Bukkit.getPlayer(args[1])).getFaction();
			int value = Integer.valueOf(args[2]);
			f.setChestSize(value);
			sender.sendMessage("Set chest size of " + f.getTag() + ": " + value);
		}
		
		else if (args[0].equalsIgnoreCase("settntbanklimit")) {
			Faction f = FPlayers.getInstance().getByPlayer(Bukkit.getPlayer(args[1])).getFaction();
			int value = Integer.valueOf(args[2]);
			f.setTntBankLimit(value);
			sender.sendMessage("Set tnt bank limit of " + f.getTag() + ": " + value);
		}
		
		else if (args[0].equalsIgnoreCase("setmaxpower")) {
			Faction f = FPlayers.getInstance().getByPlayer(Bukkit.getPlayer(args[1])).getFaction();
			int value = Integer.valueOf(args[2]);
			f.setPowerBoost(value);
			sender.sendMessage("Set max power of " + f.getTag() + ": " + value);
		}

		
		return false;
	}

	public void sendTut(CommandSender sender) {
		sender.sendMessage("/ef setchestsize <player> <value>");
		sender.sendMessage("/ef settntbanklimit <player> <value>");
		sender.sendMessage("/ef setmaxpower <player> <value>");
		sender.sendMessage("/ef addpower <player> <value>");
	}
	
}

package mk.plugin.eternalcore.socket;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ESocketUtils {
	
	
	public static final int BASIC_VALUE = 10;
	
	public static final int MIN_CHANCE = 10;
	public static final int MAX_CHANCE = 60;
	
	public static int rate(ETier tier) {
		int min = getMinRate(tier);
		int max = getMaxRate(tier);
		return Utils.randomInt(min, max);
	}
	
	public static int getMinRate(ETier tier) {
		return new Double((double) BASIC_VALUE * 0.75 * tier.getMulti()).intValue();
	}
	
	public static int getMaxRate(ETier tier) {
		return new Double((double) BASIC_VALUE * 1.25 * tier.getMulti()).intValue();
	}
	
	public static ESocket getSocket(ETier tier, EStat stat, int chance, boolean isLocked) {
		int buff = rate(tier);
		ESocket socket = new ESocket(tier, stat, buff, chance, isLocked);
		return socket;
	}
	
	public static ItemStack getSocketItem(ESocket s) {
		String name = Utils.randomName(3);
		ETier tier = s.getTier();
		EStat stat = s.getStat();
		int success = s.getChance();
		int fail = 100 - success;
		
		ItemStack item = new ItemStack(Material.DIAMOND);
		ItemStackUtils.setDisplayName(item, tier.getColor() + "§l" + name + getLockedLore(s));
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oĐá quý");
		lore.add("§aPhẩm chất: §f" + tier.getColor() + tier.getName());
		lore.add("§aGiá trị: §f+" + s.getBuff() + "% " + stat.getName());
		lore.add("§aTỉ lệ: §2" + success + "% Thành §f| §c" + fail + "% Bại");
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addEnchantEffect(item);
		
		item = ItemStackUtils.setTag(item, "esocket", s.toString());
		
		return item;
	}
	
	public static boolean isESocket(ItemStack item) {
		return ItemStackUtils.hasTag(item, "esocket");
	}
	
	public static ESocket fromItem(ItemStack item) {
		if (!isESocket(item)) return null;
		return ESocket.fromString(ItemStackUtils.getTag(item, "esocket"));
	}
	
	public static String getLockedLore(ESocket dtl) {
		return dtl.isLocked() ? EConfig.LOCKED_LINE : "";
	}
	
	
}

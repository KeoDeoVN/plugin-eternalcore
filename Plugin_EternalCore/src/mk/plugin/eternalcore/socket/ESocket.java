package mk.plugin.eternalcore.socket;

import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;

public class ESocket {
	
	private ETier tier;
	private EStat stat;
	private int buff;
	private int chance;
	private boolean isLocked;
	
	public ESocket(ETier tier, EStat stat, int buff, int chance, boolean isLocked) {
		this.tier = tier;
		this.stat = stat;
		this.buff = buff;
		this.chance = chance;
		this.isLocked = isLocked;
	}

	public ETier getTier() {
		return tier;
	}

	public EStat getStat() {
		return stat;
	}

	public int getBuff() {
		return buff;
	}

	public int getChance() {
		return chance;
	}
	 
	public boolean isLocked() {
		return isLocked;
	}
	
	public String toString() {
		return this.tier.name() + ":" + this.stat.name() + ":" + this.buff + ":" + this.chance + ":" + this.isLocked;
	}
	
	public static ESocket fromString(String s) {
		ETier tier = ETier.valueOf(s.split(":")[0]);
		EStat stat = EStat.valueOf(s.split(":")[1]);
		int buff = Integer.valueOf(s.split(":")[2]);
		int chance = Integer.valueOf(s.split(":")[3]);
		boolean isLocked = Boolean.valueOf(s.split(":")[4]);
		return new ESocket(tier, stat, buff, chance, isLocked);
	}
	
}

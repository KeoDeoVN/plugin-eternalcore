package mk.plugin.eternalcore.socket;

import java.util.List;
import java.util.Map.Entry;

import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.inventory.ItemStack;

import com.google.common.collect.Lists;

import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.stat.EStat;
import mk.plugin.eternalcore.tier.ETier;
import mk.plugin.eternalcore.util.ItemStackUtils;
import mk.plugin.eternalcore.util.Utils;

public class ERSocketUtils {
	
	public static ItemStack getItem(ERSocket rdtl) {
		ETier tier = rdtl.getMaxTier();
		ItemStack item = new ItemStack(Material.DIAMOND);
		ItemStackUtils.setDisplayName(item, tier.getColor() + "§lĐá quý ngẫu nhiên" + getLockedLore(rdtl));
		List<String> lore = Lists.newArrayList();
		lore.add("§7§oClick chuột phải để dùng");
		
		lore.add("§aXác suất:");
		for (Entry<ETier, Integer> e : rdtl.getTierChances().entrySet()) {
			lore.add("  " + e.getKey().getColor() + e.getKey().getName() + ": §f" + e.getValue() + "%");
		}
		
		ItemStackUtils.setLore(item, lore);
		ItemStackUtils.addEnchantEffect(item);
		
		item = ItemStackUtils.setTag(item, "ERSocket", rdtl.toString());
		
		return item;
	}
	
	public static boolean isERSocket(ItemStack item) {
		return ItemStackUtils.hasTag(item, "ERSocket");
	}
	
	public static ERSocket fromItem(ItemStack item) {
		if (!isERSocket(item)) return null;
		return ERSocket.fromString(ItemStackUtils.getTag(item, "ERSocket"));
	}
	
	public static String getLockedLore(ERSocket dtl) {
		return dtl.isLocked() ? EConfig.LOCKED_LINE : "";
	}
	
	public static void onInteract(PlayerInteractEvent e) {
		Player player = e.getPlayer();
		ItemStack item = player.getInventory().getItemInMainHand();	
		if (item == null || item.getType() == Material.AIR) return;
		if (isERSocket(item)) {
			e.setCancelled(true);
			if (item.getAmount() == 1) player.getInventory().setItemInMainHand(null);
			else item.setAmount(item.getAmount() - 1);
			
			ERSocket rdtl = fromItem(item);
			ETier tier = rdtl.rateTier();
			
			ESocket dtl = new ESocket(tier, EStat.values()[Utils.randomInt(0, EStat.values().length - 1)], Utils.randomInt(ESocketUtils.getMinRate(tier), ESocketUtils.getMaxRate(tier)), Utils.randomInt(ESocketUtils.MIN_CHANCE, ESocketUtils.MAX_CHANCE), true);
			Utils.giveItem(player, ESocketUtils.getSocketItem(dtl));
			player.sendMessage("§aSử dụng thành công!");
			Utils.sendSound(player, Sound.ENTITY_EXPERIENCE_ORB_PICKUP, 1, 1);
		}
	}
	
	
	
	
	
	
	
	
	
}

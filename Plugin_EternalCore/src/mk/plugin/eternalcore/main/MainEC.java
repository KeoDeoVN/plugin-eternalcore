package mk.plugin.eternalcore.main;

import java.io.File;
import java.io.IOException;

import org.black_ixx.playerpoints.PlayerPoints;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.RegisteredServiceProvider;
import org.bukkit.plugin.java.JavaPlugin;

import me.realized.tokenmanager.api.TokenManager;
import mk.plugin.eternalcore.command.EAdminCommand;
import mk.plugin.eternalcore.command.EFactionCommand;
import mk.plugin.eternalcore.command.EPlayerCommand;
import mk.plugin.eternalcore.config.EConfig;
import mk.plugin.eternalcore.feature.permbuff.EPermBuff;
import mk.plugin.eternalcore.feature.power.EPowerPlaceholder;
import mk.plugin.eternalcore.heal.EHealTask;
import mk.plugin.eternalcore.listener.ChatListener;
import mk.plugin.eternalcore.listener.CraftListener;
import mk.plugin.eternalcore.listener.DataListener;
import mk.plugin.eternalcore.listener.DeathListener;
import mk.plugin.eternalcore.listener.EntityListener;
import mk.plugin.eternalcore.listener.GUIListener;
import mk.plugin.eternalcore.listener.ItemListener;
import mk.plugin.eternalcore.listener.JoinListener;
import mk.plugin.eternalcore.listener.LevelListener;
import mk.plugin.eternalcore.listener.SkillListener;
import mk.plugin.eternalcore.listener.StatListener;
import mk.plugin.eternalcore.listener.UseListener;
import mk.plugin.eternalcore.mythicmob.drop.MythicDrops;
import mk.plugin.eternalcore.player.EPlayerDataUtils;
import mk.plugin.eternalcore.task.EListNameTask;
import net.milkbowl.vault.economy.Economy;

public class MainEC extends JavaPlugin {
	
	public static MainEC plugin;
	private static Economy econ = null;
	private static PlayerPoints playerPoints = null;
	public static TokenManager tokenM = null;
	
	public FileConfiguration config;
	
	@Override
	public void onEnable() {
		plugin = this;
		this.saveDefaultConfig();
		this.reloadConfig();
		
		// Hook
		hookOtherPlugins();
		
		// Commands
		this.getCommand("eternal").setExecutor(new EAdminCommand());
		this.getCommand("eternalfaction").setExecutor(new EFactionCommand());
		this.getCommand("see").setExecutor(new EPlayerCommand());
		this.getCommand("potential").setExecutor(new EPlayerCommand());
		this.getCommand("skill").setExecutor(new EPlayerCommand());
		
		// Listeners
		Bukkit.getPluginManager().registerEvents(new DataListener(), this);
		Bukkit.getPluginManager().registerEvents(new StatListener(), this);
		Bukkit.getPluginManager().registerEvents(new GUIListener(), this);
		Bukkit.getPluginManager().registerEvents(new SkillListener(), this);
		Bukkit.getPluginManager().registerEvents(new DeathListener(), this);
		Bukkit.getPluginManager().registerEvents(new CraftListener(), this);
		Bukkit.getPluginManager().registerEvents(new ItemListener(), this);
		Bukkit.getPluginManager().registerEvents(new UseListener(), this);
		Bukkit.getPluginManager().registerEvents(new ChatListener(), this);
		Bukkit.getPluginManager().registerEvents(new LevelListener(), this);
		Bukkit.getPluginManager().registerEvents(new JoinListener(), this);
		Bukkit.getPluginManager().registerEvents(new EntityListener(), this);
		
		// Load data
		Bukkit.getOnlinePlayers().forEach(player -> {
			EPlayerDataUtils.load(player);
		});
		
		// Tasks
		new EHealTask().runTaskTimer(this, 0, 20);
		new EListNameTask().runTaskTimer(this, 0, 20);
		
		// Placeholder
		new EPowerPlaceholder().register();
	}
	
	@Override
	public void onDisable() {
		
		// Save
		Bukkit.getOnlinePlayers().forEach(player -> {
			EPlayerDataUtils.save(player);
		});
	}
	
	public void reloadConfig() {
		config = YamlConfiguration.loadConfiguration(new File(this.getDataFolder(), "config.yml"));
		EConfig.init(config);
		EPermBuff.load(config);
		MythicDrops.reload(config);
	}
	
	public void saveConfig() {
		try {
			config.save(new File(this.getDataFolder(), "config.yml"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	private void hookOtherPlugins() {
		hookVault();
		hookPlayerPoint();
		tokenM = (TokenManager) Bukkit.getPluginManager().getPlugin("TokenManager");
	}
	
	private boolean hookVault() {
	    if (getServer().getPluginManager().getPlugin("Vault") == null) {
	            return false;
	     }
	    RegisteredServiceProvider<Economy> rsp = getServer().getServicesManager().getRegistration(Economy.class);
	    if (rsp == null) {
	    	return false;
	    }
	    econ = rsp.getProvider();
	    return econ != null;
	}
	
    private boolean hookPlayerPoint() {
    	final Plugin pl = this.getServer().getPluginManager().getPlugin("PlayerPoints");
    	playerPoints = PlayerPoints.class.cast(pl);
    	return playerPoints != null;
    	
    }
    
    public static Economy getEcononomy() { return econ; }   
    public static PlayerPoints getPP() { return playerPoints; }  
    public static TokenManager getTokenManager() { return tokenM; }	
	
}
